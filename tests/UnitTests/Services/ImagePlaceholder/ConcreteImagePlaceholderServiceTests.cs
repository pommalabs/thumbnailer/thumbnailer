﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using FakeItEasy;
using Microsoft.Extensions.Logging;
using NUnit.Framework;
using PommaLabs.MimeTypes;
using PommaLabs.Thumbnailer.Models.Commands;
using PommaLabs.Thumbnailer.Models.DTO.Internal;
using PommaLabs.Thumbnailer.Models.DTO.Public;
using PommaLabs.Thumbnailer.Models.Enumerations;
using PommaLabs.Thumbnailer.Services.ImagePlaceholder;

namespace PommaLabs.Thumbnailer.UnitTests.Services.ImagePlaceholder;

[TestFixture, Parallelizable]
internal sealed class ConcreteImagePlaceholderServiceTests
{
    [Test]
    public async Task GenerateImagePlaceholderAsync_WhenFileDoesNotExist_ShouldReturnEmptyResult()
    {
        // Arrange
        var logger = A.Fake<ILogger<ConcreteImagePlaceholderService>>();
        var imagePlaceholderService = new ConcreteImagePlaceholderService(logger);
        var pgCommand = new ImagePlaceholderGenerationCommand(new TempFileMetadata(MimeTypeMap.IMAGE.PNG), ImagePlaceholderAlgorithm.BlurHash);

        // Act
        var result = await imagePlaceholderService.GenerateImagePlaceholderAsync(pgCommand, default);

        // Assert
        Assert.That(result, Is.SameAs(ImagePlaceholderResult.Empty));
    }

    [Test]
    public void GenerateImagePlaceholderAsync_WhenAlgorithmIsNone_ShouldThrowNotSupportedException()
    {
        // Arrange
        var logger = A.Fake<ILogger<ConcreteImagePlaceholderService>>();
        var imagePlaceholderService = new ConcreteImagePlaceholderService(logger);
        var pgCommand = new ImagePlaceholderGenerationCommand(new TempFileMetadata(MimeTypeMap.IMAGE.PNG), ImagePlaceholderAlgorithm.None);

        // Act & Assert
        Assert.ThrowsAsync<NotSupportedException>(() => imagePlaceholderService.GenerateImagePlaceholderAsync(pgCommand, default));
    }
}
