﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using FakeItEasy;
using Microsoft.AspNetCore.DataProtection;
using Microsoft.Extensions.Options;
using NUnit.Framework;
using PommaLabs.HtmlArk;
using PommaLabs.Thumbnailer.Models.Configurations;
using PommaLabs.Thumbnailer.Services.Html;

namespace PommaLabs.Thumbnailer.UnitTests.Services.Html;

[TestFixture, Parallelizable]
internal sealed class ConcreteHtmlServiceTests
{
    [Test]
    public async Task EmbedHtmlImagesAsync_WhenInvoked_ShouldArchiveGivenHtmlPath()
    {
        // Arrange
        var htmlArchiver = A.Fake<IHtmlArchiver>();
        var htmlService = CreateHtmlService(htmlArchiver);
        var htmlFileName = GetHtmlFileName();

        // Act
        await htmlService.EmbedHtmlImagesAsync(htmlFileName);

        // Assert
        A.CallTo(() => htmlArchiver.ArchiveAsync(htmlFileName, An<HtmlArchiverSettings>._))
            .MustHaveHappenedOnceExactly();
    }

    [Test]
    public void EncryptHtmlFileName_WhenInvoked_ShouldEncryptGivenHtmlFileName()
    {
        // Arrange
        var htmlService = CreateHtmlService();
        var htmlFileName = GetHtmlFileName();

        // Act
        var encryptedHtmlFileName = htmlService.EncryptHtmlFileName(htmlFileName);

        // Assert
        Assert.That(encryptedHtmlFileName, Is.Not.EqualTo(htmlFileName));
    }

    [Test]
    public void TryDecryptHtmlFileName_WithValidInput_ShouldReturnTrueAndDecryptedHtmlFileName()
    {
        // Arrange
        var htmlService = CreateHtmlService();
        var htmlFileName = GetHtmlFileName();
        var encryptedHtmlFileName = htmlService.EncryptHtmlFileName(htmlFileName);

        // Act
        var decrypted = htmlService.TryDecryptHtmlFileName(encryptedHtmlFileName, out var decryptedHtmlFileName);

        // Assert
        Assert.That(decrypted, Is.True);
        Assert.That(decryptedHtmlFileName, Is.EqualTo(htmlFileName));
    }

    [Test]
    public void TryDecryptHtmlFileName_WithInvalidInput_ShouldReturnFalse()
    {
        // Arrange
        var htmlService = CreateHtmlService();

        // Act
        var decrypted = htmlService.TryDecryptHtmlFileName(Utils.Faker.Lorem.Word(), out var decryptedHtmlFileName);

        // Assert
        Assert.That(decrypted, Is.False);
        Assert.That(decryptedHtmlFileName, Is.Empty);
    }

    private static ConcreteHtmlService CreateHtmlService(IHtmlArchiver? htmlArchiver = null)
    {
        htmlArchiver ??= A.Fake<IHtmlArchiver>();
        return new ConcreteHtmlService(
            new EphemeralDataProtectionProvider(),
            new OptionsWrapper<SecurityConfiguration>(new()),
            htmlArchiver);
    }

    private static string GetHtmlFileName()
    {
        return Path.GetTempFileName() + ".html";
    }
}
