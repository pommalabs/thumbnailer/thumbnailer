﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using FakeItEasy;
using Microsoft.Extensions.Logging;
using NUnit.Framework;
using PommaLabs.Thumbnailer.Core;
using PommaLabs.Thumbnailer.Models.Commands;
using PommaLabs.Thumbnailer.Services.Job;

namespace PommaLabs.Thumbnailer.UnitTests.Services.Job;

[TestFixture, Parallelizable]
internal sealed class ValidatingJobServiceTests
{
    [Test]
    public async Task DequeueCommandAsync_WhenInvoked_ShouldInvokeDecoratedService()
    {
        // Arrange
        var decoratedJobService = A.Fake<IJobService>();
        var logger = A.Fake<ILogger<ValidatingJobService>>();
        var jobService = new ValidatingJobService(decoratedJobService, logger);

        // Act
        await jobService.DequeueCommandAsync(default);

        // Assert
        A.CallTo(() => decoratedJobService.DequeueCommandAsync(A<CancellationToken>._)).MustHaveHappenedOnceExactly();
    }

    [Test]
    public async Task EnqueueCommandAsync_WhenInvoked_ShouldValidateCommand()
    {
        // Arrange
        var decoratedJobService = A.Fake<IJobService>();
        var logger = A.Fake<ILogger<ValidatingJobService>>();
        var jobService = new ValidatingJobService(decoratedJobService, logger);
        var command = A.Fake<ThumbnailerCommandBase>();

        // Act
        await jobService.EnqueueCommandAsync<string>(command, default);

        // Assert
        A.CallTo(() => command.Validate(A<Validator>._)).MustHaveHappenedOnceExactly();
    }

    [Test]
    public async Task EnqueueCommandAsync_WhenInvoked_ShouldInvokeDecoratedService()
    {
        // Arrange
        var decoratedJobService = A.Fake<IJobService>();
        var logger = A.Fake<ILogger<ValidatingJobService>>();
        var jobService = new ValidatingJobService(decoratedJobService, logger);
        var command = A.Fake<ThumbnailerCommandBase>();

        // Act
        await jobService.EnqueueCommandAsync<string>(command, default);

        // Assert
        A.CallTo(() => decoratedJobService.EnqueueCommandAsync<string>(command, A<CancellationToken>._)).MustHaveHappenedOnceExactly();
    }

    [TestCase(""), TestCase(" ")]
    public async Task GetJobResultAsync_WhenPublicJobIdIsEmptyOrWhiteSpace_ShouldReturnDefault(string publicJobId)
    {
        // Arrange
        var decoratedJobService = A.Fake<IJobService>();
        var logger = A.Fake<ILogger<ValidatingJobService>>();
        var jobService = new ValidatingJobService(decoratedJobService, logger);

        // Act
        var result = await jobService.GetJobResultAsync<string>(publicJobId, default);

        // Assert
        Assert.That(result, Is.EqualTo(default(string?)));
    }

    [Test]
    public async Task GetJobResultAsync_WhenInvoked_ShouldInvokeDecoratedService()
    {
        // Arrange
        var decoratedJobService = A.Fake<IJobService>();
        var logger = A.Fake<ILogger<ValidatingJobService>>();
        var jobService = new ValidatingJobService(decoratedJobService, logger);
        var publicJobId = GetRandomPublicJobId();

        // Act
        await jobService.GetJobResultAsync<string>(publicJobId, default);

        // Assert
        A.CallTo(() => decoratedJobService.GetJobResultAsync<string>(publicJobId, A<CancellationToken>._)).MustHaveHappenedOnceExactly();
    }

    [Test]
    public async Task MarkJobAsFailedAsync_WhenInvoked_ShouldInvokeDecoratedService()
    {
        // Arrange
        var decoratedJobService = A.Fake<IJobService>();
        var logger = A.Fake<ILogger<ValidatingJobService>>();
        var jobService = new ValidatingJobService(decoratedJobService, logger);
        var internalJobId = GetRandomInternalJobId();
        var failureReason = Utils.Faker.Random.Word();

        // Act
        await jobService.MarkJobAsFailedAsync(internalJobId, failureReason, default);

        // Assert
        A.CallTo(() => decoratedJobService.MarkJobAsFailedAsync(internalJobId, failureReason, A<CancellationToken>._)).MustHaveHappenedOnceExactly();
    }

    [Test]
    public async Task MarkJobAsProcessedAsync_WhenInvoked_ShouldInvokeDecoratedService()
    {
        // Arrange
        var decoratedJobService = A.Fake<IJobService>();
        var logger = A.Fake<ILogger<ValidatingJobService>>();
        var jobService = new ValidatingJobService(decoratedJobService, logger);
        var internalJobId = GetRandomInternalJobId();
        var result = Utils.Faker.Random.Word();

        // Act
        await jobService.MarkJobAsProcessedAsync(internalJobId, result, default);

        // Assert
        A.CallTo(() => decoratedJobService.MarkJobAsProcessedAsync(internalJobId, result, A<CancellationToken>._)).MustHaveHappenedOnceExactly();
    }

    [Test]
    public async Task QueryJobAsync_WhenInvoked_ShouldInvokeDecoratedService()
    {
        // Arrange
        var decoratedJobService = A.Fake<IJobService>();
        var logger = A.Fake<ILogger<ValidatingJobService>>();
        var jobService = new ValidatingJobService(decoratedJobService, logger);
        var publicJobId = GetRandomPublicJobId();

        // Act
        await jobService.QueryJobAsync(publicJobId, default);

        // Assert
        A.CallTo(() => decoratedJobService.QueryJobAsync(publicJobId, A<CancellationToken>._)).MustHaveHappenedOnceExactly();
    }

    private static Guid GetRandomInternalJobId() => Guid.NewGuid();

    private static string GetRandomPublicJobId() => GetRandomInternalJobId().ToString();
}
