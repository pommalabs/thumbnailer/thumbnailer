﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using FakeItEasy;
using Microsoft.ApplicationInsights;
using NUnit.Framework;
using PommaLabs.MimeTypes;
using PommaLabs.Thumbnailer.Models.Commands;
using PommaLabs.Thumbnailer.Models.DTO.Internal;
using PommaLabs.Thumbnailer.Models.Enumerations;
using PommaLabs.Thumbnailer.Services.Optimization;

namespace PommaLabs.Thumbnailer.UnitTests.Services.Optimization;

[TestFixture, Parallelizable]
internal sealed class TracingOptimizationServiceTests
{
    [Test]
    public async Task OptimizeMediaAsync_WhenInvoked_ShouldInvokeDecoratedService()
    {
        // Arrange
        var serviceProvider = A.Fake<IServiceProvider>();
        A.CallTo(() => serviceProvider.GetService(typeof(TelemetryClient))).Returns(new TelemetryClient(null));

        var decoratedOptimizationService = A.Fake<IOptimizationService>();
        var optimizationService = new TracingOptimizationService(decoratedOptimizationService, serviceProvider);
        var moCommand = new MediaOptimizationCommand(new TempFileMetadata(MimeTypeMap.IMAGE.PNG), ImagePlaceholderAlgorithm.None);

        // Act
        await optimizationService.OptimizeMediaAsync(moCommand, default);

        // Assert
        A.CallTo(() => decoratedOptimizationService.OptimizeMediaAsync(moCommand, A<CancellationToken>._)).MustHaveHappenedOnceExactly();
    }
}
