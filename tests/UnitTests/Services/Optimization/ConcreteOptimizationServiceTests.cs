﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using FakeItEasy;
using NUnit.Framework;
using PommaLabs.MimeTypes;
using PommaLabs.Thumbnailer.Models.Commands;
using PommaLabs.Thumbnailer.Models.DTO.Internal;
using PommaLabs.Thumbnailer.Models.Enumerations;
using PommaLabs.Thumbnailer.Models.Exceptions;
using PommaLabs.Thumbnailer.Repositories.TempFile;
using PommaLabs.Thumbnailer.Services.Optimization;
using PommaLabs.Thumbnailer.Services.Process;

namespace PommaLabs.Thumbnailer.UnitTests.Services.Optimization;

[TestFixture, Parallelizable]
internal sealed class ConcreteOptimizationServiceTests
{
    [Test]
    public async Task OptimizeMediaAsync_WhenGifFileIsReceived_ShouldInvokeGifsicle()
    {
        // Arrange
        var processService = A.Fake<IProcessService>();
        var tempFileRepository = A.Fake<ITempFileRepository>();
        var optimizationService = new ConcreteOptimizationService(processService, tempFileRepository);
        var moCommand = new MediaOptimizationCommand(new TempFileMetadata(MimeTypeMap.IMAGE.GIF), ImagePlaceholderAlgorithm.None);

        // Act
        await optimizationService.OptimizeMediaAsync(moCommand, default);

        // Assert
        A.CallTo(() => processService.RunProcessAsync("gifsicle", A<string>._, A<bool>._, default)).MustHaveHappenedOnceExactly();
    }

    [Test]
    public async Task OptimizeMediaAsync_WhenJpegFileIsReceived_ShouldInvokeJpegOptim()
    {
        // Arrange
        var processService = A.Fake<IProcessService>();
        var tempFileRepository = A.Fake<ITempFileRepository>();
        var optimizationService = new ConcreteOptimizationService(processService, tempFileRepository);
        var moCommand = new MediaOptimizationCommand(new TempFileMetadata(MimeTypeMap.IMAGE.JPEG), ImagePlaceholderAlgorithm.None);

        // Act
        await optimizationService.OptimizeMediaAsync(moCommand, default);

        // Assert
        A.CallTo(() => processService.RunProcessAsync("evaluator", A<string>.That.StartsWith("jpegoptim"), A<bool>._, default)).MustHaveHappenedOnceExactly();
    }

    [Test]
    public async Task OptimizeMediaAsync_WhenPngFileIsReceived_ShouldInvokePngquant()
    {
        // Arrange
        var processService = A.Fake<IProcessService>();
        var tempFileRepository = A.Fake<ITempFileRepository>();
        var optimizationService = new ConcreteOptimizationService(processService, tempFileRepository);
        var moCommand = new MediaOptimizationCommand(new TempFileMetadata(MimeTypeMap.IMAGE.PNG), ImagePlaceholderAlgorithm.None);

        // Act
        await optimizationService.OptimizeMediaAsync(moCommand, default);

        // Assert
        A.CallTo(() => processService.RunProcessAsync("pngquant", A<string>._, A<bool>._, default)).MustHaveHappenedOnceExactly();
    }

    [TestCase(98), TestCase(99)]
    public async Task OptimizeMediaAsync_WhenAlreadyOptimizedPngFileIsReceived_ShouldReturnInputFile(int exitCode)
    {
        // Arrange
        var processService = A.Fake<IProcessService>();
        var tempFileRepository = A.Fake<ITempFileRepository>();
        var optimizationService = new ConcreteOptimizationService(processService, tempFileRepository);
        var moCommand = new MediaOptimizationCommand(new TempFileMetadata(MimeTypeMap.IMAGE.PNG), ImagePlaceholderAlgorithm.None);

        A.CallTo(() => processService.RunProcessAsync("pngquant", A<string>._, A<bool>._, default))
            .Throws(new ProcessException(string.Empty, string.Empty, exitCode));

        // Act
        var result = await optimizationService.OptimizeMediaAsync(moCommand, default);

        // Assert
        Assert.That(result, Is.SameAs(moCommand.File));
    }

    [Test]
    public async Task OptimizeMediaAsync_WhenSvgFileIsReceived_ShouldInvokeScour()
    {
        // Arrange
        var processService = A.Fake<IProcessService>();
        var tempFileRepository = A.Fake<ITempFileRepository>();
        var optimizationService = new ConcreteOptimizationService(processService, tempFileRepository);
        var moCommand = new MediaOptimizationCommand(new TempFileMetadata(MimeTypeMap.IMAGE.SVG_XML), ImagePlaceholderAlgorithm.None);

        // Act
        await optimizationService.OptimizeMediaAsync(moCommand, default);

        // Assert
        A.CallTo(() => processService.RunProcessAsync("scour", A<string>._, A<bool>._, default)).MustHaveHappenedOnceExactly();
    }

    [Test]
    public async Task OptimizeMediaAsync_WhenMp4FileIsReceived_ShouldInvokeFfmpeg()
    {
        // Arrange
        var processService = A.Fake<IProcessService>();
        var tempFileRepository = A.Fake<ITempFileRepository>();
        var optimizationService = new ConcreteOptimizationService(processService, tempFileRepository);
        var moCommand = new MediaOptimizationCommand(new TempFileMetadata(MimeTypeMap.VIDEO.MP4), ImagePlaceholderAlgorithm.None);

        // Act
        await optimizationService.OptimizeMediaAsync(moCommand, default);

        // Assert
        A.CallTo(() => processService.RunProcessAsync("ffmpeg", A<string>._, A<bool>._, default)).MustHaveHappenedOnceExactly();
    }
}
