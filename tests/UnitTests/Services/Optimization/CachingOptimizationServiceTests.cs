﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using FakeItEasy;
using Microsoft.Extensions.Options;
using NUnit.Framework;
using PommaLabs.KVLite.Memory;
using PommaLabs.MimeTypes;
using PommaLabs.Thumbnailer.Models.Commands;
using PommaLabs.Thumbnailer.Models.Configurations;
using PommaLabs.Thumbnailer.Models.DTO.Internal;
using PommaLabs.Thumbnailer.Models.Enumerations;
using PommaLabs.Thumbnailer.Services.Optimization;

namespace PommaLabs.Thumbnailer.UnitTests.Services.Optimization;

[TestFixture, Parallelizable(ParallelScope.Fixtures)]
internal sealed class CachingOptimizationServiceTests
{
    [Test]
    public async Task OptimizeMediaAsync_WhenInvoked_ShouldInvokeDecoratedService()
    {
        // Arrange
        var decoratedOptimizationService = A.Fake<IOptimizationService>();
        var cache = new MemoryCache(new MemoryCacheSettings());
        var databaseConfiguration = A.Fake<IOptions<DatabaseConfiguration>>();
        var optimizationService = new CachingOptimizationService(decoratedOptimizationService, cache, databaseConfiguration);
        var moCommand = new MediaOptimizationCommand(new TempFileMetadata(MimeTypeMap.IMAGE.PNG), ImagePlaceholderAlgorithm.None);

        // Act
        await optimizationService.OptimizeMediaAsync(moCommand, default);

        // Assert
        A.CallTo(() => decoratedOptimizationService.OptimizeMediaAsync(moCommand, A<CancellationToken>._))
            .MustHaveHappenedTwiceExactly(); // Nested service is invoked twice because output file does not exist and caching layer tries to recreate it.
    }
}
