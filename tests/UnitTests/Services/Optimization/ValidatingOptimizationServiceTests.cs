﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using FakeItEasy;
using Microsoft.Extensions.Logging;
using NUnit.Framework;
using PommaLabs.MimeTypes;
using PommaLabs.Thumbnailer.Models.Commands;
using PommaLabs.Thumbnailer.Models.DTO.Internal;
using PommaLabs.Thumbnailer.Models.Enumerations;
using PommaLabs.Thumbnailer.Services.Optimization;

namespace PommaLabs.Thumbnailer.UnitTests.Services.Optimization;

[TestFixture, Parallelizable]
internal sealed class ValidatingOptimizationServiceTests
{
    [Test]
    public async Task OptimizeMediaAsync_WhenInvoked_ShouldInvokeDecoratedService()
    {
        // Arrange
        var decoratedOptimizationService = A.Fake<IOptimizationService>();
        var logger = A.Fake<ILogger<ValidatingOptimizationService>>();
        var optimizationService = new ValidatingOptimizationService(decoratedOptimizationService, logger);
        var moCommand = new MediaOptimizationCommand(new TempFileMetadata(MimeTypeMap.IMAGE.PNG), ImagePlaceholderAlgorithm.None);

        // Act
        try
        {
            // Validation layer checks output file existance and throws if it does not exist.
            await optimizationService.OptimizeMediaAsync(moCommand, default);
        }
        catch
        {
            // Assert
            A.CallTo(() => decoratedOptimizationService.OptimizeMediaAsync(moCommand, A<CancellationToken>._)).MustHaveHappenedOnceExactly();
            return;
        }
        Assert.Fail("Expected assert exception was not thrown");
    }
}
