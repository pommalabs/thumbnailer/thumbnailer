﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using FakeItEasy;
using Microsoft.Extensions.Options;
using NodaTime;
using NUnit.Framework;
using PommaLabs.Salatino.Models.Options;
using PommaLabs.Thumbnailer.Models.Configurations;
using PommaLabs.Thumbnailer.Repositories.ApiKey;

namespace PommaLabs.Thumbnailer.UnitTests.Repositories.ApiKey;

[TestFixture, Parallelizable]
internal sealed class ConfigurationApiKeyRepositoryTests
{
    [Test]
    public async Task AddTempApiKeyAsync_WhenInvoked_ShouldInvokeDecoratedRepository()
    {
        // Arrange
        var securityOptions = A.Fake<IOptions<SecurityOptions>>();
        var securityConfiguration = A.Fake<IOptions<SecurityConfiguration>>();
        var decoratedApiKeyRepository = A.Fake<IApiKeyRepository>();
        var clock = A.Fake<IClock>();
        var apiKeyRepository = new ConfigurationApiKeyRepository(securityOptions, securityConfiguration, decoratedApiKeyRepository, clock);

        var apiKey = Utils.Faker.Random.Word();

        // Act
        await apiKeyRepository.AddTempApiKeyAsync(apiKey, default);

        // Assert
        A.CallTo(() => decoratedApiKeyRepository.AddTempApiKeyAsync(apiKey, A<CancellationToken>._)).MustHaveHappenedOnceExactly();
    }

    [Test]
    public async Task DeleteExpiredTempApiKeysAsync_WhenInvoked_ShouldInvokeDecoratedRepository()
    {
        // Arrange
        var securityOptions = A.Fake<IOptions<SecurityOptions>>();
        var securityConfiguration = A.Fake<IOptions<SecurityConfiguration>>();
        var decoratedApiKeyRepository = A.Fake<IApiKeyRepository>();
        var clock = A.Fake<IClock>();
        var apiKeyRepository = new ConfigurationApiKeyRepository(securityOptions, securityConfiguration, decoratedApiKeyRepository, clock);

        // Act
        await apiKeyRepository.DeleteExpiredTempApiKeysAsync(default);

        // Assert
        A.CallTo(() => decoratedApiKeyRepository.DeleteExpiredTempApiKeysAsync(A<CancellationToken>._)).MustHaveHappenedOnceExactly();
    }

    [Test]
    public async Task ValidateApiKeyAsync_WhenApiKeyIsDefinedInConfiguration_ShouldNotInvokeDecoratedRepository()
    {
        // Arrange
        var apiKeyName = Utils.Faker.Random.Word();
        var apiKeyValue = Utils.Faker.Random.Word();
        var securityOptions = new OptionsWrapper<SecurityOptions>(new SecurityOptions
        {
            ApiKeys =
            [
                new SecurityOptions.ApiKey { Name = apiKeyName, Value = apiKeyValue }
            ],
        });

        var securityConfiguration = A.Fake<IOptions<SecurityConfiguration>>();
        var decoratedApiKeyRepository = A.Fake<IApiKeyRepository>();
        var clock = A.Fake<IClock>();
        var apiKeyRepository = new ConfigurationApiKeyRepository(securityOptions, securityConfiguration, decoratedApiKeyRepository, clock);

        // Act
        await apiKeyRepository.ValidateApiKeyAsync(apiKeyValue, default);

        // Assert
        A.CallTo(() => decoratedApiKeyRepository.ValidateApiKeyAsync(apiKeyValue, A<CancellationToken>._)).MustNotHaveHappened();
    }

    [Test]
    public async Task ValidateApiKeyAsync_WhenApiKeyIsDefinedInConfiguration_ShouldValidateId()
    {
        // Arrange
        var apiKeyName = Utils.Faker.Random.Word();
        var apiKeyValue = Utils.Faker.Random.Word();
        var securityOptions = new OptionsWrapper<SecurityOptions>(new SecurityOptions
        {
            ApiKeys =
            [
                new SecurityOptions.ApiKey { Name = apiKeyName, Value = apiKeyValue }
            ],
        });

        var securityConfiguration = A.Fake<IOptions<SecurityConfiguration>>();
        var decoratedApiKeyRepository = A.Fake<IApiKeyRepository>();
        var clock = A.Fake<IClock>();
        var apiKeyRepository = new ConfigurationApiKeyRepository(securityOptions, securityConfiguration, decoratedApiKeyRepository, clock);

        // Act
        var apiKeyCredentials = await apiKeyRepository.ValidateApiKeyAsync(apiKeyValue, default);

        // Assert
        Assert.That(apiKeyCredentials, Is.Not.Null);
        Assert.That(apiKeyCredentials!.Name, Is.EqualTo(apiKeyName));
    }
}
