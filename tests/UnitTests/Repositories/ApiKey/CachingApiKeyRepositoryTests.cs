﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using FakeItEasy;
using NUnit.Framework;
using PommaLabs.Thumbnailer.Models.DTO.Internal;
using PommaLabs.Thumbnailer.Repositories.ApiKey;

namespace PommaLabs.Thumbnailer.UnitTests.Repositories.ApiKey;

[TestFixture, Parallelizable]
internal sealed class CachingApiKeyRepositoryTests
{
    [Test]
    public async Task AddTempApiKeyAsync_WhenInvoked_ShouldInvokeDecoratedRepository()
    {
        // Arrange
        var decoratedApiKeyRepository = A.Fake<IApiKeyRepository>();
        var cache = CreateApiKeyCache();
        var apiKeyRepository = new CachingApiKeyRepository(decoratedApiKeyRepository, cache);

        var apiKey = Utils.Faker.Random.Word();

        // Act
        await apiKeyRepository.AddTempApiKeyAsync(apiKey, default);

        // Assert
        A.CallTo(() => decoratedApiKeyRepository.AddTempApiKeyAsync(apiKey, A<CancellationToken>._)).MustHaveHappenedOnceExactly();
    }

    [Test]
    public async Task AddTempApiKeyAsync_WhenInvoked_ShouldClearApiKeyCache()
    {
        // Arrange
        var decoratedApiKeyRepository = A.Fake<IApiKeyRepository>();
        var cache = CreateApiKeyCache();
        var apiKeyRepository = new CachingApiKeyRepository(decoratedApiKeyRepository, cache);

        var apiKey = Utils.Faker.Random.Word();

        var placeholderApiKey = new ApiKeyCredentials
        {
            Name = Utils.Faker.Random.Word(),
            Value = Utils.Faker.Random.Word(),
            ExpiresAt = Utils.Faker.Date.FutureOffset(),
            IsValid = true,
        };

        cache.Add(placeholderApiKey.Value, placeholderApiKey, placeholderApiKey.ExpiresAt.Value);

        // Act
        await apiKeyRepository.AddTempApiKeyAsync(apiKey, default);

        // Assert
        Assert.That(cache.Count, Is.EqualTo(0));
    }

    [Test]
    public async Task DeleteExpiredTempApiKeysAsync_WhenInvoked_ShouldInvokeDecoratedRepository()
    {
        // Arrange
        var decoratedApiKeyRepository = A.Fake<IApiKeyRepository>();
        var cache = CreateApiKeyCache();
        var apiKeyRepository = new CachingApiKeyRepository(decoratedApiKeyRepository, cache);

        // Act
        await apiKeyRepository.DeleteExpiredTempApiKeysAsync(default);

        // Assert
        A.CallTo(() => decoratedApiKeyRepository.DeleteExpiredTempApiKeysAsync(A<CancellationToken>._)).MustHaveHappenedOnceExactly();
    }

    [Test]
    public async Task DeleteExpiredTempApiKeysAsync_WhenInvoked_ShouldClearApiKeyCache()
    {
        // Arrange
        var decoratedApiKeyRepository = A.Fake<IApiKeyRepository>();
        var cache = CreateApiKeyCache();
        var apiKeyRepository = new CachingApiKeyRepository(decoratedApiKeyRepository, cache);

        var placeholderApiKey = new ApiKeyCredentials
        {
            Name = Utils.Faker.Random.Word(),
            Value = Utils.Faker.Random.Word(),
            ExpiresAt = Utils.Faker.Date.FutureOffset(),
            IsValid = true,
        };

        cache.Add(placeholderApiKey.Value, placeholderApiKey, placeholderApiKey.ExpiresAt.Value);

        // Act
        await apiKeyRepository.DeleteExpiredTempApiKeysAsync(default);

        // Assert
        Assert.That(cache.Count, Is.EqualTo(0));
    }

    [Test]
    public async Task ValidateApiKeyAsync_WhenInvoked_ShouldInvokeDecoratedRepository()
    {
        // Arrange
        var decoratedApiKeyRepository = A.Fake<IApiKeyRepository>();
        var cache = CreateApiKeyCache();
        var apiKeyRepository = new CachingApiKeyRepository(decoratedApiKeyRepository, cache);

        var apiKey = Utils.Faker.Random.Word();

        // Act
        await apiKeyRepository.ValidateApiKeyAsync(apiKey, default);

        // Assert
        A.CallTo(() => decoratedApiKeyRepository.ValidateApiKeyAsync(apiKey, A<CancellationToken>._)).MustHaveHappenedOnceExactly();
    }

    private static ApiKeyCache CreateApiKeyCache()
    {
        return new ApiKeyCache(Utils.Faker.Random.Int(10, 100));
    }
}
