﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using FakeItEasy;
using Microsoft.Extensions.Diagnostics.HealthChecks;
using NUnit.Framework;
using PommaLabs.Thumbnailer.Core;
using PommaLabs.Thumbnailer.Repositories.TempFile;
using PommaLabs.Thumbnailer.Server.HealthChecks;

namespace PommaLabs.Thumbnailer.UnitTests.HealthChecks;

[TestFixture, Parallelizable(ParallelScope.Fixtures)]
internal sealed class TempFileRepositoryHealthCheckTests
{
    #region Setup/Teardown

    [SetUp]
    public void SetUp()
    {
        HealthCheckCacheHelper<ITempFileRepository>.ClearCachedResults();
    }

    [TearDown]
    public void TearDown()
    {
        HealthCheckCacheHelper<ITempFileRepository>.ClearCachedResults();
    }

    #endregion Setup/Teardown

    [Test]
    public async Task ShouldReuseCachedResult_WhenHealthCheckFailsBecauseOfMissingFreeSpace_AndResultIsStillValid()
    {
        // Arrange
        var client = A.Fake<ITempFileRepository>();
        A.CallTo(() => client.GetAvailableFreeSpaceInBytes(A<CancellationToken>._))
            .Returns(0);

        var healthCheck = new TempFileRepositoryHealthCheck(client);

        var context = new HealthCheckContext();
        await healthCheck.CheckHealthAsync(context);

        // Act
        await healthCheck.CheckHealthAsync(context);

        // Assert
        A.CallTo(() => client.GetAvailableFreeSpaceInBytes(A<CancellationToken>._))
            .MustHaveHappenedOnceExactly();
    }

    [Test]
    public async Task ShouldReuseCachedResult_WhenHealthCheckFailsBecauseOfException_AndResultIsStillValid()
    {
        // Arrange
        var client = A.Fake<ITempFileRepository>();
        A.CallTo(() => client.GetAvailableFreeSpaceInBytes(A<CancellationToken>._))
            .Throws<InvalidOperationException>();

        var healthCheck = new TempFileRepositoryHealthCheck(client);

        var context = new HealthCheckContext();
        await healthCheck.CheckHealthAsync(context);

        // Act
        await healthCheck.CheckHealthAsync(context);

        // Assert
        A.CallTo(() => client.GetAvailableFreeSpaceInBytes(A<CancellationToken>._))
            .MustHaveHappenedOnceExactly();
    }

    [Test]
    public async Task ShouldReuseCachedResult_WhenHealthCheckSucceeds_AndResultIsStillValid()
    {
        // Arrange
        var client = A.Fake<ITempFileRepository>();
        A.CallTo(() => client.GetAvailableFreeSpaceInBytes(A<CancellationToken>._))
            .Returns(10 * TempFileRepositoryHealthCheck.FreeSpaceThreshold);

        var healthCheck = new TempFileRepositoryHealthCheck(client);

        var context = new HealthCheckContext();
        await healthCheck.CheckHealthAsync(context);

        // Act
        await healthCheck.CheckHealthAsync(context);

        // Assert
        A.CallTo(() => client.GetAvailableFreeSpaceInBytes(A<CancellationToken>._))
            .MustHaveHappenedOnceExactly();
    }
}
