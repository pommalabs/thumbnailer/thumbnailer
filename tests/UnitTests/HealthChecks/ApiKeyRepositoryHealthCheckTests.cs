﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using FakeItEasy;
using Microsoft.Extensions.Diagnostics.HealthChecks;
using NUnit.Framework;
using PommaLabs.Thumbnailer.Core;
using PommaLabs.Thumbnailer.Models.DTO.Internal;
using PommaLabs.Thumbnailer.Repositories.ApiKey;
using PommaLabs.Thumbnailer.Server.HealthChecks;

namespace PommaLabs.Thumbnailer.UnitTests.HealthChecks;

[TestFixture, Parallelizable(ParallelScope.Fixtures)]
internal sealed class ApiKeyRepositoryHealthCheckTests
{
    #region Setup/Teardown

    [SetUp]
    public void SetUp()
    {
        HealthCheckCacheHelper<IApiKeyRepository>.ClearCachedResults();
    }

    [TearDown]
    public void TearDown()
    {
        HealthCheckCacheHelper<IApiKeyRepository>.ClearCachedResults();
    }

    #endregion Setup/Teardown

    [Test]
    public async Task ShouldReuseCachedResult_WhenHealthCheckFails_AndResultIsStillValid()
    {
        // Arrange
        var client = A.Fake<IApiKeyRepository>();
        A.CallTo(() => client.ValidateApiKeyAsync(A<string>._, A<CancellationToken>._))
            .Returns<ApiKeyCredentials?>(default);

        var healthCheck = new ApiKeyRepositoryHealthCheck(client);

        var context = new HealthCheckContext();
        await healthCheck.CheckHealthAsync(context);

        // Act
        await healthCheck.CheckHealthAsync(context);

        // Assert
        A.CallTo(() => client.ValidateApiKeyAsync(A<string>._, A<CancellationToken>._))
            .MustHaveHappenedOnceExactly();
    }

    [Test]
    public async Task ShouldReuseCachedResult_WhenHealthCheckSucceeds_AndResultIsStillValid()
    {
        // Arrange
        var client = A.Fake<IApiKeyRepository>();
        A.CallTo(() => client.ValidateApiKeyAsync(A<string>._, A<CancellationToken>._))
            .Throws<InvalidOperationException>();

        var healthCheck = new ApiKeyRepositoryHealthCheck(client);

        var context = new HealthCheckContext();
        await healthCheck.CheckHealthAsync(context);

        // Act
        await healthCheck.CheckHealthAsync(context);

        // Assert
        A.CallTo(() => client.ValidateApiKeyAsync(A<string>._, A<CancellationToken>._))
            .MustHaveHappenedOnceExactly();
    }
}
