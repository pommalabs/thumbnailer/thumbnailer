﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System.Collections;
using System.Runtime.CompilerServices;
using System.Text.Json;
using Blurhash.SkiaSharp;
using Microsoft.Extensions.DependencyInjection;
using PommaLabs.Thumbnailer.Client;
using PommaLabs.Thumbnailer.Client.Models.Enumerations;
using PommaLabs.Thumbnailer.Core;
using SkiaSharp;
using FileResponse = PommaLabs.Thumbnailer.Client.Core.FileResponse;

namespace PommaLabs.Thumbnailer.IntegrationTests;

[TestFixture, Parallelizable]
internal abstract class ThumbnailerTestsBase
{
    protected const string ApiKeyHeaderName = "X-Api-Key";
    protected const string ImagePlaceholderAlgorithmQueryParam = "imagePlaceholderAlgorithm";
    protected const string ImagePlaceholderHeaderName = "X-Image-Placeholder";
    protected const string PngExtension = "png";

    private const double ImageComparerThreshold = 90;
    private const string SampleFilesDirectory = "SampleFiles";
    private const string SpecFileName = "_spec.json";

    static ThumbnailerTestsBase()
    {
        VerifyImageHash.RegisterComparer(ImageComparerThreshold, null, "gif");
        VerifyImageHash.RegisterComparer(ImageComparerThreshold, null, "jpg");
        VerifyImageHash.RegisterComparer(ImageComparerThreshold, null, PngExtension);
        VerifyImageHash.RegisterComparer(ImageComparerThreshold, null, "webp");
    }

    protected static async Task<IThumbnailerClient> GetThumbnailerClientAsync()
    {
        var client = Services.ServiceProvider.GetRequiredService<IThumbnailerClient>();
        await EnsureServiceIsHealthyAsync(client);
        return client;
    }

    protected static IEnumerable<SampleFile> ReadSampleFiles(OperationType operationType)
    {
        var pickRandomSample = bool.Parse(Environment.GetEnvironmentVariable("PICK_RANDOM_SAMPLE") ?? "false");

        foreach (var fileFormatDirectory in Directory.GetDirectories(SampleFilesDirectory))
        {
            var specFile = ReadSpecFile(fileFormatDirectory);

            if (!IsOperationSupported(operationType, specFile))
            {
                continue;
            }

            var filePaths = Directory
                .GetFiles(fileFormatDirectory)
                .Where(fp => !fp.EndsWith(SpecFileName));

            if (pickRandomSample)
            {
                var tempFilePaths = filePaths.ToList();
                var sampleIndex = Random.Shared.Next(tempFilePaths.Count);
                filePaths = [tempFilePaths[sampleIndex]];
            }

            foreach (var filePath in filePaths)
            {
                yield return CreateSampleFile(filePath, fileFormatDirectory, specFile);
            }
        }
    }

    protected static IEnumerable<(SampleFile SampleFile, ImagePlaceholderAlgorithm ImagePlaceholderAlgorithm)> ReadSampleFilesWithImagePlaceholderAlgorithm(OperationType operationType)
    {
        var sampleFiles = ReadSampleFiles(operationType);
        var supportedImagePlaceholderAlgorithms = Enum.GetValues<ImagePlaceholderAlgorithm>().Where(a => a != ImagePlaceholderAlgorithm.None);

        return from sampleFile in sampleFiles
               from imagePlaceholderAlgorithm in supportedImagePlaceholderAlgorithms
               select (sampleFile, imagePlaceholderAlgorithm);
    }

    protected static IEnumerable<SampleFileList> ReadSampleFilesByFormat(OperationType operationType)
    {
        foreach (var fileFormatDirectory in Directory.GetDirectories(SampleFilesDirectory))
        {
            var specFile = ReadSpecFile(fileFormatDirectory);

            if (!IsOperationSupported(operationType, specFile))
            {
                continue;
            }

            var filePaths = Directory
                .GetFiles(fileFormatDirectory)
                .Where(fp => !fp.EndsWith(SpecFileName));

            var format = GetFormat(fileFormatDirectory);
            var sampleFiles = filePaths.Select(filePath => CreateSampleFile(filePath, fileFormatDirectory, specFile));

            yield return new SampleFileList(format, sampleFiles);
        }
    }

    private static bool IsOperationSupported(OperationType operationType, SpecFile specFile)
    {
        return operationType switch
        {
            OperationType.ThumbnailGeneration => specFile.ThumbnailGenerationSupported,
            OperationType.MediaOptimization => specFile.MediaOptimizationSupported,
            OperationType.ImagePlaceholderGeneration => specFile.ImagePlaceholderGenerationSupported,
            _ => false
        };
    }

    private static SampleFile CreateSampleFile(string filePath, string fileFormatDirectory, SpecFile specFile)
    {
        return new SampleFile
        {
            LocalPath = filePath,
            Format = GetFormat(fileFormatDirectory),
            ThumbnailGenerationSupported = specFile.ThumbnailGenerationSupported,
            MediaOptimizationSupported = specFile.MediaOptimizationSupported,
            ImagePlaceholderGenerationSupported = specFile.ImagePlaceholderGenerationSupported,
            FallbackRequired = specFile.FallbackRequired,
        };
    }

    private static string GetFormat(string fileFormatDirectory)
    {
        return Path.GetFileName(fileFormatDirectory).ToUpperInvariant();
    }

    protected async Task VerifyImagePlaceholderAsync(SampleFile sampleFile, string imagePlaceholder, ImagePlaceholderAlgorithm imagePlaceholderAlgorithm)
    {
        switch (imagePlaceholderAlgorithm)
        {
            case ImagePlaceholderAlgorithm.BlurHash:
                await VerifyBlurHashAsync(sampleFile, imagePlaceholder);
                break;

            case ImagePlaceholderAlgorithm.ThumbHash:
                await VerifyThumbHashAsync(sampleFile, imagePlaceholder);
                break;

            case ImagePlaceholderAlgorithm.None:
            default:
                throw new NotSupportedException();
        }
    }

    private async Task VerifyBlurHashAsync(SampleFile sampleFile, string imagePlaceholder)
    {
        using var scaledBitmap = Blurhasher.Decode(imagePlaceholder, 128, 128);
        using var encodedImage = SKImage.FromBitmap(scaledBitmap).Encode(SKEncodedImageFormat.Png, 100);
        await Verify(encodedImage.ToArray(), "png", CreateImagePlaceholderVerifySettings(sampleFile, ImagePlaceholderAlgorithm.BlurHash));
    }

    private async Task VerifyThumbHashAsync(SampleFile sampleFile, string imagePlaceholder)
    {
        var thumbHash = Convert.FromBase64String(imagePlaceholder);
        var (w, h, rgba) = ThumbHashes.Utilities.ThumbHashToRgba(thumbHash);
        using var hashImage = SKImage.FromPixelCopy(new SKImageInfo(w, h, SKColorType.Rgba8888, SKAlphaType.Unpremul), rgba);
        const int ScaleFactor = 4; // Yields an image whose larger side is 128px.
        using var scaledBitmap = new SKBitmap(new SKImageInfo(w * ScaleFactor, h * ScaleFactor, SKColorType.Rgba8888, SKAlphaType.Unpremul));
        hashImage.ScalePixels(scaledBitmap.PeekPixels(), SKSamplingOptions.Default);
        using var encodedImage = SKImage.FromBitmap(scaledBitmap).Encode(SKEncodedImageFormat.Png, 100);
        await Verify(encodedImage.ToArray(), "png", CreateImagePlaceholderVerifySettings(sampleFile, ImagePlaceholderAlgorithm.ThumbHash));
    }

    protected VerifySettings CreateFileVerifySettings(SampleFile sampleFile, [CallerMemberName] string? methodName = null)
    {
        var testContext = methodName!.Split("_")[1];

        var settings = new VerifySettings();

        settings.UseDirectory("VerifiedResults");
        settings.UseFileName($"{GetType().Name}.{testContext}.{sampleFile.Code}");

        // Verified results are shared between tests for v1 and v2 endpoints.
        settings.DisableRequireUniquePrefix();

        return settings;
    }

    protected static async Task<byte[]> ConvertFileResponseToByteArrayAsync(FileResponse response)
    {
        using var memoryStream = new MemoryStream();
        await response.Stream.CopyToAsync(memoryStream);
        return memoryStream.ToArray();
    }

    protected static async Task<byte[]> ReadSampleFileBytesAsync(SampleFile sampleFile)
    {
        return await File.ReadAllBytesAsync(sampleFile.LocalPath);
    }

    private VerifySettings CreateImagePlaceholderVerifySettings(SampleFile sampleFile, ImagePlaceholderAlgorithm imagePlaceholderAlgorithm)
    {
        var settings = new VerifySettings();

        settings.UseDirectory("VerifiedResults");
        settings.UseFileName($"{GetType().Name}.{imagePlaceholderAlgorithm}.{sampleFile.Code}");

        // Verified results are shared between tests for v1 and v2 endpoints.
        settings.DisableRequireUniquePrefix();

        return settings;
    }

    private static async Task EnsureServiceIsHealthyAsync(IThumbnailerClient client)
    {
        const int RetryCount = 30;
        const int Delay = 1000;

        for (var i = 0; i < RetryCount; ++i)
        {
            if (await client.IsHealthyAsync())
            {
                return;
            }
            await Task.Delay(Delay);
        }

        throw new InvalidOperationException("Thumbnailer service is not healthy, tests will not be run");
    }

    private static SpecFile ReadSpecFile(string fileFormatDirectory)
    {
        var specFileContents = File.ReadAllText(Path.Combine(fileFormatDirectory, SpecFileName));
        return JsonSerializer.Deserialize<SpecFile>(specFileContents, Constants.CaseInsensitiveJsonSerializerOptions)!;
    }

    public sealed class SampleFile : SpecFile
    {
        public string Code => $"{Format}_{Number}";

        public string ContentType => MimeTypeHelper.GetMimeTypeAndExtension(LocalPath).mimeType;

        public string Extension => Format.ToLowerInvariant();

        public string Format { get; set; } = string.Empty;

        public string LocalPath { get; set; } = string.Empty;

        public string Name => Path.GetFileName(LocalPath);

        public string Number => Path.GetFileNameWithoutExtension(LocalPath);

        public Uri RemoteUri => new($"http://sample-files:48080/{Format.ToLowerInvariant()}/{Name}");

        public override string ToString() => Code;
    }

    public class SpecFile
    {
        public bool FallbackRequired { get; set; }

        public bool ImagePlaceholderGenerationSupported { get; set; }

        public bool MediaOptimizationSupported { get; set; }

        public bool ThumbnailGenerationSupported { get; set; }
    }

    protected sealed class SampleFileList(string format, IEnumerable<SampleFile> sampleFiles)
        : IEnumerable<SampleFile>
    {
        public IEnumerator<SampleFile> GetEnumerator()
        {
            return sampleFiles.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return ((IEnumerable)sampleFiles).GetEnumerator();
        }

        public override string ToString()
        {
            return format;
        }
    }
}
