﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using PommaLabs.Thumbnailer.Client;
using PommaLabs.Thumbnailer.Client.Core;

namespace PommaLabs.Thumbnailer.IntegrationTests;

[SetUpFixture]
internal static class Services
{
    private static readonly string? s_baseUri;
    private static readonly WebApplicationFactory<Program>? s_webServiceFactory;
    private static readonly Process? s_htmlServerProcess;

    static Services()
    {
        var services = new ServiceCollection();

        s_baseUri = Environment.GetEnvironmentVariable("THUMBNAILER_BASE_URI");

        services.Configure<ThumbnailerClientConfiguration>(options =>
        {
            var apiKey = Environment.GetEnvironmentVariable("THUMBNAILER_API_KEY");

            if (s_baseUri != null) options.BaseUri = new Uri(s_baseUri);
            if (apiKey != null) options.ApiKey = apiKey;
        });

        services.AddLogging(options => options.AddConsole().SetMinimumLevel(LogLevel.Trace));

        services.AddThumbnailerClient();

        ServiceProvider = services.BuildServiceProvider();

        if (string.IsNullOrWhiteSpace(s_baseUri))
        {
            // When using in-memory TestServer, we also need a running instance of the API
            // in order to expose HTML files to Firefox process instances. In fact,
            // an URL is passed to Firefox command line arguments and the HTTP call
            // to download the HTML file is performed by Firefox itself,
            // which cannot use the dedicated HTTP client.
            var htmlServerProcessStartInfo = new ProcessStartInfo
            {
                FileName = "dotnet",
                Arguments = "PommaLabs.Thumbnailer.Server.dll",
            };

            const string Port = "38080";
            Environment.SetEnvironmentVariable("PORT", Port);
            htmlServerProcessStartInfo.EnvironmentVariables["PORT"] = Port;

            s_htmlServerProcess = Process.Start(htmlServerProcessStartInfo);

            s_webServiceFactory = new WebApplicationFactory<Program>();

            // First client creation also ensures that test server is available. Otherwise,
            // concurrent calls to CreateClient might start more than one test server.
            s_webServiceFactory.CreateClient();

            HttpClientFactory.CreateHttpClient = _ => s_webServiceFactory.CreateClient();
        }
    }

    public static ServiceProvider ServiceProvider { get; }

    [SuppressMessage("Structure", "NUnit1028:The non-test method is public", Justification = "Helper method used to create HTTP clients")]
    public static HttpClient GetHttpClient()
    {
        return s_webServiceFactory?.CreateClient() ?? new HttpClient { BaseAddress = new Uri(s_baseUri!) };
    }

    [OneTimeTearDown]
    public static void OneTimeTearDown()
    {
        if (s_htmlServerProcess != null)
        {
            s_htmlServerProcess.Kill();
            s_htmlServerProcess.Dispose();
        }
        s_webServiceFactory?.Dispose();
        ServiceProvider?.Dispose();
    }
}
