﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using FakeItEasy;
using Microsoft.Extensions.Options;
using NodaTime;
using PommaLabs.Thumbnailer.Models.Configurations;
using PommaLabs.Thumbnailer.Repositories.ApiKey;
using PommaLabs.Thumbnailer.Server;
using SqlKata.Execution;

namespace PommaLabs.Thumbnailer.IntegrationTests.Repositories.ApiKey;

internal abstract class DbApiKeyRepositoryTests : ApiKeyRepositoryTestsBase
{
    private QueryFactory? _db;

    protected abstract DatabaseConfiguration DatabaseConfiguration { get; }

    protected override IApiKeyRepository CreateRepository(
        IOptions<SecurityConfiguration>? securityConfiguration = null,
        IClock? clock = null)
    {
        securityConfiguration ??= A.Fake<IOptions<SecurityConfiguration>>();
        clock ??= A.Fake<IClock>();

        var dbc = new OptionsWrapper<DatabaseConfiguration>(DatabaseConfiguration);
        _db = QueryFactoryHelper.GetQueryFactory(DatabaseConfiguration);

        return new DbApiKeyRepository(securityConfiguration, _db, dbc, clock);
    }

    [TearDown]
    public async Task TearDown()
    {
        if (_db == null)
        {
            return;
        }

        await _db
            .Query($"{DatabaseConfiguration.SchemaNameWithDot}api_keys")
            .DeleteAsync();

        _db.Dispose();
        _db = null;
    }
}
