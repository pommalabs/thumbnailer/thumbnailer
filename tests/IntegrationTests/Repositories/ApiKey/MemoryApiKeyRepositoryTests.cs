﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using FakeItEasy;
using Microsoft.Extensions.Options;
using NodaTime;
using PommaLabs.Thumbnailer.Models.Configurations;
using PommaLabs.Thumbnailer.Repositories.ApiKey;

namespace PommaLabs.Thumbnailer.IntegrationTests.Repositories.ApiKey;

internal sealed class MemoryApiKeyRepositoryTests : ApiKeyRepositoryTestsBase
{
    protected override IApiKeyRepository CreateRepository(
        IOptions<SecurityConfiguration>? securityConfiguration = null,
        IClock? clock = null)
    {
        securityConfiguration ??= A.Fake<IOptions<SecurityConfiguration>>();
        clock ??= A.Fake<IClock>();
        return new MemoryApiKeyRepository(securityConfiguration, clock);
    }
}
