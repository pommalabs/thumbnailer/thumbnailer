﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using PommaLabs.Thumbnailer.Models.Configurations;
using PommaLabs.Thumbnailer.Models.Enumerations;

namespace PommaLabs.Thumbnailer.IntegrationTests.Repositories.ApiKey;

internal sealed class MySqlApiKeyRepositoryTests : DbApiKeyRepositoryTests
{
    protected override DatabaseConfiguration DatabaseConfiguration { get; } = new DatabaseConfiguration
    {
        Provider = DatabaseProvider.MySql,
        ConnectionString = "Server=mariadb;Database=thumbnailer;Uid=thumbnailer;Pwd=thumbnailer;",
    };
}
