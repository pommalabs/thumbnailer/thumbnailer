﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using FakeItEasy;
using Humanizer;
using Microsoft.Extensions.Options;
using NodaTime;
using NodaTime.Testing;
using PommaLabs.Thumbnailer.Models.Configurations;
using PommaLabs.Thumbnailer.Repositories.ApiKey;

namespace PommaLabs.Thumbnailer.IntegrationTests.Repositories.ApiKey;

[TestFixture, Parallelizable(ParallelScope.Fixtures)]
internal abstract class ApiKeyRepositoryTestsBase
{
    private const int HoursUntilApiKeyExpires = 1;

    [Test]
    public async Task AddTempApiKeyAsync_WhenInvoked_ShouldAddValidApiKey()
    {
        // Arrange
        var repository = CreateRepository();

        var apiKey = GetRandomApiKey();

        // Act
        await repository.AddTempApiKeyAsync(apiKey, default);

        // Assert
        var apiKeyCredentials = await repository.ValidateApiKeyAsync(apiKey, default);
        Assert.That(apiKeyCredentials, Is.Not.Null);
        Assert.That(apiKeyCredentials!.Value, Is.EqualTo(apiKey));
    }

    [Test]
    public async Task ValidateApiKeyAsync_WhenApiKeyDoesNotExist_ShouldReturnNull()
    {
        // Arrange
        var repository = CreateRepository();

        var apiKey = GetRandomApiKey();

        // Act
        var apiKeyCredentials = await repository.ValidateApiKeyAsync(apiKey, default);

        // Assert
        Assert.That(apiKeyCredentials, Is.Null);
    }

    [Test]
    public async Task ValidateApiKeyAsync_WhenApiKeyHasExpired_ShouldReturnNull()
    {
        // Arrange
        var clock = new FakeClock(SystemClock.Instance.GetCurrentInstant());
        var repository = CreateRepository(clock: clock);

        var apiKey = GetRandomApiKey();

        await repository.AddTempApiKeyAsync(apiKey, default);
        clock.AdvanceHours(HoursUntilApiKeyExpires);

        // Act
        var apiKeyCredentials = await repository.ValidateApiKeyAsync(apiKey, default);

        // Assert
        Assert.That(apiKeyCredentials, Is.Null);
    }

    [Test]
    public async Task ValidateApiKeyAsync_WhenCaseSensitiveValidationIsEnabled_ShouldApplyCaseSensitiveValidation()
    {
        // Arrange
        var securityConfiguration = A.Fake<IOptions<SecurityConfiguration>>();
        var repository = CreateRepository(securityConfiguration: securityConfiguration);

        var apiKey = GetRandomApiKey();
        await repository.AddTempApiKeyAsync(apiKey.ToLower(), default);

        A.CallTo(() => securityConfiguration.Value).Returns(new SecurityConfiguration { EnableCaseSensitiveApiKeyValidation = true });

        // Act
        var sameCase = await repository.ValidateApiKeyAsync(apiKey.ToLower(), default);
        var differentCase = await repository.ValidateApiKeyAsync(apiKey.ToUpper(), default);

        // Assert
        Assert.That(sameCase, Is.Not.Null);
        Assert.That(differentCase, Is.Null);
    }

    [Test]
    public async Task ValidateApiKeyAsync_WhenCaseSensitiveValidationIsDisabled_ShouldApplyCaseInsensitiveValidation()
    {
        // Arrange
        var securityConfiguration = A.Fake<IOptions<SecurityConfiguration>>();
        var repository = CreateRepository(securityConfiguration: securityConfiguration);

        var apiKey = GetRandomApiKey();
        await repository.AddTempApiKeyAsync(apiKey.ToLower(), default);

        A.CallTo(() => securityConfiguration.Value).Returns(new SecurityConfiguration { EnableCaseSensitiveApiKeyValidation = false });

        // Act
        var sameCase = await repository.ValidateApiKeyAsync(apiKey.ToLower(), default);
        var differentCase = await repository.ValidateApiKeyAsync(apiKey.ToUpper(), default);

        // Assert
        Assert.That(sameCase, Is.Not.Null);
        Assert.That(differentCase, Is.Not.Null);
    }

    [Test]
    public async Task DeleteExpiredTempApiKeysAsync_WhenApiKeyHasNotExpired_ShouldNotDeleteIt()
    {
        // Arrange
        var clock = A.Fake<IClock>();
        var repository = CreateRepository(clock: clock);

        var apiKey = GetRandomApiKey();
        await repository.AddTempApiKeyAsync(apiKey, default);

        // Act
        await repository.DeleteExpiredTempApiKeysAsync(default);

        // Assert
        var apiKeyCredentials = await repository.ValidateApiKeyAsync(apiKey, default);
        Assert.That(apiKeyCredentials, Is.Not.Null);
    }

    [Test]
    public async Task DeleteExpiredTempApiKeysAsync_WhenApiKeyHasExpired_ShouldDeleteIt()
    {
        // Arrange
        var clock = new FakeClock(SystemClock.Instance.GetCurrentInstant());
        var repository = CreateRepository(clock: clock);

        var apiKey = GetRandomApiKey();
        await repository.AddTempApiKeyAsync(apiKey, default);

        // Act
        clock.AdvanceHours(HoursUntilApiKeyExpires);
        await repository.DeleteExpiredTempApiKeysAsync(default);
        clock.AdvanceHours(-1 * HoursUntilApiKeyExpires);

        // Assert
        var apiKeyCredentials = await repository.ValidateApiKeyAsync(apiKey, default);
        Assert.That(apiKeyCredentials, Is.Null);
    }

    protected abstract IApiKeyRepository CreateRepository(
        IOptions<SecurityConfiguration>? securityConfiguration = null,
        IClock? clock = null);

    private static string GetRandomApiKey()
    {
        const int MaxApiKeyLength = 32;
        return Utils.Faker.Random.Word().Truncate(MaxApiKeyLength);
    }
}
