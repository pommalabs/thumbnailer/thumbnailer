﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using PommaLabs.Thumbnailer.Models.Configurations;
using PommaLabs.Thumbnailer.Models.Enumerations;

namespace PommaLabs.Thumbnailer.IntegrationTests.Repositories.ApiKey;

internal sealed class SqlServerApiKeyRepositoryTests : DbApiKeyRepositoryTests
{
    protected override DatabaseConfiguration DatabaseConfiguration { get; } = new DatabaseConfiguration
    {
        Provider = DatabaseProvider.SqlServer,
        ConnectionString = "Server=mssql;Database=thumbnailer;User Id=sa;Password=thumbnailer(3!3)THUMBNAILER;Encrypt=false;",
    };
}
