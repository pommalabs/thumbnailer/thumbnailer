﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System.Collections.Concurrent;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using PommaLabs.MimeTypes;
using PommaLabs.Thumbnailer.Client;
using PommaLabs.Thumbnailer.Client.Core;
using PommaLabs.Thumbnailer.Client.Models.Enumerations;
using PommaLabs.Thumbnailer.Client.Models.Results;
using FileDetails = PommaLabs.Thumbnailer.Client.Core.FileDetails;
using ImagePlaceholderAlgorithm = PommaLabs.Thumbnailer.Client.Models.Enumerations.ImagePlaceholderAlgorithm;
using JobDetails = PommaLabs.Thumbnailer.Client.Core.JobDetails;

namespace PommaLabs.Thumbnailer.IntegrationTests;

internal sealed class ThumbnailGenerationTests : ThumbnailerTestsBase
{
    [SetUp]
    public void SetUp()
    {
        var logger = Services.ServiceProvider.GetRequiredService<ILogger<ThumbnailGenerationTests>>();
        logger.LogTrace("Running thumbnail generation test {TestName}...", TestContext.CurrentContext.Test.Name);
    }

    [TestCaseSource(nameof(ReadSampleFiles), new object[] { OperationType.ThumbnailGeneration })]
    public async Task Base64_256x256WithFill_V1_ShouldGenerateExpectedResult(SampleFile sampleFile)
    {
        // Arrange
        var client = await GetThumbnailerClientAsync();
        var clientConfiguration = Services.ServiceProvider.GetRequiredService<IOptions<ThumbnailerClientConfiguration>>();
        var fileBytes = await ReadSampleFileBytesAsync(sampleFile);

        using var httpClient = Services.GetHttpClient();
        var apiClient = new ThumbnailGenerationApiClient(httpClient)
        {
            ApiKey = clientConfiguration.Value.ApiKey
        };

        // Act
        var isSupported = await client.IsThumbnailGenerationSupportedAsync(sampleFile.ContentType);
        var response = await apiClient.GenerateThumbnailFromJsonV1Async(
            widthPx: null, heightPx: null, sidePx: null, shavePx: null, fill: null, smartCrop: null,
            fallback: sampleFile.FallbackRequired, imagePlaceholderAlgorithm: null, responseType: null, openInBrowser: null,
            body: new FileDetails { FileName = sampleFile.Name, Contents = fileBytes, ContentType = sampleFile.ContentType });
        var resultBytes = await ConvertFileResponseToByteArrayAsync(response);

        // Assert
        Assert.That(isSupported || sampleFile.FallbackRequired, Is.True);
        Assert.That(resultBytes, Is.Not.Empty);
        var (resultMimeType, _) = MimeTypeHelper.GetMimeTypeAndExtension(resultBytes, sampleFile.Name);
        Assert.That(resultMimeType, Is.EqualTo(MimeTypeMap.IMAGE.PNG));
        await Verify(resultBytes, PngExtension, CreateFileVerifySettings(sampleFile));
    }

    [TestCaseSource(nameof(ReadSampleFiles), new object[] { OperationType.ThumbnailGeneration })]
    public async Task Base64_256x256WithFill_V2_ShouldGenerateExpectedResult(SampleFile sampleFile)
    {
        // Arrange
        var client = await GetThumbnailerClientAsync();
        var clientImplementation = (StandardThumbnailerClient)client;
        var clientConfiguration = Services.ServiceProvider.GetRequiredService<IOptions<ThumbnailerClientConfiguration>>();
        var fileBytes = await ReadSampleFileBytesAsync(sampleFile);

        using var httpClient = Services.GetHttpClient();
        var apiClient = new ThumbnailGenerationApiClient(httpClient)
        {
            ApiKey = clientConfiguration.Value.ApiKey
        };

        // Act
        var isSupported = await client.IsThumbnailGenerationSupportedAsync(sampleFile.ContentType);
        var job = await apiClient.GenerateThumbnailFromJsonV2Async(
            widthPx: null, heightPx: null, shavePx: null, fill: null, smartCrop: null,
            fallback: sampleFile.FallbackRequired, imagePlaceholderAlgorithm: null,
            body: new FileDetails { FileName = sampleFile.Name, Contents = fileBytes, ContentType = sampleFile.ContentType });

        var clientJob = new JobDetails { JobId = job.JobId };
        await clientImplementation.CheckJobStatusAsync(clientJob, default);
        var result = await clientImplementation.DownloadFileResultAsync(clientJob, default);

        // Assert
        Assert.That(isSupported || sampleFile.FallbackRequired, Is.True);
        Assert.That(result.Contents, Is.Not.Empty);
        var (resultMimeType, _) = MimeTypeHelper.GetMimeTypeAndExtension(result.Contents, sampleFile.Name);
        Assert.That(resultMimeType, Is.EqualTo(MimeTypeMap.IMAGE.PNG));
        await Verify(result.Contents, PngExtension, CreateFileVerifySettings(sampleFile));
    }

    [TestCaseSource(nameof(ReadSampleFilesWithImagePlaceholderAlgorithm), new object[] { OperationType.ThumbnailGeneration })]
    public async Task Base64_WithImagePlaceholderAlgorithm_V1_ShouldGenerateExpectedImagePlaceholder((SampleFile SampleFile, ImagePlaceholderAlgorithm ImagePlaceholderAlgorithm) sampleFileWithAlgorithm)
    {
        // Arrange
        var sampleFile = sampleFileWithAlgorithm.SampleFile;
        var imagePlaceholderAlgorithm = sampleFileWithAlgorithm.ImagePlaceholderAlgorithm;
        var clientConfiguration = Services.ServiceProvider.GetRequiredService<IOptions<ThumbnailerClientConfiguration>>();
        var fileBytes = await ReadSampleFileBytesAsync(sampleFile);

        using var httpClient = Services.GetHttpClient();
        var apiClient = new ThumbnailGenerationApiClient(httpClient)
        {
            ApiKey = clientConfiguration.Value.ApiKey
        };

        // Act
        var response = await apiClient.GenerateThumbnailFromJsonV1Async(
            widthPx: null, heightPx: null, sidePx: null, shavePx: null, fill: null, smartCrop: null,
            fallback: sampleFile.FallbackRequired, responseType: null, openInBrowser: null,
            imagePlaceholderAlgorithm: (Client.Core.ImagePlaceholderAlgorithm)imagePlaceholderAlgorithm,
            body: new FileDetails { FileName = sampleFile.Name, Contents = fileBytes, ContentType = sampleFile.ContentType });

        // Assert
        response.Headers.TryGetValue(ImagePlaceholderHeaderName, out var imagePlaceholderHeaderValues);
        var imagePlaceholder = imagePlaceholderHeaderValues?.SingleOrDefault();
        Assert.That(imagePlaceholder, Is.Not.Null.Or.Empty);
        await VerifyImagePlaceholderAsync(sampleFile, imagePlaceholder!, imagePlaceholderAlgorithm);
    }

    [TestCaseSource(nameof(ReadSampleFiles), new object[] { OperationType.ThumbnailGeneration })]
    public async Task LocalPath_256x256WithFill_V1_ShouldGenerateExpectedResult(SampleFile sampleFile)
    {
        // Arrange
        var client = await GetThumbnailerClientAsync();
        var clientConfiguration = Services.ServiceProvider.GetRequiredService<IOptions<ThumbnailerClientConfiguration>>();
        var fileBytes = await ReadSampleFileBytesAsync(sampleFile);

        using var httpClient = Services.GetHttpClient();
        var apiClient = new ThumbnailGenerationApiClient(httpClient)
        {
            ApiKey = clientConfiguration.Value.ApiKey
        };

        // Act
        var isSupported = await client.IsThumbnailGenerationSupportedAsync(sampleFile.ContentType);
        var response = await apiClient.GenerateThumbnailFromFileV1Async(
            widthPx: null, heightPx: null, sidePx: null, shavePx: null, fill: null, smartCrop: null,
            fallback: sampleFile.FallbackRequired, imagePlaceholderAlgorithm: null, responseType: null, openInBrowser: null,
            new FileParameter(new MemoryStream(fileBytes), sampleFile.Name, sampleFile.ContentType));
        var resultBytes = await ConvertFileResponseToByteArrayAsync(response);

        // Assert
        Assert.That(isSupported || sampleFile.FallbackRequired, Is.True);
        Assert.That(resultBytes, Is.Not.Empty);
        var (resultMimeType, _) = MimeTypeHelper.GetMimeTypeAndExtension(resultBytes, sampleFile.Name);
        Assert.That(resultMimeType, Is.EqualTo(MimeTypeMap.IMAGE.PNG));
        await Verify(resultBytes, PngExtension, CreateFileVerifySettings(sampleFile));
    }

    [TestCaseSource(nameof(ReadSampleFiles), new object[] { OperationType.ThumbnailGeneration })]
    public async Task LocalPath_256x256WithFill_V2_ShouldGenerateExpectedResult(SampleFile sampleFile)
    {
        // Arrange
        var client = await GetThumbnailerClientAsync();
        var fileBytes = await ReadSampleFileBytesAsync(sampleFile);

        // Act
        var isSupported = await client.IsThumbnailGenerationSupportedAsync(sampleFile.ContentType);
        var result = await client.GenerateThumbnailAsync(
            fileBytes, fileName: sampleFile.Name, fallback: sampleFile.FallbackRequired);

        // Assert
        Assert.That(isSupported || sampleFile.FallbackRequired, Is.True);
        Assert.That(result.Contents, Is.Not.Empty);
        var (resultMimeType, _) = MimeTypeHelper.GetMimeTypeAndExtension(result.Contents, sampleFile.Name);
        Assert.That(resultMimeType, Is.EqualTo(MimeTypeMap.IMAGE.PNG));
        await Verify(result.Contents, PngExtension, CreateFileVerifySettings(sampleFile));
    }

    [TestCaseSource(nameof(ReadSampleFiles), new object[] { OperationType.ThumbnailGeneration })]
    public async Task LocalPath_256x256WithSmartCrop_V2_ShouldGenerateExpectedResult(SampleFile sampleFile)
    {
        // Arrange
        var client = await GetThumbnailerClientAsync();
        var fileBytes = await ReadSampleFileBytesAsync(sampleFile);

        // Act
        var isSupported = await client.IsThumbnailGenerationSupportedAsync(sampleFile.ContentType);
        var result = await client.GenerateThumbnailAsync(
            fileBytes, fileName: sampleFile.Name, smartCrop: true,
            fallback: sampleFile.FallbackRequired);

        // Assert
        Assert.That(isSupported || sampleFile.FallbackRequired, Is.True);
        Assert.That(result.Contents, Is.Not.Empty);
        var (resultMimeType, _) = MimeTypeHelper.GetMimeTypeAndExtension(result.Contents, sampleFile.Name);
        Assert.That(resultMimeType, Is.EqualTo(MimeTypeMap.IMAGE.PNG));
        await Verify(result.Contents, PngExtension, CreateFileVerifySettings(sampleFile));
    }

    [TestCaseSource(nameof(ReadSampleFiles), new object[] { OperationType.ThumbnailGeneration })]
    public async Task LocalPath_384x384ShavedOff42_V2_ShouldGenerateExpectedResult(SampleFile sampleFile)
    {
        // Arrange
        var client = await GetThumbnailerClientAsync();
        var fileBytes = await ReadSampleFileBytesAsync(sampleFile);

        // Act
        var isSupported = await client.IsThumbnailGenerationSupportedAsync(sampleFile.ContentType);
        var result = await client.GenerateThumbnailAsync(
            fileBytes, fileName: sampleFile.Name, widthPx: 384, heightPx: 384, shavePx: 42,
            fallback: sampleFile.FallbackRequired);

        // Assert
        Assert.That(isSupported || sampleFile.FallbackRequired, Is.True);
        Assert.That(result.Contents, Is.Not.Empty);
        var (resultMimeType, _) = MimeTypeHelper.GetMimeTypeAndExtension(result.Contents, sampleFile.Name);
        Assert.That(resultMimeType, Is.EqualTo(MimeTypeMap.IMAGE.PNG));
        await Verify(result.Contents, PngExtension, CreateFileVerifySettings(sampleFile));
    }

    [TestCaseSource(nameof(ReadSampleFiles), new object[] { OperationType.ThumbnailGeneration })]
    public async Task LocalPath_512x512WithoutFill_V2_ShouldGenerateExpectedResult(SampleFile sampleFile)
    {
        // Arrange
        var client = await GetThumbnailerClientAsync();
        var fileBytes = await ReadSampleFileBytesAsync(sampleFile);

        // Act
        var isSupported = await client.IsThumbnailGenerationSupportedAsync(sampleFile.ContentType);
        var result = await client.GenerateThumbnailAsync(
            fileBytes, fileName: sampleFile.Name, widthPx: 512, heightPx: 512, fill: false,
            fallback: sampleFile.FallbackRequired);

        // Assert
        Assert.That(isSupported || sampleFile.FallbackRequired, Is.True);
        Assert.That(result.Contents, Is.Not.Empty);
        var (resultMimeType, _) = MimeTypeHelper.GetMimeTypeAndExtension(result.Contents, sampleFile.Name);
        Assert.That(resultMimeType, Is.EqualTo(MimeTypeMap.IMAGE.PNG));
        await Verify(result.Contents, PngExtension, CreateFileVerifySettings(sampleFile));
    }

    [TestCaseSource(nameof(ReadSampleFilesWithImagePlaceholderAlgorithm), new object[] { OperationType.ThumbnailGeneration })]
    public async Task LocalPath_WithImagePlaceholderAlgorithm_V2_ShouldGenerateExpectedImagePlaceholder((SampleFile SampleFile, ImagePlaceholderAlgorithm ImagePlaceholderAlgorithm) sampleFileWithAlgorithm)
    {
        // Arrange
        var client = await GetThumbnailerClientAsync();
        var sampleFile = sampleFileWithAlgorithm.SampleFile;
        var imagePlaceholderAlgorithm = sampleFileWithAlgorithm.ImagePlaceholderAlgorithm;
        var fileBytes = await ReadSampleFileBytesAsync(sampleFile);

        // Act
        var result = await client.GenerateThumbnailAsync(
            fileBytes, fileName: sampleFile.Name, fallback: sampleFile.FallbackRequired, imagePlaceholderAlgorithm: imagePlaceholderAlgorithm);

        // Assert
        Assert.That(result.ImagePlaceholder, Is.Not.Null.Or.Empty);
        await VerifyImagePlaceholderAsync(sampleFile, result.ImagePlaceholder!, imagePlaceholderAlgorithm);
    }

    [TestCaseSource(nameof(ReadSampleFiles), new object[] { OperationType.ThumbnailGeneration })]
    public async Task RemoteUri_256x256WithFill_V1_ShouldGenerateExpectedResult(SampleFile sampleFile)
    {
        // Arrange
        var client = await GetThumbnailerClientAsync();
        var clientConfiguration = Services.ServiceProvider.GetRequiredService<IOptions<ThumbnailerClientConfiguration>>();

        using var httpClient = Services.GetHttpClient();
        var apiClient = new ThumbnailGenerationApiClient(httpClient)
        {
            ApiKey = clientConfiguration.Value.ApiKey
        };

        // Act
        var isSupported = await client.IsThumbnailGenerationSupportedAsync(sampleFile.ContentType);
        var response = await apiClient.GenerateThumbnailFromUriV1Async(sampleFile.RemoteUri,
            widthPx: null, heightPx: null, sidePx: null, shavePx: null, fill: null, smartCrop: null,
            fallback: sampleFile.FallbackRequired, imagePlaceholderAlgorithm: null, responseType: null, openInBrowser: null);
        var resultBytes = await ConvertFileResponseToByteArrayAsync(response);

        // Assert
        Assert.That(isSupported || sampleFile.FallbackRequired, Is.True);
        Assert.That(resultBytes, Is.Not.Empty);
        var (resultMimeType, _) = MimeTypeHelper.GetMimeTypeAndExtension(resultBytes, sampleFile.Name);
        Assert.That(resultMimeType, Is.EqualTo(MimeTypeMap.IMAGE.PNG));
        await Verify(resultBytes, PngExtension, CreateFileVerifySettings(sampleFile));
    }

    [TestCaseSource(nameof(ReadSampleFiles), new object[] { OperationType.ThumbnailGeneration })]
    public async Task RemoteUri_256x256WithFill_V2_ShouldGenerateExpectedResult(SampleFile sampleFile)
    {
        // Arrange
        var client = await GetThumbnailerClientAsync();

        // Act
        var isSupported = await client.IsThumbnailGenerationSupportedAsync(sampleFile.ContentType);
        var result = await client.GenerateThumbnailAsync(
            sampleFile.RemoteUri, fallback: sampleFile.FallbackRequired);

        // Assert
        Assert.That(isSupported || sampleFile.FallbackRequired, Is.True);
        Assert.That(result.Contents, Is.Not.Empty);
        var (resultMimeType, _) = MimeTypeHelper.GetMimeTypeAndExtension(result.Contents, sampleFile.Name);
        Assert.That(resultMimeType, Is.EqualTo(MimeTypeMap.IMAGE.PNG));
        await Verify(result.Contents, PngExtension, CreateFileVerifySettings(sampleFile));
    }

    [TestCaseSource(nameof(ReadSampleFiles), new object[] { OperationType.ThumbnailGeneration })]
    public async Task RemoteUri_256x256WithSmartCrop_V2_ShouldGenerateExpectedResult(SampleFile sampleFile)
    {
        // Arrange
        var client = await GetThumbnailerClientAsync();

        // Act
        var isSupported = await client.IsThumbnailGenerationSupportedAsync(sampleFile.ContentType);
        var result = await client.GenerateThumbnailAsync(
            sampleFile.RemoteUri, smartCrop: true,
            fallback: sampleFile.FallbackRequired);

        // Assert
        Assert.That(isSupported || sampleFile.FallbackRequired, Is.True);
        Assert.That(result.Contents, Is.Not.Empty);
        var (resultMimeType, _) = MimeTypeHelper.GetMimeTypeAndExtension(result.Contents, sampleFile.Name);
        Assert.That(resultMimeType, Is.EqualTo(MimeTypeMap.IMAGE.PNG));
        await Verify(result.Contents, PngExtension, CreateFileVerifySettings(sampleFile));
    }

    [TestCaseSource(nameof(ReadSampleFiles), new object[] { OperationType.ThumbnailGeneration })]
    public async Task RemoteUri_384x384ShavedOff42_V2_ShouldGenerateExpectedResult(SampleFile sampleFile)
    {
        // Arrange
        var client = await GetThumbnailerClientAsync();

        // Act
        var isSupported = await client.IsThumbnailGenerationSupportedAsync(sampleFile.ContentType);
        var result = await client.GenerateThumbnailAsync(
            sampleFile.RemoteUri, widthPx: 384, heightPx: 384, shavePx: 42,
            fallback: sampleFile.FallbackRequired);

        // Assert
        Assert.That(isSupported || sampleFile.FallbackRequired, Is.True);
        Assert.That(result.Contents, Is.Not.Empty);
        var (resultMimeType, _) = MimeTypeHelper.GetMimeTypeAndExtension(result.Contents, sampleFile.Name);
        Assert.That(resultMimeType, Is.EqualTo(MimeTypeMap.IMAGE.PNG));
        await Verify(result.Contents, PngExtension, CreateFileVerifySettings(sampleFile));
    }

    [TestCaseSource(nameof(ReadSampleFiles), new object[] { OperationType.ThumbnailGeneration })]
    public async Task RemoteUri_512x512WithoutFill_V2_ShouldGenerateExpectedResult(SampleFile sampleFile)
    {
        // Arrange
        var client = await GetThumbnailerClientAsync();

        // Act
        var isSupported = await client.IsThumbnailGenerationSupportedAsync(sampleFile.ContentType);
        var result = await client.GenerateThumbnailAsync(
            sampleFile.RemoteUri, widthPx: 512, heightPx: 512, fill: false,
            fallback: sampleFile.FallbackRequired);

        // Assert
        Assert.That(isSupported || sampleFile.FallbackRequired, Is.True);
        Assert.That(result.Contents, Is.Not.Empty);
        var (resultMimeType, _) = MimeTypeHelper.GetMimeTypeAndExtension(result.Contents, sampleFile.Name);
        Assert.That(resultMimeType, Is.EqualTo(MimeTypeMap.IMAGE.PNG));
        await Verify(result.Contents, PngExtension, CreateFileVerifySettings(sampleFile));
    }

    [TestCaseSource(nameof(ReadSampleFilesWithImagePlaceholderAlgorithm), new object[] { OperationType.ThumbnailGeneration })]
    public async Task RemoteUri_WithImagePlaceholderAlgorithm_V2_ShouldGenerateExpectedImagePlaceholder((SampleFile SampleFile, ImagePlaceholderAlgorithm ImagePlaceholderAlgorithm) sampleFileWithAlgorithm)
    {
        // Arrange
        var client = await GetThumbnailerClientAsync();
        var sampleFile = sampleFileWithAlgorithm.SampleFile;
        var imagePlaceholderAlgorithm = sampleFileWithAlgorithm.ImagePlaceholderAlgorithm;

        // Act
        var result = await client.GenerateThumbnailAsync(
            sampleFile.RemoteUri, fallback: sampleFile.FallbackRequired, imagePlaceholderAlgorithm: imagePlaceholderAlgorithm);

        // Assert
        Assert.That(result.ImagePlaceholder, Is.Not.Null.Or.Empty);
        await VerifyImagePlaceholderAsync(sampleFile, result.ImagePlaceholder!, imagePlaceholderAlgorithm);
    }

    [TestCaseSource(nameof(ReadSampleFilesByFormat), new object[] { OperationType.ThumbnailGeneration })]
    public async Task Parallel_512x512WithoutFill_V2_ShouldGenerateExpectedResult(IEnumerable<SampleFile> sampleFiles)
    {
        // Arrange
        var client = await GetThumbnailerClientAsync();

        // Act
        var fileResults = new ConcurrentBag<(SampleFile SampleFile, FileResult Result)>();
        await Parallel.ForEachAsync(sampleFiles, async (sampleFile, ct) =>
        {
            var result = await client.GenerateThumbnailAsync(
                sampleFile.RemoteUri, widthPx: 512, heightPx: 512, fill: false,
                fallback: sampleFile.FallbackRequired, cancellationToken: ct);
            fileResults.Add((sampleFile, result));
        });

        // Assert
        foreach (var r in fileResults)
        {
            Assert.That(r.Result.Contents, Is.Not.Empty);
            var (resultMimeType, _) = MimeTypeHelper.GetMimeTypeAndExtension(r.Result.Contents, r.SampleFile.Name);
            Assert.That(resultMimeType, Is.EqualTo(MimeTypeMap.IMAGE.PNG));
            await Verify(r.Result.Contents, PngExtension, CreateFileVerifySettings(r.SampleFile));
        }
    }
}
