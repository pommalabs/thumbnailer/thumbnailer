﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System.Net;
using System.Net.Http.Json;
using AngleSharp;
using AngleSharp.Html.Dom;
using PommaLabs.Salatino.Endpoints;
using PommaLabs.Salatino.Models.DataTransferObjects;
using PommaLabs.Thumbnailer.Models.DTO.Internal;

namespace PommaLabs.Thumbnailer.IntegrationTests;

internal sealed class WebsiteTests : ThumbnailerTestsBase
{
    private const string ConvertFairUseTokenToApiKeyEndpoint = "/fair-use-token-to-api-key";
    private const string OptimizePage = "/optimize";
    private const string ThumbnailPage = "/thumbnail";

    [TestCase(""), TestCase(" "), TestCase("invalid")]
    public async Task FairUseTokenToApiKey_InvalidToken_RespondsWith200AndInvalidApiKey(string invalidToken)
    {
        // Arrange
        using var client = Services.GetHttpClient();
        var response = await client.GetAsync(OptimizePage);
        var html = await response.Content.ReadAsStringAsync();

        var browsingConfig = Configuration.Default;
        using var browsingContext = BrowsingContext.New(browsingConfig);
        using var htmlDoc = await browsingContext.OpenAsync(req => req.Content(html));

        var antiforgeryToken = (htmlDoc.QuerySelector("input[name='__RequestVerificationToken']") as IHtmlInputElement)!.Value;
        var conversionRequest = new FormUrlEncodedContent(new Dictionary<string, string>
        {
            { "__RequestVerificationToken", antiforgeryToken },
            { "token", invalidToken }
        });

        // Act
        response = await client.PostAsync(ConvertFairUseTokenToApiKeyEndpoint, conversionRequest);

        // Assert
        Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.OK));
        var apiKey = await response.Content.ReadFromJsonAsync<ApiKeyCredentials>();
        Assert.That(apiKey, Is.Not.Null);
        Assert.That(apiKey!.IsValid, Is.False);
    }

    [Test]
    public async Task FairUseTokenToApiKey_ValidToken_RespondsWith200AndValidApiKey()
    {
        // Arrange
        using var client = Services.GetHttpClient();
        var response = await client.GetAsync(OptimizePage);
        var html = await response.Content.ReadAsStringAsync();

        var browsingConfig = Configuration.Default;
        using var browsingContext = BrowsingContext.New(browsingConfig);
        using var htmlDoc = await browsingContext.OpenAsync(req => req.Content(html));

        var antiforgeryToken = (htmlDoc.QuerySelector("input[name='__RequestVerificationToken']") as IHtmlInputElement)!.Value;

        response = await client.GetAsync(FairUseTokenEndpoints.GetFairUseTokenRoute);
        var fairUseToken = await response.Content.ReadFromJsonAsync<FairUseToken>();
        var conversionRequest = new FormUrlEncodedContent(new Dictionary<string, string>
        {
            { "__RequestVerificationToken", antiforgeryToken },
            { "token", fairUseToken!.Token }
        });

        // Act
        response = await client.PostAsync(ConvertFairUseTokenToApiKeyEndpoint, conversionRequest);

        // Assert
        Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.OK));
        var apiKey = await response.Content.ReadFromJsonAsync<ApiKeyCredentials>();
        Assert.That(apiKey, Is.Not.Null);
        Assert.That(apiKey!.IsValid, Is.True);
    }

    [Test]
    public async Task FairUseTokenToApiKey_ValidToken_Twice_RespondsWith200AndInvalidApiKey()
    {
        // Arrange
        using var client = Services.GetHttpClient();
        var response = await client.GetAsync(OptimizePage);
        var html = await response.Content.ReadAsStringAsync();

        var browsingConfig = Configuration.Default;
        using var browsingContext = BrowsingContext.New(browsingConfig);
        using var htmlDoc = await browsingContext.OpenAsync(req => req.Content(html));

        var antiforgeryToken = (htmlDoc.QuerySelector("input[name='__RequestVerificationToken']") as IHtmlInputElement)!.Value;

        response = await client.GetAsync(FairUseTokenEndpoints.GetFairUseTokenRoute);
        var fairUseToken = await response.Content.ReadFromJsonAsync<FairUseToken>();
        var conversionRequest = new FormUrlEncodedContent(new Dictionary<string, string>
        {
            { "__RequestVerificationToken", antiforgeryToken },
            { "token", fairUseToken!.Token }
        });

        // Act
        await client.PostAsync(ConvertFairUseTokenToApiKeyEndpoint, conversionRequest);
        response = await client.PostAsync(ConvertFairUseTokenToApiKeyEndpoint, conversionRequest);

        // Assert
        Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.OK));
        var apiKey = await response.Content.ReadFromJsonAsync<ApiKeyCredentials>();
        Assert.That(apiKey, Is.Not.Null);
        Assert.That(apiKey!.IsValid, Is.False);
    }

    [Test]
    public async Task Health_Get_RespondsWith200()
    {
        // Arrange
        using var client = Services.GetHttpClient();

        // Act
        var response = await client.GetAsync("/health");

        // Assert
        Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.OK));
    }

    [TestCase("/"), TestCase(OptimizePage), TestCase(ThumbnailPage)]
    public async Task Page_Get_RespondsWith200(string page)
    {
        // Arrange
        using var client = Services.GetHttpClient();

        // Act
        var response = await client.GetAsync(page);

        // Assert
        Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.OK));
    }

    [TestCase("/index.html"), TestCase("/schema/swagger.json")]
    public async Task Swagger_Get_RespondsWith200(string endpoint)
    {
        // Arrange
        using var client = Services.GetHttpClient();

        // Act
        var response = await client.GetAsync("/swagger" + endpoint);

        // Assert
        Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.OK));
    }

    [Test]
    public async Task Version_Get_RespondsWith200()
    {
        // Arrange
        using var client = Services.GetHttpClient();

        // Act
        var response = await client.GetAsync("/version");

        // Assert
        Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.OK));
    }
}
