﻿// Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

// 80 CHARSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS

using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Dasync.Collections;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using PommaLabs.Thumbnailer.Client;
using PommaLabs.Thumbnailer.Client.Core;
using PommaLabs.Thumbnailer.Client.Models.Enumerations;

namespace PommaLabs.Thumbnailer.FunctionalTests
{
    /// <summary>
    ///   Tests common endpoints of Thumbnailer service.
    /// </summary>
    public class Program
    {
        private const string LosslessOptimizationSuffix = "optimized.lossless";
        private const string LossyOptimizationSuffix = "optimized.lossy";
        private const string SamplesDirectoryPath = "Samples";
        private const string SpecFileName = "_spec.json";
        private const string ThumbnailSuffix = "thumbnail";

        private static int s_concurrentRequestCount;
        private static ILogger<Program>? s_logger;
        private static bool s_persistOutput;
        private static IThumbnailerClient? s_thumbnailerClient;

        /// <summary>
        ///   Tests common endpoints of Thumbnailer service.
        /// </summary>
        /// <param name="baseUri">Base URI.</param>
        /// <param name="apiKey">API key.</param>
        /// <param name="persistOutput">True if test output should be persisted, false otherwise.</param>
        /// <param name="concurrentRequestCount">How many concurrent requests can be made.</param>
        /// <returns><c>0</c> if everything run OK, <c>1</c> otherwise.</returns>
        public static async Task<int> Main(
            Uri? baseUri = default, string? apiKey = default,
            bool persistOutput = default, int concurrentRequestCount = -1)
        {
            using var serviceProvider = ConfigureServices(baseUri, apiKey);

            s_thumbnailerClient = serviceProvider.GetRequiredService<IThumbnailerClient>();
            s_logger = serviceProvider.GetRequiredService<ILogger<Program>>();
            s_persistOutput = persistOutput;
            s_concurrentRequestCount = (concurrentRequestCount == -1) ? Environment.ProcessorCount : concurrentRequestCount;

            var result = await RunTests();

            return result ? 0 : 1;
        }

        private static ServiceProvider ConfigureServices(Uri? baseUri, string? apiKey)
        {
            var services = new ServiceCollection();

            services.Configure<ThumbnailerClientConfiguration>(options =>
            {
                if (baseUri != null) options.BaseUri = baseUri;
                if (apiKey != null) options.ApiKey = apiKey;
            });

            services.AddLogging(options => options.AddConsole().SetMinimumLevel(LogLevel.Trace));

            services.AddThumbnailerClient();

            return services.BuildServiceProvider();
        }

        private static async Task PersistTestResultAsync(TestFile file, string suffix, byte[] outputBytes, string outputExtension)
        {
            if (!s_persistOutput)
            {
                return;
            }

            var outputName = $"{file.Number}.{suffix}.{outputExtension}";
            var outputPath = Path.Combine(Path.GetDirectoryName(file.Path)!, outputName);
            await File.WriteAllBytesAsync(outputPath, outputBytes);
        }

        private static SpecFile ReadSpecFile(string fileFormatDirectory)
        {
            var specFileContents = File.ReadAllText(Path.Combine(fileFormatDirectory, SpecFileName));
            return JsonConvert.DeserializeObject<SpecFile>(specFileContents);
        }

        private static IEnumerable<TestFile> ReadTestFiles()
        {
            var testFiles = new ConcurrentBag<TestFile>();

            foreach (var fileFormatDirectory in Directory.GetDirectories(SamplesDirectoryPath))
            {
                var specFile = ReadSpecFile(fileFormatDirectory);

                var filePaths = Directory
                    .GetFiles(fileFormatDirectory)
                    .Where(fp => !fp.EndsWith(SpecFileName))
                    .Where(fp => !fp.Contains(LosslessOptimizationSuffix, StringComparison.OrdinalIgnoreCase))
                    .Where(fp => !fp.Contains(LossyOptimizationSuffix, StringComparison.OrdinalIgnoreCase))
                    .Where(fp => !fp.Contains(ThumbnailSuffix, StringComparison.OrdinalIgnoreCase));

                Parallel.ForEach(filePaths, new ParallelOptions { MaxDegreeOfParallelism = s_concurrentRequestCount }, filePath =>
                {
                    testFiles.Add(new TestFile
                    {
                        Path = filePath,
                        ContentType = MimeTypeHelper.GetMimeType(filePath),
                        Format = Path.GetFileName(fileFormatDirectory).ToUpperInvariant(),
                        Number = Path.GetFileNameWithoutExtension(filePath),
                        ThumbnailGenerationSupported = specFile.ThumbnailGenerationSupported,
                        MediaOptimizationSupported = specFile.MediaOptimizationSupported
                    });
                });
            }

            return testFiles;
        }

        private static async Task<bool> RunTests()
        {
            s_logger.LogInformation("Current directory is: {CurrentDirectory}", Directory.GetCurrentDirectory());

            var testFiles = ReadTestFiles()
                .OrderBy(x => x.Format)
                .ThenBy(x => x.Number)
                .ToArray();

            var failures = 0;

            await testFiles.ParallelForEachAsync(async testFile =>
            {
                s_logger.LogInformation("Processing test file {FileCode}...", testFile.Code);

                if (!await TryMediaOptimizationAsync(testFile) || !await TryThumbnailGenerationAsync(testFile))
                {
                    Interlocked.Increment(ref failures);
                }
            }, s_concurrentRequestCount, false);

            return failures == 0;
        }

        private static async Task<bool> TryMediaOptimizationAsync(TestFile file)
        {
            var stopwatch = new Stopwatch();
            var supported = await s_thumbnailerClient!.IsMediaOptimizationSupportedAsync(file.ContentType);

            if (supported && file.MediaOptimizationSupported)
            {
                var fileBytes = await File.ReadAllBytesAsync(file.Path);

                stopwatch.Restart();
                try
                {
                    var outputBytes = await s_thumbnailerClient.OptimizeMediaAsync(fileBytes, file.ContentType, mode: OptimizationMode.Lossy);
                    await PersistTestResultAsync(file, LossyOptimizationSuffix, outputBytes, file.Format.ToLowerInvariant());

                    outputBytes = await s_thumbnailerClient.OptimizeMediaAsync(fileBytes, file.ContentType, mode: OptimizationMode.Lossless);
                    await PersistTestResultAsync(file, LosslessOptimizationSuffix, outputBytes, file.Format.ToLowerInvariant());
                }
                catch (Exception ex)
                {
                    s_logger.LogError(ex, "An error occurred while optimizing media file {FileCode}", file.Code);
                    return false;
                }
                finally
                {
                    stopwatch.Stop();
                    s_logger.LogInformation("media optimization completed in {ElapsedMilliseconds} ms", stopwatch.ElapsedMilliseconds);
                }
            }
            else if (supported || file.MediaOptimizationSupported)
            {
                // If we got here, file optimization is supported (or not) when it should not (or
                // should be). Then, we need to log the error and mark the test as failed.
                s_logger.LogError(
                    "{FileFormat} ({ContentType}) format support by media optimization does not match between client and test expectations: client says {ClientSupport}, test says {TestSupport}",
                    file.Format, file.ContentType, supported, file.MediaOptimizationSupported);

                return false;
            }

            return true;
        }

        private static async Task<bool> TryThumbnailGenerationAsync(TestFile file)
        {
            var stopwatch = new Stopwatch();
            var supported = await s_thumbnailerClient!.IsThumbnailGenerationSupportedAsync(file.ContentType);

            if (supported && file.ThumbnailGenerationSupported)
            {
                var fileBytes = await File.ReadAllBytesAsync(file.Path);

                stopwatch.Restart();
                try
                {
                    var outputBytes = await s_thumbnailerClient.GenerateThumbnailAsync(fileBytes, file.ContentType);
                    await PersistTestResultAsync(file, ThumbnailSuffix, outputBytes, "png");
                }
                catch (Exception ex)
                {
                    s_logger.LogError(ex, "An error occurred while generating thumbnail of file {FileCode}", file.Code);
                    return false;
                }
                finally
                {
                    stopwatch.Stop();
                    s_logger.LogInformation("Thumbnail generation completed in {ElapsedMilliseconds} ms", stopwatch.ElapsedMilliseconds);
                }
            }
            else if (supported || file.ThumbnailGenerationSupported)
            {
                // If we got here, thumbnail generation is supported (or not) when it should not (or
                // should be). Then, we need to log the error and mark the test as failed.
                s_logger.LogError(
                    "{FileFormat} ({ContentType}) format support by thumbnail generation does not match between client and test expectations: client says {ClientSupport}, test says {TestSupport}",
                    file.Format, file.ContentType, supported, file.ThumbnailGenerationSupported);

                return false;
            }

            return true;
        }

        private class SpecFile
        {
            public bool MediaOptimizationSupported { get; set; }

            public bool ThumbnailGenerationSupported { get; set; }
        }

        private sealed class TestFile : SpecFile
        {
            public string Code => $"{Format}#{Number}";

            public string ContentType { get; set; } = string.Empty;

            public string Format { get; set; } = string.Empty;

            public string Number { get; set; } = string.Empty;

            public string Path { get; set; } = string.Empty;
        }
    }
}
