﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System.Collections.Concurrent;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using PommaLabs.MimeTypes;
using PommaLabs.Thumbnailer.Client;
using PommaLabs.Thumbnailer.Client.Core;
using PommaLabs.Thumbnailer.Client.Models.Enumerations;
using PommaLabs.Thumbnailer.Client.Models.Results;
using FileDetails = PommaLabs.Thumbnailer.Client.Core.FileDetails;
using ImagePlaceholderAlgorithm = PommaLabs.Thumbnailer.Client.Models.Enumerations.ImagePlaceholderAlgorithm;
using JobDetails = PommaLabs.Thumbnailer.Client.Core.JobDetails;

namespace PommaLabs.Thumbnailer.IntegrationTests;

internal sealed class MediaOptimizationTests : ThumbnailerTestsBase
{
    private static readonly HashSet<string> s_unverifiableExtensions =
    [
        MimeTypeMap.Extensions.MP4,
    ];

    [SetUp]
    public void SetUp()
    {
        var logger = Services.ServiceProvider.GetRequiredService<ILogger<MediaOptimizationTests>>();
        logger.LogTrace("Running media optimization test {TestName}...", TestContext.CurrentContext.Test.Name);
    }

    [TestCaseSource(nameof(ReadSampleFiles), new object[] { OperationType.MediaOptimization })]
    public async Task Base64_ValidMedia_V1_ShouldGenerateExpectedResult(SampleFile sampleFile)
    {
        // Arrange
        var client = await GetThumbnailerClientAsync();
        var clientConfiguration = Services.ServiceProvider.GetRequiredService<IOptions<ThumbnailerClientConfiguration>>();
        var fileBytes = await ReadSampleFileBytesAsync(sampleFile);

        using var httpClient = Services.GetHttpClient();
        var apiClient = new MediaOptimizationApiClient(httpClient)
        {
            ApiKey = clientConfiguration.Value.ApiKey
        };

        // Act
        var isSupported = await client.IsMediaOptimizationSupportedAsync(sampleFile.ContentType);
        var response = await apiClient.OptimizeMediaFromJsonV1Async(
            imagePlaceholderAlgorithm: null, responseType: null, openInBrowser: null,
            body: new FileDetails { FileName = sampleFile.Name, Contents = fileBytes, ContentType = sampleFile.ContentType });
        var resultBytes = await ConvertFileResponseToByteArrayAsync(response);

        // Assert
        Assert.That(isSupported, Is.True);
        Assert.That(resultBytes, Is.Not.Empty);
        var (resultMimeType, resultExtension) = MimeTypeHelper.GetMimeTypeAndExtension(resultBytes, sampleFile.Name);
        Assert.That(resultMimeType, Is.EqualTo(sampleFile.ContentType));
        if (!s_unverifiableExtensions.Contains(resultExtension))
        {
            await Verify(resultBytes, sampleFile.Extension, CreateFileVerifySettings(sampleFile));
        }
    }

    [TestCaseSource(nameof(ReadSampleFiles), new object[] { OperationType.MediaOptimization })]
    public async Task Base64_ValidMedia_V2_ShouldGenerateExpectedResult(SampleFile sampleFile)
    {
        // Arrange
        var client = await GetThumbnailerClientAsync();
        var clientImplementation = (StandardThumbnailerClient)client;
        var clientConfiguration = Services.ServiceProvider.GetRequiredService<IOptions<ThumbnailerClientConfiguration>>();
        var fileBytes = await ReadSampleFileBytesAsync(sampleFile);

        using var httpClient = Services.GetHttpClient();
        var apiClient = new MediaOptimizationApiClient(httpClient)
        {
            ApiKey = clientConfiguration.Value.ApiKey
        };

        // Act
        var isSupported = await client.IsMediaOptimizationSupportedAsync(sampleFile.ContentType);
        var job = await apiClient.OptimizeMediaFromJsonV2Async(imagePlaceholderAlgorithm: null,
            body: new FileDetails { FileName = sampleFile.Name, Contents = fileBytes, ContentType = sampleFile.ContentType });

        var clientJob = new JobDetails { JobId = job.JobId };
        await clientImplementation.CheckJobStatusAsync(clientJob, default);
        var result = await clientImplementation.DownloadFileResultAsync(clientJob, default);

        // Assert
        Assert.That(isSupported, Is.True);
        Assert.That(result.Contents, Is.Not.Empty);
        var (resultMimeType, resultExtension) = MimeTypeHelper.GetMimeTypeAndExtension(result.Contents, sampleFile.Name);
        Assert.That(resultMimeType, Is.EqualTo(sampleFile.ContentType));
        if (!s_unverifiableExtensions.Contains(resultExtension))
        {
            await Verify(result.Contents, sampleFile.Extension, CreateFileVerifySettings(sampleFile));
        }
    }

    [TestCaseSource(nameof(ReadSampleFilesWithImagePlaceholderAlgorithm), new object[] { OperationType.MediaOptimization })]
    public async Task Base64_WithImagePlaceholderAlgorithm_V1_ShouldGenerateExpectedImagePlaceholder((SampleFile SampleFile, ImagePlaceholderAlgorithm ImagePlaceholderAlgorithm) sampleFileWithAlgorithm)
    {
        // Arrange
        var client = await GetThumbnailerClientAsync();
        var sampleFile = sampleFileWithAlgorithm.SampleFile;
        var imagePlaceholderAlgorithm = sampleFileWithAlgorithm.ImagePlaceholderAlgorithm;
        var clientConfiguration = Services.ServiceProvider.GetRequiredService<IOptions<ThumbnailerClientConfiguration>>();
        var fileBytes = await ReadSampleFileBytesAsync(sampleFile);

        using var httpClient = Services.GetHttpClient();
        var apiClient = new MediaOptimizationApiClient(httpClient)
        {
            ApiKey = clientConfiguration.Value.ApiKey
        };

        // Act
        var isSupported = await client.IsImagePlaceholderGenerationSupportedAsync(sampleFile.ContentType);
        var response = await apiClient.OptimizeMediaFromJsonV1Async(
            (Client.Core.ImagePlaceholderAlgorithm)imagePlaceholderAlgorithm, responseType: null, openInBrowser: null,
            body: new FileDetails { FileName = sampleFile.Name, Contents = fileBytes, ContentType = sampleFile.ContentType });

        // Assert
        if (isSupported)
        {
            response.Headers.TryGetValue(ImagePlaceholderHeaderName, out var imagePlaceholderHeaderValues);
            var imagePlaceholder = imagePlaceholderHeaderValues?.SingleOrDefault();
            Assert.That(imagePlaceholder, Is.Not.Null.Or.Empty);
            await VerifyImagePlaceholderAsync(sampleFile, imagePlaceholder!, imagePlaceholderAlgorithm);
        }
        else
        {
            Assert.That(response.Headers.ContainsKey(ImagePlaceholderHeaderName), Is.False);
        }
    }

    [TestCaseSource(nameof(ReadSampleFiles), new object[] { OperationType.MediaOptimization })]
    public async Task LocalPath_ValidMedia_V1_ShouldGenerateExpectedResult(SampleFile sampleFile)
    {
        // Arrange
        var client = await GetThumbnailerClientAsync();
        var clientConfiguration = Services.ServiceProvider.GetRequiredService<IOptions<ThumbnailerClientConfiguration>>();
        var fileBytes = await ReadSampleFileBytesAsync(sampleFile);

        using var httpClient = Services.GetHttpClient();
        var apiClient = new MediaOptimizationApiClient(httpClient)
        {
            ApiKey = clientConfiguration.Value.ApiKey
        };

        // Act
        var isSupported = await client.IsMediaOptimizationSupportedAsync(sampleFile.ContentType);
        var response = await apiClient.OptimizeMediaFromFileV1Async(
            imagePlaceholderAlgorithm: null, responseType: null, openInBrowser: null,
            new FileParameter(new MemoryStream(fileBytes), sampleFile.Name, sampleFile.ContentType));
        var resultBytes = await ConvertFileResponseToByteArrayAsync(response);

        // Assert
        Assert.That(isSupported, Is.True);
        Assert.That(resultBytes, Is.Not.Empty);
        var (resultMimeType, resultExtension) = MimeTypeHelper.GetMimeTypeAndExtension(resultBytes, sampleFile.Name);
        Assert.That(resultMimeType, Is.EqualTo(sampleFile.ContentType));
        if (!s_unverifiableExtensions.Contains(resultExtension))
        {
            await Verify(resultBytes, sampleFile.Extension, CreateFileVerifySettings(sampleFile));
        }
    }

    [TestCaseSource(nameof(ReadSampleFiles), new object[] { OperationType.MediaOptimization })]
    public async Task LocalPath_ValidMedia_V2_ShouldGenerateExpectedResult(SampleFile sampleFile)
    {
        // Arrange
        var client = await GetThumbnailerClientAsync();
        var fileBytes = await ReadSampleFileBytesAsync(sampleFile);

        // Act
        var isSupported = await client.IsMediaOptimizationSupportedAsync(sampleFile.ContentType);
        var result = await client.OptimizeMediaAsync(fileBytes, fileName: sampleFile.Name);

        // Assert
        Assert.That(isSupported, Is.True);
        Assert.That(result.Contents, Is.Not.Empty);
        var (resultMimeType, resultExtension) = MimeTypeHelper.GetMimeTypeAndExtension(result.Contents, sampleFile.Name);
        Assert.That(resultMimeType, Is.EqualTo(sampleFile.ContentType));
        if (!s_unverifiableExtensions.Contains(resultExtension))
        {
            await Verify(result.Contents, sampleFile.Extension, CreateFileVerifySettings(sampleFile));
        }
    }

    [TestCaseSource(nameof(ReadSampleFilesWithImagePlaceholderAlgorithm), new object[] { OperationType.MediaOptimization })]
    public async Task LocalPath_WithImagePlaceholderAlgorithm_V2_ShouldGenerateExpectedImagePlaceholder((SampleFile SampleFile, ImagePlaceholderAlgorithm ImagePlaceholderAlgorithm) sampleFileWithAlgorithm)
    {
        // Arrange
        var client = await GetThumbnailerClientAsync();
        var sampleFile = sampleFileWithAlgorithm.SampleFile;
        var imagePlaceholderAlgorithm = sampleFileWithAlgorithm.ImagePlaceholderAlgorithm;
        var fileBytes = await ReadSampleFileBytesAsync(sampleFile);

        // Act
        var isSupported = await client.IsImagePlaceholderGenerationSupportedAsync(sampleFile.ContentType);
        var result = await client.OptimizeMediaAsync(
            fileBytes, fileName: sampleFile.Name,
            imagePlaceholderAlgorithm: imagePlaceholderAlgorithm);

        // Assert
        if (isSupported)
        {
            Assert.That(result.ImagePlaceholder, Is.Not.Null.Or.Empty);
            await VerifyImagePlaceholderAsync(sampleFile, result.ImagePlaceholder!, imagePlaceholderAlgorithm);
        }
        else
        {
            Assert.That(result.ImagePlaceholder, Is.Null);
        }
    }

    [TestCaseSource(nameof(ReadSampleFiles), new object[] { OperationType.MediaOptimization })]
    public async Task RemoteUri_ValidMedia_V1_ShouldGenerateExpectedResult(SampleFile sampleFile)
    {
        // Arrange
        var client = await GetThumbnailerClientAsync();
        var clientConfiguration = Services.ServiceProvider.GetRequiredService<IOptions<ThumbnailerClientConfiguration>>();

        using var httpClient = Services.GetHttpClient();
        var apiClient = new MediaOptimizationApiClient(httpClient)
        {
            ApiKey = clientConfiguration.Value.ApiKey
        };

        // Act
        var isSupported = await client.IsMediaOptimizationSupportedAsync(sampleFile.ContentType);
        var response = await apiClient.OptimizeMediaFromUriV1Async(sampleFile.RemoteUri,
            imagePlaceholderAlgorithm: null, responseType: null, openInBrowser: null);
        var resultBytes = await ConvertFileResponseToByteArrayAsync(response);

        // Assert
        Assert.That(isSupported, Is.True);
        Assert.That(resultBytes, Is.Not.Empty);
        var (resultMimeType, resultExtension) = MimeTypeHelper.GetMimeTypeAndExtension(resultBytes, sampleFile.Name);
        Assert.That(resultMimeType, Is.EqualTo(sampleFile.ContentType));
        if (!s_unverifiableExtensions.Contains(resultExtension))
        {
            await Verify(resultBytes, sampleFile.Extension, CreateFileVerifySettings(sampleFile));
        }
    }

    [TestCaseSource(nameof(ReadSampleFiles), new object[] { OperationType.MediaOptimization })]
    public async Task RemoteUri_ValidMedia_V2_ShouldGenerateExpectedResult(SampleFile sampleFile)
    {
        // Arrange
        var client = await GetThumbnailerClientAsync();

        // Act
        var isSupported = await client.IsMediaOptimizationSupportedAsync(sampleFile.ContentType);
        var result = await client.OptimizeMediaAsync(sampleFile.RemoteUri);

        // Assert
        Assert.That(isSupported, Is.True);
        Assert.That(result.Contents, Is.Not.Empty);
        var (resultMimeType, resultExtension) = MimeTypeHelper.GetMimeTypeAndExtension(result.Contents, sampleFile.Name);
        Assert.That(resultMimeType, Is.EqualTo(sampleFile.ContentType));
        if (!s_unverifiableExtensions.Contains(resultExtension))
        {
            await Verify(result.Contents, sampleFile.Extension, CreateFileVerifySettings(sampleFile));
        }
    }

    [TestCaseSource(nameof(ReadSampleFilesWithImagePlaceholderAlgorithm), new object[] { OperationType.MediaOptimization })]
    public async Task RemoteUri_WithImagePlaceholderAlgorithm_V2_ShouldGenerateExpectedImagePlaceholder((SampleFile SampleFile, ImagePlaceholderAlgorithm ImagePlaceholderAlgorithm) sampleFileWithAlgorithm)
    {
        // Arrange
        var client = await GetThumbnailerClientAsync();
        var sampleFile = sampleFileWithAlgorithm.SampleFile;
        var imagePlaceholderAlgorithm = sampleFileWithAlgorithm.ImagePlaceholderAlgorithm;

        // Act
        var isSupported = await client.IsImagePlaceholderGenerationSupportedAsync(sampleFile.ContentType);
        var result = await client.OptimizeMediaAsync(sampleFile.RemoteUri, imagePlaceholderAlgorithm);

        // Assert
        if (isSupported)
        {
            Assert.That(result.ImagePlaceholder, Is.Not.Null.Or.Empty);
            await VerifyImagePlaceholderAsync(sampleFile, result.ImagePlaceholder!, imagePlaceholderAlgorithm);
        }
        else
        {
            Assert.That(result.ImagePlaceholder, Is.Null);
        }
    }

    [TestCaseSource(nameof(ReadSampleFilesByFormat), new object[] { OperationType.MediaOptimization })]
    public async Task Parallel_ValidMedia_V2_ShouldGenerateExpectedResult(IEnumerable<SampleFile> sampleFiles)
    {
        // Arrange
        var client = await GetThumbnailerClientAsync();

        // Act
        var fileResults = new ConcurrentBag<(SampleFile SampleFile, FileResult Result)>();
        await Parallel.ForEachAsync(sampleFiles, async (sampleFile, ct) =>
        {
            var result = await client.OptimizeMediaAsync(sampleFile.RemoteUri, cancellationToken: ct);
            fileResults.Add((sampleFile, result));
        });

        // Assert
        foreach (var r in fileResults)
        {
            Assert.That(r.Result.Contents, Is.Not.Empty);
            var (resultMimeType, resultExtension) = MimeTypeHelper.GetMimeTypeAndExtension(r.Result.Contents, r.SampleFile.Name);
            Assert.That(resultMimeType, Is.EqualTo(r.SampleFile.ContentType));
            if (!s_unverifiableExtensions.Contains(resultExtension))
            {
                await Verify(r.Result.Contents, r.SampleFile.Extension, CreateFileVerifySettings(r.SampleFile));
            }
        }
    }
}
