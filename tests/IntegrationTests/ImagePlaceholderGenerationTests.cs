﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using PommaLabs.Thumbnailer.Client;
using PommaLabs.Thumbnailer.Client.Core;
using PommaLabs.Thumbnailer.Client.Models.Enumerations;
using FileDetails = PommaLabs.Thumbnailer.Client.Core.FileDetails;
using ImagePlaceholderAlgorithm = PommaLabs.Thumbnailer.Client.Models.Enumerations.ImagePlaceholderAlgorithm;
using JobDetails = PommaLabs.Thumbnailer.Client.Core.JobDetails;

namespace PommaLabs.Thumbnailer.IntegrationTests;

internal sealed class ImagePlaceholderGenerationTests : ThumbnailerTestsBase
{
    [SetUp]
    public void SetUp()
    {
        var logger = Services.ServiceProvider.GetRequiredService<ILogger<ImagePlaceholderGenerationTests>>();
        logger.LogTrace("Running image placeholder generation test {TestName}...", TestContext.CurrentContext.Test.Name);
    }

    [TestCaseSource(nameof(ReadSampleFilesWithImagePlaceholderAlgorithm), new object[] { OperationType.ImagePlaceholderGeneration })]
    public async Task Base64_V1_ShouldGenerateExpectedPlaceholder((SampleFile SampleFile, ImagePlaceholderAlgorithm ImagePlaceholderAlgorithm) sampleFileWithAlgorithm)
    {
        // Arrange
        var sampleFile = sampleFileWithAlgorithm.SampleFile;
        var imagePlaceholderAlgorithm = sampleFileWithAlgorithm.ImagePlaceholderAlgorithm;
        var client = await GetThumbnailerClientAsync();
        var clientConfiguration = Services.ServiceProvider.GetRequiredService<IOptions<ThumbnailerClientConfiguration>>();
        var fileBytes = await ReadSampleFileBytesAsync(sampleFile);

        using var httpClient = Services.GetHttpClient();
        var apiClient = new ImagePlaceholderGenerationApiClient(httpClient)
        {
            ApiKey = clientConfiguration.Value.ApiKey
        };

        // Act
        var isSupported = await client.IsImagePlaceholderGenerationSupportedAsync(sampleFile.ContentType);
        var result = await apiClient.GenerateImagePlaceholderFromJsonV1Async(
            (Client.Core.ImagePlaceholderAlgorithm)imagePlaceholderAlgorithm,
            new FileDetails { FileName = sampleFile.Name, Contents = fileBytes, ContentType = sampleFile.ContentType });

        //Assert
        Assert.That(isSupported, Is.True);
        Assert.That(result.Placeholder, Is.Not.Null.Or.Empty);
        await VerifyImagePlaceholderAsync(sampleFile, result.Placeholder!, imagePlaceholderAlgorithm);
    }

    [TestCaseSource(nameof(ReadSampleFilesWithImagePlaceholderAlgorithm), new object[] { OperationType.ImagePlaceholderGeneration })]
    public async Task Base64_V2_ShouldGenerateExpectedPlaceholder((SampleFile SampleFile, ImagePlaceholderAlgorithm ImagePlaceholderAlgorithm) sampleFileWithAlgorithm)
    {
        // Arrange
        var sampleFile = sampleFileWithAlgorithm.SampleFile;
        var imagePlaceholderAlgorithm = sampleFileWithAlgorithm.ImagePlaceholderAlgorithm;
        var client = await GetThumbnailerClientAsync();
        var clientImplementation = (StandardThumbnailerClient)client;
        var clientConfiguration = Services.ServiceProvider.GetRequiredService<IOptions<ThumbnailerClientConfiguration>>();
        var fileBytes = await ReadSampleFileBytesAsync(sampleFile);

        using var httpClient = Services.GetHttpClient();
        var apiClient = new ImagePlaceholderGenerationApiClient(httpClient)
        {
            ApiKey = clientConfiguration.Value.ApiKey
        };

        // Act
        var isSupported = await client.IsImagePlaceholderGenerationSupportedAsync(sampleFile.ContentType);
        var job = await apiClient.GenerateImagePlaceholderFromJsonV2Async(
            (Client.Core.ImagePlaceholderAlgorithm)imagePlaceholderAlgorithm,
            new FileDetails { FileName = sampleFile.Name, Contents = fileBytes, ContentType = sampleFile.ContentType });

        var clientJob = new JobDetails { JobId = job.JobId };
        await clientImplementation.CheckJobStatusAsync(clientJob, default).ConfigureAwait(false);
        var result = await clientImplementation.DownloadImagePlaceholderResultAsync(clientJob, default);

        // Assert
        Assert.That(isSupported, Is.True);
        Assert.That(result.Placeholder, Is.Not.Null.Or.Empty);
        await VerifyImagePlaceholderAsync(sampleFile, result.Placeholder, imagePlaceholderAlgorithm);
    }

    [TestCaseSource(nameof(ReadSampleFilesWithImagePlaceholderAlgorithm), new object[] { OperationType.ImagePlaceholderGeneration })]
    public async Task LocalPath_V1_ShouldGenerateExpectedPlaceholder((SampleFile SampleFile, ImagePlaceholderAlgorithm ImagePlaceholderAlgorithm) sampleFileWithAlgorithm)
    {
        // Arrange
        var sampleFile = sampleFileWithAlgorithm.SampleFile;
        var imagePlaceholderAlgorithm = sampleFileWithAlgorithm.ImagePlaceholderAlgorithm;
        var client = await GetThumbnailerClientAsync();
        var clientConfiguration = Services.ServiceProvider.GetRequiredService<IOptions<ThumbnailerClientConfiguration>>();
        var fileBytes = await ReadSampleFileBytesAsync(sampleFile);

        using var httpClient = Services.GetHttpClient();
        var apiClient = new ImagePlaceholderGenerationApiClient(httpClient)
        {
            ApiKey = clientConfiguration.Value.ApiKey
        };

        // Act
        var isSupported = await client.IsImagePlaceholderGenerationSupportedAsync(sampleFile.ContentType);
        var result = await apiClient.GenerateImagePlaceholderFromFileV1Async(
            (Client.Core.ImagePlaceholderAlgorithm)imagePlaceholderAlgorithm,
            new FileParameter(new MemoryStream(fileBytes), sampleFile.Name, sampleFile.ContentType));

        //Assert
        Assert.That(isSupported, Is.True);
        Assert.That(result.Placeholder, Is.Not.Null.Or.Empty);
        await VerifyImagePlaceholderAsync(sampleFile, result.Placeholder!, imagePlaceholderAlgorithm);
    }

    [TestCaseSource(nameof(ReadSampleFilesWithImagePlaceholderAlgorithm), new object[] { OperationType.ImagePlaceholderGeneration })]
    public async Task LocalPath_V2_ShouldGenerateExpectedPlaceholder((SampleFile SampleFile, ImagePlaceholderAlgorithm ImagePlaceholderAlgorithm) sampleFileWithAlgorithm)
    {
        // Arrange
        var sampleFile = sampleFileWithAlgorithm.SampleFile;
        var imagePlaceholderAlgorithm = sampleFileWithAlgorithm.ImagePlaceholderAlgorithm;
        var client = await GetThumbnailerClientAsync();
        var fileBytes = await ReadSampleFileBytesAsync(sampleFile);

        // Act
        var isSupported = await client.IsImagePlaceholderGenerationSupportedAsync(sampleFile.ContentType);
        var result = await client.GenerateImagePlaceholderAsync(fileBytes, imagePlaceholderAlgorithm, sampleFile.ContentType, sampleFile.Name);

        //Assert
        Assert.That(isSupported, Is.True);
        Assert.That(result.Placeholder, Is.Not.Null.Or.Empty);
        await VerifyImagePlaceholderAsync(sampleFile, result.Placeholder, imagePlaceholderAlgorithm);
    }

    [TestCaseSource(nameof(ReadSampleFilesWithImagePlaceholderAlgorithm), new object[] { OperationType.ImagePlaceholderGeneration })]
    public async Task RemoteUri_V1_ShouldGenerateExpectedPlaceholder((SampleFile SampleFile, ImagePlaceholderAlgorithm ImagePlaceholderAlgorithm) sampleFileWithAlgorithm)
    {
        // Arrange
        var sampleFile = sampleFileWithAlgorithm.SampleFile;
        var imagePlaceholderAlgorithm = sampleFileWithAlgorithm.ImagePlaceholderAlgorithm;
        var client = await GetThumbnailerClientAsync();
        var clientConfiguration = Services.ServiceProvider.GetRequiredService<IOptions<ThumbnailerClientConfiguration>>();

        using var httpClient = Services.GetHttpClient();
        var apiClient = new ImagePlaceholderGenerationApiClient(httpClient)
        {
            ApiKey = clientConfiguration.Value.ApiKey
        };

        // Act
        var isSupported = await client.IsImagePlaceholderGenerationSupportedAsync(sampleFile.ContentType);
        var result = await apiClient.GenerateImagePlaceholderFromUriV1Async(sampleFile.RemoteUri, (Client.Core.ImagePlaceholderAlgorithm)imagePlaceholderAlgorithm);

        // Assert
        Assert.That(isSupported, Is.True);
        Assert.That(result.Placeholder, Is.Not.Null.Or.Empty);
        await VerifyImagePlaceholderAsync(sampleFile, result.Placeholder!, imagePlaceholderAlgorithm);
    }

    [TestCaseSource(nameof(ReadSampleFilesWithImagePlaceholderAlgorithm), new object[] { OperationType.ImagePlaceholderGeneration })]
    public async Task RemoteUri_V2_ShouldGenerateExpectedPlaceholder((SampleFile SampleFile, ImagePlaceholderAlgorithm ImagePlaceholderAlgorithm) sampleFileWithAlgorithm)
    {
        // Arrange
        var sampleFile = sampleFileWithAlgorithm.SampleFile;
        var imagePlaceholderAlgorithm = sampleFileWithAlgorithm.ImagePlaceholderAlgorithm;
        var client = await GetThumbnailerClientAsync();

        // Act
        var isSupported = await client.IsImagePlaceholderGenerationSupportedAsync(sampleFile.ContentType);
        var result = await client.GenerateImagePlaceholderAsync(sampleFile.RemoteUri, imagePlaceholderAlgorithm);

        // Assert
        Assert.That(isSupported, Is.True);
        Assert.That(result.Placeholder, Is.Not.Null.Or.Empty);
        await VerifyImagePlaceholderAsync(sampleFile, result.Placeholder, imagePlaceholderAlgorithm);
    }
}
