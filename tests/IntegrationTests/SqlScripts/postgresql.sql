CREATE SEQUENCE "public"."api_keys_id_seq";

CREATE TABLE "public"."api_keys" (
    "id"bigint NOT NULL DEFAULT nextval('api_keys_id_seq'::regclass),
    "name" CHARACTER VARYING(100) NOT NULL,
    "value" CHARACTER VARYING(32) NOT NULL,
    "created_at" TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "expires_at" TIMESTAMP NULL,
    "temporary" BOOLEAN NOT NULL DEFAULT 'false',
    CONSTRAINT "pk_api_keys" PRIMARY KEY ("id"),
    CONSTRAINT "uk_api_keys" UNIQUE ("value")
);

COMMENT ON COLUMN "public"."api_keys"."id" IS 'Automatically generated ID.';
COMMENT ON COLUMN "public"."api_keys"."name" IS 'Descriptive name of the API key.';
COMMENT ON COLUMN "public"."api_keys"."value" IS 'The API key.';
COMMENT ON COLUMN "public"."api_keys"."created_at" IS 'Creation timestamp.';
COMMENT ON COLUMN "public"."api_keys"."expires_at" IS 'Optional expiration timestamp.';
COMMENT ON COLUMN "public"."api_keys"."temporary" IS 'If true, API key will be automatically removed once it will be expired.';

INSERT INTO "public"."api_keys" ("name", "value") VALUES ('INTEGRATION TESTS', '__INTEGRATION_TESTS__');

CREATE SEQUENCE "public"."cache_entries_kvle_id_seq";

CREATE TABLE "public"."cache_entries" (
    "kvle_id" BIGINT NOT NULL DEFAULT nextval('cache_entries_kvle_id_seq'::regclass),
    "kvle_hash" BIGINT NOT NULL,
    "kvle_expiry" BIGINT NOT NULL,
    "kvle_interval" BIGINT NOT NULL,
    "kvle_value" BYTEA NOT NULL,
    "kvle_compressed" SMALLINT NOT NULL,
    "kvle_partition" VARCHAR(2000) NOT NULL,
    "kvle_key" VARCHAR(2000) NOT NULL,
    "kvle_creation" BIGINT NOT NULL,
    "kvle_parent_hash0" BIGINT NULL DEFAULT NULL,
    "kvle_parent_key0" VARCHAR(2000) NULL DEFAULT NULL,
    "kvle_parent_hash1" BIGINT NULL DEFAULT NULL,
    "kvle_parent_key1" VARCHAR(2000) NULL DEFAULT NULL,
    "kvle_parent_hash2" BIGINT NULL DEFAULT NULL,
    "kvle_parent_key2" VARCHAR(2000) NULL DEFAULT NULL,
    CONSTRAINT "pk_cache_entries" PRIMARY KEY ("kvle_id"),
    CONSTRAINT "uk_cache_entries" UNIQUE ("kvle_hash")
);

COMMENT ON COLUMN "public"."cache_entries"."kvle_id" IS 'Automatically generated ID.';
COMMENT ON COLUMN "public"."cache_entries"."kvle_hash" IS 'Hash of partition and key.';
COMMENT ON COLUMN "public"."cache_entries"."kvle_expiry" IS 'When the entry will expire, expressed as seconds after UNIX epoch.';
COMMENT ON COLUMN "public"."cache_entries"."kvle_interval" IS 'How many seconds should be used to extend expiry time when the entry is retrieved.';
COMMENT ON COLUMN "public"."cache_entries"."kvle_value" IS 'Serialized and optionally compressed content of this entry.';
COMMENT ON COLUMN "public"."cache_entries"."kvle_compressed" IS 'Whether the entry content was compressed or not.';
COMMENT ON COLUMN "public"."cache_entries"."kvle_partition" IS 'A partition holds a group of related keys.';
COMMENT ON COLUMN "public"."cache_entries"."kvle_key" IS 'A key uniquely identifies an entry inside a partition.';
COMMENT ON COLUMN "public"."cache_entries"."kvle_creation" IS 'When the entry was created, expressed as seconds after UNIX epoch.';
COMMENT ON COLUMN "public"."cache_entries"."kvle_parent_hash0" IS 'Optional parent entry hash, used to link entries in a hierarchical way.';
COMMENT ON COLUMN "public"."cache_entries"."kvle_parent_key0" IS 'Optional parent entry key, used to link entries in a hierarchical way.';
COMMENT ON COLUMN "public"."cache_entries"."kvle_parent_hash1" IS 'Optional parent entry hash, used to link entries in a hierarchical way.';
COMMENT ON COLUMN "public"."cache_entries"."kvle_parent_key1" IS 'Optional parent entry key, used to link entries in a hierarchical way.';
COMMENT ON COLUMN "public"."cache_entries"."kvle_parent_hash2" IS 'Optional parent entry hash, used to link entries in a hierarchical way.';
COMMENT ON COLUMN "public"."cache_entries"."kvle_parent_key2" IS 'Optional parent entry key, used to link entries in a hierarchical way.';
