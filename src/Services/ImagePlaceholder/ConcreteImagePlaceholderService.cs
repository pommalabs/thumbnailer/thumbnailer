﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using Blurhash.SkiaSharp;
using Microsoft.Extensions.Logging;
using PommaLabs.Thumbnailer.Models.Commands;
using PommaLabs.Thumbnailer.Models.DTO.Public;
using PommaLabs.Thumbnailer.Models.Enumerations;
using SkiaSharp;

namespace PommaLabs.Thumbnailer.Services.ImagePlaceholder;

/// <summary>
///   Image placeholder generation service that generates the image placeholder
/// </summary>
public sealed class ConcreteImagePlaceholderService : IImagePlaceholderService
{
    private readonly ILogger<ConcreteImagePlaceholderService> _logger;

    /// <summary>
    ///   Constructor.
    /// </summary>
    /// <param name="logger">Logger.</param>
    public ConcreteImagePlaceholderService(ILogger<ConcreteImagePlaceholderService> logger)
    {
        _logger = logger;
    }

    /// <inheritdoc/>
    public Task<ImagePlaceholderResult> GenerateImagePlaceholderAsync(ImagePlaceholderGenerationCommand command, CancellationToken cancellationToken)
    {
        SKBitmap? image = null;
        try
        {
            switch (command.ImagePlaceholderAlgorithm)
            {
                case ImagePlaceholderAlgorithm.BlurHash:
                    image = GetImage(command);
                    var (xComponentCount, yComponentCount) = GetBlurHashComponents(image);
                    var blurHash = Blurhasher.Encode(image, xComponentCount, yComponentCount);
                    return Task.FromResult(new ImagePlaceholderResult(blurHash));

                case ImagePlaceholderAlgorithm.ThumbHash:
                    image = GetImage(command);
                    var thumbHash = ThumbHashes.Utilities.RgbaToThumbHash(image.Width, image.Height, image.GetPixelSpan());
                    return Task.FromResult(new ImagePlaceholderResult(Convert.ToBase64String(thumbHash)));

                default:
                    var ex = new NotSupportedException("Given image placeholder algorithm is not supported");
                    ex.Data.Add(nameof(command.ImagePlaceholderAlgorithm), command.ImagePlaceholderAlgorithm);
                    throw ex;
            }
        }
        catch (Exception ex) when (ex is not NotSupportedException)
        {
            _logger.LogWarning(ex, "An error occurred while generating image placeholder with internal job ID '{PublicJobId}'", command.InternalJobId);
            return Task.FromResult(ImagePlaceholderResult.Empty);
        }
        finally
        {
            image?.Dispose();
        }
    }

    private static SKBitmap GetImage(ImagePlaceholderGenerationCommand command)
    {
        return SKBitmap.Decode(command.File.Path).Copy(SKColorType.Rgba8888);
    }

    private static (int XComponentCount, int YComponentCount) GetBlurHashComponents(SKBitmap image)
    {
        // BlurHash components define how many information is retained in the placeholder (max value
        // is 9). Using 3 or 4 is a nice compromise.
        if (image.Width > image.Height) return (4, 3);
        return (image.Width < image.Height) ? (3, 4) : (3, 3);
    }
}
