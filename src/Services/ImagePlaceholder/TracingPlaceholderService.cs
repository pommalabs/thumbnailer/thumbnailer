﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System.Text.Json;
using Microsoft.ApplicationInsights;
using Microsoft.ApplicationInsights.DataContracts;
using Microsoft.ApplicationInsights.Extensibility;
using Microsoft.Extensions.DependencyInjection;
using PommaLabs.Thumbnailer.Core;
using PommaLabs.Thumbnailer.Models.Commands;
using PommaLabs.Thumbnailer.Models.DTO.Public;

namespace PommaLabs.Thumbnailer.Services.ImagePlaceholder;

/// <summary>
///   Image placeholder generation service that wraps all operations into activities.
/// </summary>
public sealed class TracingPlaceholderService : IImagePlaceholderService
{
    private readonly IImagePlaceholderService _imagePlaceholderService;
    private readonly TelemetryClient? _telemetryClient;

    /// <summary>
    ///   Constructor.
    /// </summary>
    /// <param name="imagePlaceholderService">Image placeholder generation service.</param>
    /// <param name="serviceProvider">Service provider.</param>
    public TracingPlaceholderService(
        IImagePlaceholderService imagePlaceholderService,
        IServiceProvider serviceProvider)
    {
        _imagePlaceholderService = imagePlaceholderService;
        _telemetryClient = serviceProvider.GetService<TelemetryClient>();
    }

    /// <inheritdoc/>
    public async Task<ImagePlaceholderResult> GenerateImagePlaceholderAsync(ImagePlaceholderGenerationCommand command, CancellationToken cancellationToken)
    {
        using var activity = ThumbnailerActivitySource.Instance.StartActivity(Constants.ActivityNames.PlaceholderGeneration);

        if (activity != null)
        {
            activity.AddTag(Constants.TagNames.CommandInternalJobId, command.InternalJobId);
            activity.AddTag(Constants.TagNames.CommandFileContentType, command.File.ContentType);
        }

        IOperationHolder<DependencyTelemetry>? operation = null;
        try
        {
            if (_telemetryClient != null)
            {
                operation = _telemetryClient.StartOperation<DependencyTelemetry>(
                    command.File.ContentType, command.InternalJobId?.ToString());

                operation.Telemetry.Type = Constants.TelemetryTypes.PlaceholderGeneration;
                operation.Telemetry.Data = JsonSerializer.Serialize(command);
            }

            return await _imagePlaceholderService.GenerateImagePlaceholderAsync(command, cancellationToken);
        }
        catch (Exception ex)
        {
            if (operation != null)
            {
                operation.Telemetry.Success = false;
                _telemetryClient!.TrackException(ex);
            }
            throw;
        }
        finally
        {
            operation?.Dispose();
        }
    }
}
