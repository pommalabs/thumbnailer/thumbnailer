﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using Microsoft.Extensions.Options;
using PommaLabs.KVLite;
using PommaLabs.Thumbnailer.Models.Commands;
using PommaLabs.Thumbnailer.Models.Configurations;
using PommaLabs.Thumbnailer.Models.DTO.Public;

namespace PommaLabs.Thumbnailer.Services.ImagePlaceholder;

/// <summary>
///   Image placeholder generation service that checks the cache for existing values.
/// </summary>
public sealed class CachingImagePlaceholderService : IImagePlaceholderService
{
    private readonly IImagePlaceholderService _imagePlaceholderService;
    private readonly ICache _cache;
    private readonly IOptions<DatabaseConfiguration> _databaseConfiguration;

    /// <summary>
    ///   Constructor.
    /// </summary>
    /// <param name="imagePlaceholderService">Image placeholder generation service.</param>
    /// <param name="cache">Cache.</param>
    /// <param name="databaseConfiguration">Database configuration.</param>
    public CachingImagePlaceholderService(IImagePlaceholderService imagePlaceholderService,
        ICache cache,
        IOptions<DatabaseConfiguration> databaseConfiguration)
    {
        _imagePlaceholderService = imagePlaceholderService;
        _cache = cache;
        _databaseConfiguration = databaseConfiguration;
    }

    /// <inheritdoc/>
    public async Task<ImagePlaceholderResult> GenerateImagePlaceholderAsync(ImagePlaceholderGenerationCommand command, CancellationToken cancellationToken)
    {
        return await _cache.GetOrAddSlidingAsync(
            partition: nameof(IImagePlaceholderService),
            valueExpression: () => _imagePlaceholderService.GenerateImagePlaceholderAsync(command, cancellationToken),
            interval: _databaseConfiguration.Value.CacheLifetime,
            cancellationToken: cancellationToken);
    }
}
