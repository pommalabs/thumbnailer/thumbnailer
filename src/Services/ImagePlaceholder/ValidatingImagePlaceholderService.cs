﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using Microsoft.Extensions.Logging;
using PommaLabs.Thumbnailer.Core;
using PommaLabs.Thumbnailer.Models.Commands;
using PommaLabs.Thumbnailer.Models.DTO.Public;
using PommaLabs.Thumbnailer.Models.Enumerations;

namespace PommaLabs.Thumbnailer.Services.ImagePlaceholder;

/// <summary>
///   Image placeholder generation service that validates the given image and algorithm.
/// </summary>
public sealed class ValidatingImagePlaceholderService(
    IImagePlaceholderService imagePlaceholderService,
    ILogger<ValidatingImagePlaceholderService> logger)
    : ValidatingServiceBase(logger), IImagePlaceholderService
{
    private readonly IImagePlaceholderService _imagePlaceholderService = imagePlaceholderService;
    private readonly Validator _validator = new(logger);

    /// <inheritdoc/>
    public async Task<ImagePlaceholderResult> GenerateImagePlaceholderAsync(ImagePlaceholderGenerationCommand command, CancellationToken cancellationToken)
    {
        // Preconditions
        command.Validate(_validator);
        if (command.ImagePlaceholderAlgorithm == ImagePlaceholderAlgorithm.None) return ImagePlaceholderResult.Empty;

        var result = await _imagePlaceholderService.GenerateImagePlaceholderAsync(command, cancellationToken);

        // Postconditions
        ThrowWhen(result.Placeholder is null, "No image placeholder was generated");
        return result;
    }
}
