﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using NetVips;
using PommaLabs.Thumbnailer.Models.Commands;
using PommaLabs.Thumbnailer.Models.DTO.Public;
using PommaLabs.Thumbnailer.Repositories.TempFile;

namespace PommaLabs.Thumbnailer.Services.ImagePlaceholder;

/// <summary>
///   Image placeholder generation service that scales the given image to less than 100x100px if needed.
/// </summary>
public sealed class ScalingImagePlaceholderService : IImagePlaceholderService
{
    private readonly IImagePlaceholderService _imagePlaceholderService;
    private readonly ITempFileRepository _tempFileRepository;

    /// <summary>
    ///   Constructor.
    /// </summary>
    /// <param name="imagePlaceholderService">Image placeholder generation service.</param>
    /// <param name="tempFileRepository">Temporary file repository.</param>
    public ScalingImagePlaceholderService(IImagePlaceholderService imagePlaceholderService, ITempFileRepository tempFileRepository)
    {
        _imagePlaceholderService = imagePlaceholderService;
        _tempFileRepository = tempFileRepository;
    }

    /// <inheritdoc/>
    public async Task<ImagePlaceholderResult> GenerateImagePlaceholderAsync(ImagePlaceholderGenerationCommand command, CancellationToken cancellationToken)
    {
        const int MaxSizePx = 100;
        using var image = Image.NewFromFile(command.File.Path, access: Enums.Access.Sequential);
        if (image.Width > MaxSizePx || image.Height > MaxSizePx)
        {
            // The fine detail of an image is all thrown away, so images should be scaled down before running
            // the chosen algorithm on them. For BlurHash, scaling down the image is optional, but recommended,
            // while for ThumbHash it is mandatory and the image cannot be wider or higher than 100px.
            command = await ScaleImageAsync(command, image, MaxSizePx, cancellationToken);
        }
        return await _imagePlaceholderService.GenerateImagePlaceholderAsync(command, cancellationToken);
    }

    private async Task<ImagePlaceholderGenerationCommand> ScaleImageAsync(
        ImagePlaceholderGenerationCommand command, Image image, int sizePx,
        CancellationToken cancellationToken)
    {
        var resized = await _tempFileRepository.GetTempFileAsync(command.File.ContentType, cancellationToken);
        using var resizedLayer = image.ThumbnailImage(sizePx, sizePx);
        resizedLayer.WriteToFile(resized.Path);
        return new ImagePlaceholderGenerationCommand(resized, command.ImagePlaceholderAlgorithm);
    }
}
