﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using PommaLabs.Thumbnailer.Models.Commands;
using PommaLabs.Thumbnailer.Models.DTO.Public;

namespace PommaLabs.Thumbnailer.Services.ImagePlaceholder;

/// <summary>
///   Image placeholder generation service.
/// </summary>
public interface IImagePlaceholderService
{
    /// <summary>
    ///   Tries to generate an image placeholder.
    /// </summary>
    /// <param name="command">Image placeholder generation command.</param>
    /// <param name="cancellationToken">Cancellation token.</param>
    /// <returns>A result object containing the given image's placeholder.</returns>
    Task<ImagePlaceholderResult> GenerateImagePlaceholderAsync(ImagePlaceholderGenerationCommand command, CancellationToken cancellationToken);
}
