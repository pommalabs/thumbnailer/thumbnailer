﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

namespace PommaLabs.Thumbnailer.Services.Process;

/// <summary>
///   External process service.
/// </summary>
public interface IProcessService
{
    /// <summary>
    ///   Runs specified external process with given arguments.
    /// </summary>
    /// <param name="processName">Process name.</param>
    /// <param name="processArgs">Process arguments.</param>
    /// <param name="setTimeout">Whether to set a timeout on process execution time or not.</param>
    /// <param name="cancellationToken">Cancellation token.</param>
    Task RunProcessAsync(string processName, string processArgs, bool setTimeout, CancellationToken cancellationToken);
}
