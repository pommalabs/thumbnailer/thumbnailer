﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using PommaLabs.Salatino.Helpers;
using PommaLabs.Thumbnailer.Models.DTO.Internal;

namespace PommaLabs.Thumbnailer.Services.Html;

/// <summary>
///   Exposes services related to internal HTML server.
/// </summary>
public interface IHtmlService
{
    /// <summary>
    ///   Embeds all external images in the output HTML contents.
    /// </summary>
    /// <param name="htmlPath">HTML path.</param>
    /// <returns>
    ///   HTML contents where external images have been embedded as data URIs.
    /// </returns>
    Task<string> EmbedHtmlImagesAsync(string htmlPath);

    /// <summary>
    ///   Encrypts given HTML file name.
    /// </summary>
    /// <param name="htmlFileName">HTML file name.</param>
    /// <returns>Encrypted HTML file name.</returns>
    string EncryptHtmlFileName(string htmlFileName);

    /// <summary>
    ///   Tries to decrypt given HTML file name.
    /// </summary>
    /// <param name="encryptedHtmlFileName">Encrypted HTML file name.</param>
    /// <param name="htmlFileName">Decrypted HTML file name.</param>
    /// <returns>True if file name could be decrypted, false otherwise.</returns>
    bool TryDecryptHtmlFileName(string encryptedHtmlFileName, out string htmlFileName);

    /// <summary>
    ///   Returns an URI of given HTML file served by internal HTML server.
    /// </summary>
    /// <param name="htmlFile">HTML file.</param>
    /// <returns>URI of given HTML file served by internal HTML server.</returns>
    Uri GetHtmlFileUri(TempFileMetadata htmlFile)
    {
        var serverPort = ServerHelper.GetDefaultPort();
        var htmlFileName = Path.GetFileName(htmlFile.Path);
        var encryptedHtmlFileName = EncryptHtmlFileName(htmlFileName);
        return new Uri($"http://localhost:{serverPort}/api/v1/htmlServer/{encryptedHtmlFileName}");
    }
}
