﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System.Text;
using Microsoft.AspNetCore.DataProtection;
using Microsoft.AspNetCore.WebUtilities;
using Microsoft.Extensions.Options;
using PommaLabs.HtmlArk;
using PommaLabs.Thumbnailer.Models.Configurations;

namespace PommaLabs.Thumbnailer.Services.Html;

/// <summary>
///   Concrete HTML service.
/// </summary>
public sealed class ConcreteHtmlService(
    IDataProtectionProvider dataProtectionProvider,
    IOptions<SecurityConfiguration> securityConfiguration,
    IHtmlArchiver htmlArchiver)
    : IHtmlService
{
    private readonly IDataProtector _dataProtector = dataProtectionProvider.CreateProtector("HtmlServer");

    /// <summary>
    ///   Settings prepared for image embedding in an HTML file,
    ///   meaning that all other features have been disabled.
    /// </summary>
    /// <remarks>
    ///   Download errors are ignored, as a browser would do.
    /// </remarks>
    private readonly HtmlArchiverSettings _imageEmbeddingSettings = new()
    {
        HttpClientMaxResourceSizeInBytes = securityConfiguration.Value.MaxFileDownloadSizeInBytes,
        HttpClientTimeout = securityConfiguration.Value.FileDownloadTimeout,
        IgnoreAudios = true,
        IgnoreCss = true,
        IgnoreErrors = true,
        IgnoreFonts = true,
        IgnoreJs = true,
        IgnoreVideos = true,
        Minify = true,
    };

    /// <inheritdoc/>
    public Task<string> EmbedHtmlImagesAsync(string htmlPath)
    {
        return htmlArchiver.ArchiveAsync(htmlPath, _imageEmbeddingSettings);
    }

    /// <inheritdoc/>
    public string EncryptHtmlFileName(string htmlFileName)
    {
        var htmlFileNameBytes = Encoding.Default.GetBytes(htmlFileName);
        var encryptedHtmlFileName = _dataProtector.Protect(htmlFileNameBytes);
        return WebEncoders.Base64UrlEncode(encryptedHtmlFileName);
    }

    /// <inheritdoc/>
    public bool TryDecryptHtmlFileName(string encryptedHtmlFileName, out string htmlFileName)
    {
        try
        {
            var decodedHtmlFileName = WebEncoders.Base64UrlDecode(encryptedHtmlFileName);
            var decryptedHtmlFileName = _dataProtector.Unprotect(decodedHtmlFileName);
            htmlFileName = Encoding.Default.GetString(decryptedHtmlFileName);
            return true;
        }
        catch
        {
            // HTML file name could not be decrypted.
            htmlFileName = string.Empty;
            return false;
        }
    }
}
