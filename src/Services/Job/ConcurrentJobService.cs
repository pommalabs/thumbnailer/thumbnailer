﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System.Threading.Tasks.Dataflow;
using Microsoft.AspNetCore.WebUtilities;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using PommaLabs.KVLite;
using PommaLabs.Thumbnailer.Models.Commands;
using PommaLabs.Thumbnailer.Models.Configurations;
using PommaLabs.Thumbnailer.Models.DTO.Public;
using PommaLabs.Thumbnailer.Models.Enumerations;

namespace PommaLabs.Thumbnailer.Services.Job;

/// <summary>
///   Job service which stores concurrent jobs within an in-memory queue and within the distributed
///   cache. Each job is enqueued and executed on a single server, but other servers can respond to
///   job status queries.
/// </summary>
public sealed class ConcurrentJobService : IJobService
{
    private readonly ICache _cache;
    private readonly IOptions<DatabaseConfiguration> _databaseConfiguration;
    private readonly ILogger<ConcurrentJobService> _logger;
    private readonly BufferBlock<Guid> _queue;

    /// <summary>
    ///   Constructor.
    /// </summary>
    /// <param name="cache">Cache.</param>
    /// <param name="databaseConfiguration">Database configuration.</param>
    /// <param name="logger">Logger.</param>
    public ConcurrentJobService(
        ICache cache,
        IOptions<DatabaseConfiguration> databaseConfiguration,
        ILogger<ConcurrentJobService> logger)
    {
        _cache = cache;
        _databaseConfiguration = databaseConfiguration;
        _logger = logger;
        _queue = new BufferBlock<Guid>();
    }

    /// <inheritdoc/>
    public async Task<ThumbnailerCommandBase> DequeueCommandAsync(CancellationToken cancellationToken)
    {
        var internalJobId = await _queue.ReceiveAsync(cancellationToken);
        var jobData = await _cache.GetAsync<JobData>(
            partition: nameof(IJobService),
            key: new CacheKey(internalJobId),
            cancellationToken: cancellationToken);

        if (!jobData.HasValue)
        {
            _logger.LogWarning("A command with internal job ID '{InternalJobId}' should have been in the cache but it has not been found", internalJobId);
            throw new InvalidOperationException($"Command with internal job ID '{internalJobId}' was not found");
        }

        var (job, command) = jobData.Value;

        // Change job status, since it will be processed soon.
        job.Status = JobStatus.Processing;

        await _cache.AddSlidingAsync(
            partition: nameof(IJobService),
            key: new CacheKey(internalJobId),
            value: jobData.Value.WithUpdatedJobDetails(job),
            interval: _databaseConfiguration.Value.CacheLifetime,
            cancellationToken: cancellationToken);

        return command;
    }

    /// <inheritdoc/>
    public async Task<JobDetails> EnqueueCommandAsync<T>(ThumbnailerCommandBase command, CancellationToken cancellationToken)
    {
        var internalJobId = Guid.NewGuid();
        command = command with { InternalJobId = internalJobId };
        var job = new JobDetails(InternalJobIdToPublic(internalJobId));

        // First step: add job and command to the distributed cache, in order to persist this
        // information and to make it visible from all nodes.
        await _cache.AddSlidingAsync(
            partition: nameof(IJobService),
            key: new CacheKey(internalJobId),
            value: new JobData<T>(job, command, default),
            interval: _databaseConfiguration.Value.CacheLifetime,
            cancellationToken: cancellationToken);

        // Second step: enqueue job ID. Consumers will get job ID and will retrieve job and command
        // information from the distributed cache.
        _queue.Post(internalJobId);

        return job;
    }

    /// <inheritdoc/>
    public async Task<T?> GetJobResultAsync<T>(string publicJobId, CancellationToken cancellationToken)
    {
        try
        {
            var internalJobId = PublicJobIdToInternal(publicJobId);
            var jobData = (await _cache.GetAsync<JobData>(
                partition: nameof(IJobService),
                key: new CacheKey(internalJobId),
                cancellationToken: cancellationToken)).ValueOrDefault();

            if (jobData == null || jobData.Job.Status != JobStatus.Processed)
            {
                return default;
            }

            return (T?)jobData.GetResult();
        }
        catch (Exception ex)
        {
            _logger.LogWarning(ex, "An error occurred while getting the result of job with public ID '{PublicJobId}'", publicJobId);
            return default;
        }
    }

    /// <inheritdoc/>
    public async Task MarkJobAsFailedAsync(Guid internalJobId, string failureReason, CancellationToken cancellationToken)
    {
        var jobData = await _cache.GetAsync<JobData>(
            partition: nameof(IJobService),
            key: new CacheKey(internalJobId),
            cancellationToken: cancellationToken);

        if (!jobData.HasValue)
        {
            _logger.LogWarning("Cannot mark as failed job with internal ID '{InternalJobId}' because it was not found in the cache", internalJobId);
            return;
        }

        var (job, _) = jobData.Value;

        if (job.Status != JobStatus.Processing)
        {
            _logger.LogWarning("Cannot mark as failed job with internal ID '{InternalJobId}' because its status is '{JobStatus}'", internalJobId, job.Status);
            return;
        }

        job.Status = JobStatus.Failed;
        job.FailureReason = failureReason;

        await _cache.AddSlidingAsync(
            partition: nameof(IJobService),
            key: new CacheKey(internalJobId),
            value: jobData.Value.WithUpdatedJobDetails(job),
            interval: _databaseConfiguration.Value.CacheLifetime,
            cancellationToken: cancellationToken);
    }

    /// <inheritdoc/>
    public async Task MarkJobAsProcessedAsync<T>(Guid internalJobId, T result, CancellationToken cancellationToken)
        where T : notnull
    {
        var jobData = await _cache.GetAsync<JobData>(
            partition: nameof(IJobService),
            key: new CacheKey(internalJobId),
            cancellationToken: cancellationToken);

        if (!jobData.HasValue)
        {
            _logger.LogWarning("Cannot mark as processed job with internal ID '{InternalJobId}' because it was not found in the cache", internalJobId);
            return;
        }

        var (job, _) = jobData.Value;

        if (job.Status != JobStatus.Processing)
        {
            _logger.LogWarning("Cannot mark as processed job with internal ID '{InternalJobId}' because its status is '{JobStatus}'", internalJobId, job.Status);
            return;
        }

        job.Status = JobStatus.Processed;

        await _cache.AddSlidingAsync(
            partition: nameof(IJobService),
            key: new CacheKey(internalJobId),
            value: jobData.Value.WithUpdatedJobDetails(job).WithUpdatedResult(result),
            interval: _databaseConfiguration.Value.CacheLifetime,
            cancellationToken: cancellationToken);
    }

    /// <inheritdoc/>
    public async Task<JobDetails?> QueryJobAsync(string publicJobId, CancellationToken cancellationToken)
    {
        try
        {
            var internalJobId = PublicJobIdToInternal(publicJobId);
            var jobData = await _cache.GetAsync<JobData>(
                partition: nameof(IJobService),
                key: new CacheKey(internalJobId),
                cancellationToken: cancellationToken);

            return jobData.ValueOrDefault()?.Job;
        }
        catch (Exception ex)
        {
            _logger.LogWarning(ex, "An error occurred while querying command with public job ID '{PublicJobId}'", publicJobId);
            return null;
        }
    }

    private static string InternalJobIdToPublic(Guid internalJobId)
    {
        return WebEncoders.Base64UrlEncode(internalJobId.ToByteArray());
    }

    private static Guid PublicJobIdToInternal(string publicJobId)
    {
        return new Guid(WebEncoders.Base64UrlDecode(publicJobId));
    }

    private abstract record JobData(JobDetails Job, ThumbnailerCommandBase Command)
    {
        public abstract object? GetResult();

        public abstract JobData WithUpdatedJobDetails(JobDetails job);

        public abstract JobData WithUpdatedResult(object result);
    }

    private sealed record JobData<T>(JobDetails Job, ThumbnailerCommandBase Command, T? Result) : JobData(Job, Command)
    {
        public override object? GetResult() => Result;

        public override JobData WithUpdatedJobDetails(JobDetails job) => this with { Job = job };

        public override JobData WithUpdatedResult(object result) => this with { Result = (T)result };
    }
}
