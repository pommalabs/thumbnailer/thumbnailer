﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using Microsoft.Extensions.Logging;
using PommaLabs.Thumbnailer.Core;
using PommaLabs.Thumbnailer.Models.Commands;
using PommaLabs.Thumbnailer.Models.DTO.Internal;
using PommaLabs.Thumbnailer.Models.DTO.Public;

namespace PommaLabs.Thumbnailer.Services.Job;

/// <summary>
///   Job service which applies validations.
/// </summary>
public sealed class ValidatingJobService(
    IJobService jobService,
    ILogger<ValidatingJobService> logger)
    : ValidatingServiceBase(logger), IJobService
{
    private readonly IJobService _jobService = jobService;
    private readonly Validator _validator = new(logger);

    /// <inheritdoc/>
    public Task<ThumbnailerCommandBase> DequeueCommandAsync(CancellationToken cancellationToken)
    {
        return _jobService.DequeueCommandAsync(cancellationToken);
    }

    /// <inheritdoc/>
    public async Task<JobDetails> EnqueueCommandAsync<T>(ThumbnailerCommandBase command, CancellationToken cancellationToken)
    {
        // Preconditions
        command.Validate(_validator);

        return await _jobService.EnqueueCommandAsync<T>(command, cancellationToken);
    }

    /// <inheritdoc/>
    public async Task<T?> GetJobResultAsync<T>(string publicJobId, CancellationToken cancellationToken)
    {
        if (string.IsNullOrWhiteSpace(publicJobId)) return default;
        var result = await _jobService.GetJobResultAsync<T>(publicJobId, cancellationToken);

        // Postconditions
        switch (result)
        {
            case TempFileMetadata tempFile:
                ThrowWhen(!File.Exists(tempFile.Path), "Job result does not exist on file system");
                break;
        }

        return result;
    }

    /// <inheritdoc/>
    public Task MarkJobAsFailedAsync(Guid internalJobId, string failureReason, CancellationToken cancellationToken)
    {
        return _jobService.MarkJobAsFailedAsync(internalJobId, failureReason, cancellationToken);
    }

    /// <inheritdoc/>
    public Task MarkJobAsProcessedAsync<T>(Guid internalJobId, T result, CancellationToken cancellationToken)
        where T : notnull
    {
        return _jobService.MarkJobAsProcessedAsync(internalJobId, result, cancellationToken);
    }

    /// <inheritdoc/>
    public async Task<JobDetails?> QueryJobAsync(string publicJobId, CancellationToken cancellationToken)
    {
        if (string.IsNullOrWhiteSpace(publicJobId)) return null;
        return await _jobService.QueryJobAsync(publicJobId, cancellationToken);
    }
}
