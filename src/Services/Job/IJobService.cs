﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using PommaLabs.Thumbnailer.Models.Commands;
using PommaLabs.Thumbnailer.Models.DTO.Public;
using PommaLabs.Thumbnailer.Models.Enumerations;

namespace PommaLabs.Thumbnailer.Services.Job;

/// <summary>
///   Queued jobs service.
/// </summary>
public interface IJobService
{
    /// <summary>
    ///   Dequeues a command. If a queued command is not available, then method call is blocked
    ///   until one is available.
    /// </summary>
    /// <param name="cancellationToken">Cancellation token.</param>
    /// <returns>The dequeued command.</returns>
    Task<ThumbnailerCommandBase> DequeueCommandAsync(CancellationToken cancellationToken);

    /// <summary>
    ///   Adds the specified command to the queue, so that an asynchronous job will handle it.
    /// </summary>
    /// <param name="command">Command to be enqueued.</param>
    /// <param name="cancellationToken">Cancellation token.</param>
    /// <typeparam name="T">Job result type.</typeparam>
    /// <returns>A pointer to the newly created job.</returns>
    Task<JobDetails> EnqueueCommandAsync<T>(ThumbnailerCommandBase command, CancellationToken cancellationToken);

    /// <summary>
    ///   Gets the result linked to specified command. It is available for a short time only when
    ///   <see cref="JobDetails.Status"/> is equal to <see cref="JobStatus.Processed"/>.
    /// </summary>
    /// <param name="publicJobId">Public job ID.</param>
    /// <param name="cancellationToken">Cancellation token.</param>
    /// <typeparam name="T">Job result type.</typeparam>
    /// <returns>Job result, if available.</returns>
    Task<T?> GetJobResultAsync<T>(string publicJobId, CancellationToken cancellationToken);

    /// <summary>
    ///   Marks given job as failed because of specified reason.
    /// </summary>
    /// <param name="internalJobId">Internal job ID.</param>
    /// <param name="failureReason">Failure reason.</param>
    /// <param name="cancellationToken">Cancellation token.</param>
    Task MarkJobAsFailedAsync(Guid internalJobId, string failureReason, CancellationToken cancellationToken);

    /// <summary>
    ///   Marks given job as processed with specified result.
    /// </summary>
    /// <param name="internalJobId">Internal job ID.</param>
    /// <param name="result">Result.</param>
    /// <param name="cancellationToken">Cancellation token.</param>
    /// <typeparam name="T">Job result type.</typeparam>
    Task MarkJobAsProcessedAsync<T>(Guid internalJobId, T result, CancellationToken cancellationToken)
        where T : notnull;

    /// <summary>
    ///   Looks for a job with specified ID. If it is not found, null is returned.
    /// </summary>
    /// <param name="publicJobId">Public job ID.</param>
    /// <param name="cancellationToken">Cancellation token.</param>
    /// <returns>The job or null if it is not found.</returns>
    Task<JobDetails?> QueryJobAsync(string publicJobId, CancellationToken cancellationToken);
}
