﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using DotLiquid;
using Humanizer;
using PommaLabs.MimeTypes;
using PommaLabs.Thumbnailer.Models.DTO.Internal;
using PommaLabs.Thumbnailer.Repositories.TempFile;

namespace PommaLabs.Thumbnailer.Services.FallbackImage;

/// <summary>
///   Fallback image service which generates fallback SVG images starting from a template.
/// </summary>
public sealed class ConcreteFallbackImageService : IFallbackImageService
{
    private readonly ITempFileRepository _tempFileRepository;
    private readonly Template _templateSvg;

    /// <summary>
    ///   Constructor.
    /// </summary>
    /// <param name="tempFileRepository">Temporary file repository.</param>
    public ConcreteFallbackImageService(ITempFileRepository tempFileRepository)
    {
        _tempFileRepository = tempFileRepository;
        _templateSvg = InitializeTemplateSvg();
    }

    /// <inheritdoc/>
    public async Task<TempFileMetadata> GenerateFallbackSvgAsync(TempFileMetadata file, CancellationToken cancellationToken)
    {
        var templateVariables = Hash.FromAnonymousObject(new
        {
            File = new FileMetadata(
                contentType: file.ContentType,
                extension: file.Extension,
                size: file.Size),
        });

        var fallbackSvgContents = _templateSvg.Render(templateVariables);
        var fallbackSvg = await _tempFileRepository.GetTempFileAsync(MimeTypeMap.IMAGE.SVG_XML, cancellationToken);
        await File.WriteAllTextAsync(fallbackSvg.Path, fallbackSvgContents, cancellationToken);
        return fallbackSvg;
    }

    private static Template InitializeTemplateSvg()
    {
        using var templateSvgStream = File.OpenRead("Templates/fallback.svg");
        using var streamReader = new StreamReader(templateSvgStream);
        var templateSvg = Template.Parse(streamReader.ReadToEnd());
        templateSvg.MakeThreadSafe();
        return templateSvg;
    }

    private sealed class FileMetadata : Drop
    {
        public FileMetadata(
            string contentType,
            string extension,
            long size)
        {
            ContentType = contentType;
            Extension = extension;
            Size = size;
            HumanizedSize = size.Bytes().Humanize();
        }

        public string HumanizedSize { get; }

        public string ContentType { get; }

        public string Extension { get; }

        public long Size { get; }
    }
}
