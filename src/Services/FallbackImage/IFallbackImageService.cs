﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using PommaLabs.Thumbnailer.Models.Commands;
using PommaLabs.Thumbnailer.Models.DTO.Internal;

namespace PommaLabs.Thumbnailer.Services.FallbackImage;

/// <summary>
///   Handles the generation of fallback images, used when thumbnail generation fails.
/// </summary>
public interface IFallbackImageService
{
    /// <summary>
    ///   Creates a fallback thumbnail generation command, which can be fed into thumbnail
    ///   generation pipeline in order to generate a fallback image.
    /// </summary>
    /// <param name="command">Thumbnail generation command.</param>
    /// <param name="fallbackSvg">Fallback SVG image.</param>
    /// <returns>A fallback thumbnail generation command.</returns>
    ThumbnailGenerationCommand CreateFallbackCommand(ThumbnailGenerationCommand command, TempFileMetadata fallbackSvg)
    {
        return command with
        {
            File = fallbackSvg,
            // Smart crop is disabled because applying it to a fallback image might generate funny thumbnails.
            SmartCrop = false,
        };
    }

    /// <summary>
    ///   Generates a fallback SVG image, whose thumbnail can optionally be returned when thumbnail
    ///   generation fails. Fallback image is generated starting from an SVG template, whose
    ///   customization is documented in project README.
    /// </summary>
    /// <param name="file">File.</param>
    /// <param name="cancellationToken">Cancellation token.</param>
    /// <returns>A fallback image for given file.</returns>
    Task<TempFileMetadata> GenerateFallbackSvgAsync(TempFileMetadata file, CancellationToken cancellationToken);
}
