﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System.Globalization;
using PommaLabs.MimeTypes;
using PommaLabs.Thumbnailer.Models.Commands;
using PommaLabs.Thumbnailer.Models.DTO.Internal;
using PommaLabs.Thumbnailer.Models.Exceptions;
using PommaLabs.Thumbnailer.Repositories.TempFile;
using PommaLabs.Thumbnailer.Services.Process;

namespace PommaLabs.Thumbnailer.Services.Optimization;

/// <summary>
///   Optimization service which relies on an external executable to optimize media files.
/// </summary>
public sealed class ConcreteOptimizationService : IOptimizationService
{
    private static readonly Dictionary<string, (string name, string args)> s_optimizationCommands = new()
    {
        // Docs: https://www.mankier.com/1/gifsicle
        [MimeTypeMap.IMAGE.GIF] = ("gifsicle", "{0} --output={1} --optimize=2"),
        // Docs: https://www.mankier.com/1/jpegoptim
        [MimeTypeMap.IMAGE.JPEG] = ("evaluator", "jpegoptim {0} --max=80 --force --quiet --stdout > {1}"),
        // Docs: https://www.mankier.com/1/pngquant
        [MimeTypeMap.IMAGE.PNG] = ("pngquant", "{0} --output={1} --quality=65-80 --force --skip-if-larger"),
        // Docs: https://github.com/scour-project/scour
        [MimeTypeMap.IMAGE.SVG_XML] = ("scour", "{0} -o {1} --quiet --enable-comment-stripping --enable-viewboxing --no-line-breaks"),
        // Docs: https://www.mankier.com/1/cwebp
        [MimeTypeMap.IMAGE.WEBP] = ("cwebp", "{0} -o {1} -q 80 -quiet -mt"),
        // Docs: https://trac.ffmpeg.org/wiki/Encode/H.264
        // Info: https://askubuntu.com/a/353282
        [MimeTypeMap.VIDEO.MP4] = ("ffmpeg", "-y -v panic -i {0} -c:v libx264 -crf 28 -preset veryfast -movflags +faststart -pix_fmt yuv420p -c:a aac {1}"),
    };

    private readonly IProcessService _processService;
    private readonly ITempFileRepository _tempFileRepository;

    /// <summary>
    ///   Constructor.
    /// </summary>
    /// <param name="processService">Process service.</param>
    /// <param name="tempFileRepository">Temporary file repository.</param>
    public ConcreteOptimizationService(
        IProcessService processService,
        ITempFileRepository tempFileRepository)
    {
        _processService = processService;
        _tempFileRepository = tempFileRepository;
    }

    /// <inheritdoc/>
    public async Task<TempFileMetadata> OptimizeMediaAsync(MediaOptimizationCommand command, CancellationToken cancellationToken)
    {
        var (name, args) = s_optimizationCommands[command.File.ContentType];
        var optimized = await _tempFileRepository.GetTempFileAsync(command.File.ContentType, cancellationToken);

        try
        {
            await _processService.RunProcessAsync(
                name, string.Format(CultureInfo.InvariantCulture, args, command.File.Path, optimized.Path),
                setTimeout: true, cancellationToken: cancellationToken);
        }
        catch (ProcessException ex) when (name == "pngquant" && (ex.ExitCode == 98 || ex.ExitCode == 99))
        {
            // If conversion results in a file larger than the original, the image won't be saved
            // and pngquant will exit with status code 98. Additionally, file size gain must be
            // greater than the amount of quality lost. If quality drops by 50%, it will expect 50%
            // file size reduction to consider it worthwhile. If conversion results in quality below
            // the min quality the image won't be saved (or if outputting to stdin, 24-bit original
            // will be output) and pngquant will exit with status code 99.
            return command.File;
        }

        // Sometimes, the optimized file is bigger than the original. We need to return the smallest.
        return optimized.Size < command.File.Size ? optimized : command.File;
    }
}
