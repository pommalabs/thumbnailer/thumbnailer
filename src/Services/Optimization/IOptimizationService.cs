﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using PommaLabs.Thumbnailer.Models.Commands;
using PommaLabs.Thumbnailer.Models.DTO.Internal;

namespace PommaLabs.Thumbnailer.Services.Optimization;

/// <summary>
///   Media file optimization service.
/// </summary>
public interface IOptimizationService
{
    /// <summary>
    ///   Optimizes given media file.
    /// </summary>
    /// <param name="command">Media optimization command.</param>
    /// <param name="cancellationToken">Cancellation token.</param>
    /// <returns>An optimized version of given media file.</returns>
    Task<TempFileMetadata> OptimizeMediaAsync(MediaOptimizationCommand command, CancellationToken cancellationToken);
}
