﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using Microsoft.Extensions.Options;
using PommaLabs.KVLite;
using PommaLabs.Thumbnailer.Models.Commands;
using PommaLabs.Thumbnailer.Models.Configurations;
using PommaLabs.Thumbnailer.Models.DTO.Internal;

namespace PommaLabs.Thumbnailer.Services.Optimization;

/// <summary>
///   Optimization service which applies caching policies.
/// </summary>
public sealed class CachingOptimizationService : IOptimizationService
{
    private readonly ICache _cache;
    private readonly IOptions<DatabaseConfiguration> _databaseConfiguration;
    private readonly IOptimizationService _optimizationService;

    /// <summary>
    ///   Constructor.
    /// </summary>
    /// <param name="optimizationService">Optimization service.</param>
    /// <param name="cache">Cache.</param>
    /// <param name="databaseConfiguration">Database configuration.</param>
    public CachingOptimizationService(
        IOptimizationService optimizationService,
        ICache cache,
        IOptions<DatabaseConfiguration> databaseConfiguration)
    {
        _optimizationService = optimizationService;
        _cache = cache;
        _databaseConfiguration = databaseConfiguration;
    }

    /// <inheritdoc/>
    public async Task<TempFileMetadata> OptimizeMediaAsync(MediaOptimizationCommand command, CancellationToken cancellationToken)
    {
        var file = await _cache.GetOrAddSlidingAsync(
            partition: nameof(IOptimizationService),
            valueExpression: () => _optimizationService.OptimizeMediaAsync(command, cancellationToken),
            interval: _databaseConfiguration.Value.CacheLifetime,
            cancellationToken: cancellationToken);

        // A cached file path might not exists, for example, when the container crashes and
        // Thumbnailer temporary directory, if it has not been persisted, is lost.
        return File.Exists(file.Path)
            ? file
            : await _optimizationService.OptimizeMediaAsync(command, cancellationToken);
    }
}
