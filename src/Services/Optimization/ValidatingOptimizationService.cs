﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using Microsoft.Extensions.Logging;
using PommaLabs.Thumbnailer.Core;
using PommaLabs.Thumbnailer.Models.Commands;
using PommaLabs.Thumbnailer.Models.DTO.Internal;

namespace PommaLabs.Thumbnailer.Services.Optimization;

/// <summary>
///   Optimization service which applies validations.
/// </summary>
public sealed class ValidatingOptimizationService(
    IOptimizationService optimizationService,
    ILogger<ValidatingOptimizationService> logger)
    : ValidatingServiceBase(logger), IOptimizationService
{
    private readonly IOptimizationService _optimizationService = optimizationService;
    private readonly Validator _validator = new(logger);

    /// <inheritdoc/>
    public async Task<TempFileMetadata> OptimizeMediaAsync(MediaOptimizationCommand command, CancellationToken cancellationToken)
    {
        // Preconditions
        command.Validate(_validator);

        var result = await _optimizationService.OptimizeMediaAsync(command, cancellationToken);

        // Postconditions
        ThrowWhen(!File.Exists(result.Path), "Optimized media does not exist on file system");
        return result;
    }
}
