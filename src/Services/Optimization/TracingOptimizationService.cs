﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System.Text.Json;
using Microsoft.ApplicationInsights;
using Microsoft.ApplicationInsights.DataContracts;
using Microsoft.ApplicationInsights.Extensibility;
using Microsoft.Extensions.DependencyInjection;
using PommaLabs.Thumbnailer.Core;
using PommaLabs.Thumbnailer.Models.Commands;
using PommaLabs.Thumbnailer.Models.DTO.Internal;

namespace PommaLabs.Thumbnailer.Services.Optimization;

/// <summary>
///   Optimization service which wraps all operations into activities.
/// </summary>
public sealed class TracingOptimizationService : IOptimizationService
{
    private readonly IOptimizationService _optimizationService;
    private readonly TelemetryClient? _telemetryClient;

    /// <summary>
    ///   Constructor.
    /// </summary>
    /// <param name="optimizationService">Optimization service.</param>
    /// <param name="serviceProvider">Service provider.</param>
    public TracingOptimizationService(
        IOptimizationService optimizationService,
        IServiceProvider serviceProvider)
    {
        _optimizationService = optimizationService;
        _telemetryClient = serviceProvider.GetService<TelemetryClient>();
    }

    /// <inheritdoc/>
    public async Task<TempFileMetadata> OptimizeMediaAsync(MediaOptimizationCommand command, CancellationToken cancellationToken)
    {
        using var activity = ThumbnailerActivitySource.Instance.StartActivity(Constants.ActivityNames.MediaOptimization);

        if (activity != null)
        {
            activity.AddTag(Constants.TagNames.CommandInternalJobId, command.InternalJobId);
            activity.AddTag(Constants.TagNames.CommandFileContentType, command.File.ContentType);
        }

        IOperationHolder<DependencyTelemetry>? operation = null;
        try
        {
            if (_telemetryClient != null)
            {
                operation = _telemetryClient.StartOperation<DependencyTelemetry>(
                    command.File.ContentType, command.InternalJobId?.ToString());

                operation.Telemetry.Type = Constants.TelemetryTypes.MediaOptimization;
                operation.Telemetry.Data = JsonSerializer.Serialize(command);
            }

            return await _optimizationService.OptimizeMediaAsync(command, cancellationToken);
        }
        catch (Exception ex)
        {
            if (operation != null)
            {
                operation.Telemetry.Success = false;
                _telemetryClient!.TrackException(ex);
            }
            throw;
        }
        finally
        {
            operation?.Dispose();
        }
    }
}
