﻿// Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using Microsoft.Extensions.Logging;
using PommaLabs.Thumbnailer.Core;
using PommaLabs.Thumbnailer.Models.Commands;
using PommaLabs.Thumbnailer.Models.DTO.Internal;
using PommaLabs.Thumbnailer.Models.Enumerations;
using PommaLabs.Thumbnailer.Services.ImagePlaceholder;

namespace PommaLabs.Thumbnailer.Services.Optimization;

/// <summary>
///   Optimization service that invokes the thumbnail generation pipeline
/// </summary>
public sealed class PlaceholderOptimizationService : IOptimizationService
{
    private readonly IOptimizationService _optimizationService;
    private readonly IImagePlaceholderService _imagePlaceholderService;
    private readonly Validator _validator;

    /// <summary>
    ///   Constructor.
    /// </summary>
    /// <param name="optimizationService">Optimization service.</param>
    /// <param name="imagePlaceholderService">Image placeholder service.</param>
    /// <param name="logger">Logger.</param>
    public PlaceholderOptimizationService(
        IOptimizationService optimizationService,
        IImagePlaceholderService imagePlaceholderService,
        ILogger<PlaceholderOptimizationService> logger)
    {
        _optimizationService = optimizationService;
        _imagePlaceholderService = imagePlaceholderService;
        _validator = new Validator(logger);
    }

    /// <inheritdoc/>
    public async Task<TempFileMetadata> OptimizeMediaAsync(MediaOptimizationCommand command, CancellationToken cancellationToken)
    {
        // The image placeholder algorithm is set to its default value before sending the command to
        // the next layer, to make the optimization caching layer ignore that value. This way, if
        // the same file is sent for processing with a different algorithm, the optimization result
        // can be fetched from the cache, if available. The image placeholder pipeline has its own
        // cache, which means that this command result can be fetched from the cache in two steps:
        // first the optimization cache, then the image placeholder cache.
        var result = await _optimizationService.OptimizeMediaAsync(command with { ImagePlaceholderAlgorithm = ImagePlaceholderAlgorithm.None }, cancellationToken);

        try
        {
            // Validator will throw NotSupportedException if result content type is not supported.
            _validator.ValidateContentTypeForImagePlaceholderGeneration(result.ContentType);

            var pgCommand = new ImagePlaceholderGenerationCommand(result, command.ImagePlaceholderAlgorithm);
            var imagePlaceholderResult = await _imagePlaceholderService.GenerateImagePlaceholderAsync(pgCommand, cancellationToken);
            result.ImagePlaceholder = imagePlaceholderResult.Placeholder;
        }
        catch (NotSupportedException)
        {
            // File content type is not supported by image placeholder generation.
        }

        return result;
    }
}
