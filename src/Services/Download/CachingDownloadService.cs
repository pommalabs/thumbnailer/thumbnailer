﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using Microsoft.Extensions.Options;
using PommaLabs.KVLite;
using PommaLabs.Thumbnailer.Models.Configurations;
using PommaLabs.Thumbnailer.Models.DTO.Internal;

namespace PommaLabs.Thumbnailer.Services.Download;

/// <summary>
///   Download service which applies caching policies.
/// </summary>
public sealed class CachingDownloadService : IDownloadService
{
    private readonly ICache _cache;
    private readonly IOptions<DatabaseConfiguration> _databaseConfiguration;
    private readonly IDownloadService _downloadService;

    /// <summary>
    ///   Constructor.
    /// </summary>
    /// <param name="downloadService">Download service.</param>
    /// <param name="cache">Cache.</param>
    /// <param name="databaseConfiguration">Database configuration.</param>
    public CachingDownloadService(
        IDownloadService downloadService,
        ICache cache,
        IOptions<DatabaseConfiguration> databaseConfiguration)
    {
        _downloadService = downloadService;
        _cache = cache;
        _databaseConfiguration = databaseConfiguration;
    }

    /// <inheritdoc/>
    public async Task<TempFileMetadata> DownloadFileAsync(Uri fileUri, CancellationToken cancellationToken)
    {
        var file = await _cache.GetOrAddSlidingAsync(
            partition: nameof(IDownloadService),
            valueExpression: () => _downloadService.DownloadFileAsync(fileUri, cancellationToken),
            interval: _databaseConfiguration.Value.CacheLifetime,
            cancellationToken: cancellationToken);

        // A cached file path might not exists, for example, when the container crashes and
        // Thumbnailer temporary directory, if it has not been persisted, is lost.
        return File.Exists(file.Path)
            ? file
            : await _downloadService.DownloadFileAsync(fileUri, cancellationToken);
    }
}
