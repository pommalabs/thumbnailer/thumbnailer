﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using PommaLabs.Thumbnailer.Models.DTO.Internal;

namespace PommaLabs.Thumbnailer.Services.Download;

/// <summary>
///   File download service.
/// </summary>
public interface IDownloadService
{
    /// <summary>
    ///   Downloads file from specified URI.
    /// </summary>
    /// <param name="fileUri">File URI.</param>
    /// <param name="cancellationToken">Cancellation token.</param>
    /// <returns>An internal structure representing the downloaded file.</returns>
    Task<TempFileMetadata> DownloadFileAsync(Uri fileUri, CancellationToken cancellationToken);
}
