﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using PommaLabs.MimeTypes;
using PommaLabs.Thumbnailer.Core;
using PommaLabs.Thumbnailer.Models.Configurations;
using PommaLabs.Thumbnailer.Models.DTO.Internal;
using PommaLabs.Thumbnailer.Repositories.TempFile;

namespace PommaLabs.Thumbnailer.Services.Download;

/// <summary>
///   Download service which relies on the HTTP protocol.
/// </summary>
public sealed class HttpDownloadService : IDownloadService, IDisposable
{
    private readonly HttpClient _httpClient;
    private readonly ILogger<HttpDownloadService> _logger;
    private readonly ITempFileRepository _tempFileRepository;

    /// <summary>
    ///   Constructor.
    /// </summary>
    /// <param name="logger">Logger.</param>
    /// <param name="tempFileRepository">Temporary file repository.</param>
    /// <param name="securityConfiguration">Security configuration.</param>
    public HttpDownloadService(
        ILogger<HttpDownloadService> logger,
        ITempFileRepository tempFileRepository,
        IOptions<SecurityConfiguration> securityConfiguration)
    {
        _logger = logger;
        _tempFileRepository = tempFileRepository;

        _httpClient = new HttpClient
        {
            MaxResponseContentBufferSize = securityConfiguration.Value.MaxFileDownloadSizeInBytes,
            Timeout = securityConfiguration.Value.FileDownloadTimeout
        };
    }

    /// <inheritdoc/>
    public void Dispose()
    {
        _httpClient.Dispose();
    }

    /// <inheritdoc/>
    public async Task<TempFileMetadata> DownloadFileAsync(Uri fileUri, CancellationToken cancellationToken)
    {
        _logger.LogDebug("Downloading file '{FileUri}'", fileUri);

        // HttpCompletionOption.ResponseHeadersRead switch tells HttpClient not to buffer the
        // response. In other words, it will just read the headers and return the control back.
        using var response = await _httpClient.GetAsync(fileUri, HttpCompletionOption.ResponseHeadersRead, cancellationToken);

        if (!response.IsSuccessStatusCode)
        {
            _logger.LogError("An error occurred while downloading file '{FileUri}'. Remote response status code was {ResponseStatusCode}", fileUri, response.StatusCode);
            throw new InvalidOperationException($"File download failed, remote response status code was {response.StatusCode}");
        }

        var contentType = GetContentType(response);
        var fileName = GetFileName(fileUri, response);
        var extension = Path.GetExtension(fileName);
        _logger.LogInformation("Handling download of file with content type '{ContentType}', extension '{Extension}' and file name '{FileName}'", contentType, extension, fileName);

        var file = await _tempFileRepository.GetTempFileAsync(contentType, extension, cancellationToken);
        if (file.ContentType != contentType)
        {
            _logger.LogInformation("Content type of downloaded file has been overridden with '{ContentType}'", file.ContentType);
        }

        await using (var fileStream = File.OpenWrite(file.Path))
        {
            await using var responseStream = await response.Content.ReadAsStreamAsync(cancellationToken);
            await responseStream.CopyToAsync(fileStream, cancellationToken);
        }
        _logger.LogDebug("File '{FileUri}' downloaded successfully, it has been stored at path '{FilePath}'", fileUri, file.Path);

        return await _tempFileRepository.HandleFileDownloadAsync(file, cancellationToken);
    }

    private static string GetContentType(HttpResponseMessage response)
    {
        return response.Content.Headers.ContentType?.MediaType ?? MimeTypeMap.APPLICATION.OCTET_STREAM;
    }

    private static string? GetFileName(Uri fileUri, HttpResponseMessage response)
    {
        // Try to get the file name from the response headers first, then try to resort to
        // extracting the file name from the file URI.
        var fileName = response.Content.Headers.ContentDisposition?.FileName ?? Path.GetFileName(fileUri.LocalPath);
        return FileHelper.CleanupFileName(fileName);
    }
}
