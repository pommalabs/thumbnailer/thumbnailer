﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using Microsoft.Extensions.Logging;
using PommaLabs.Thumbnailer.Core;
using PommaLabs.Thumbnailer.Models.DTO.Internal;

namespace PommaLabs.Thumbnailer.Services.Download;

/// <summary>
///   Download service which applies validations.
/// </summary>
public sealed class ValidatingDownloadService(
    IDownloadService downloadService,
    ILogger<ValidatingDownloadService> logger)
    : ValidatingServiceBase(logger), IDownloadService
{
    private readonly IDownloadService _downloadService = downloadService;
    private readonly Validator _validator = new(logger);

    /// <inheritdoc/>
    public async Task<TempFileMetadata> DownloadFileAsync(Uri fileUri, CancellationToken cancellationToken)
    {
        // Preconditions
        _validator.ValidateFileUri(fileUri);

        var result = await _downloadService.DownloadFileAsync(fileUri, cancellationToken);

        // Postconditions
        ThrowWhen(!File.Exists(result.Path), "Downloaded file does not exist on file system");
        WarnWhen(new FileInfo(result.Path).Length == 0L, "Downloaded file is empty");
        return result;
    }
}
