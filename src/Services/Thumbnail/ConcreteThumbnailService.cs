﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using NetVips;
using Polly;
using Polly.Retry;
using PommaLabs.MimeTypes;
using PommaLabs.Thumbnailer.Models.Commands;
using PommaLabs.Thumbnailer.Models.DTO.Internal;
using PommaLabs.Thumbnailer.Repositories.TempFile;
using PommaLabs.Thumbnailer.Services.Html;
using PommaLabs.Thumbnailer.Services.Process;

namespace PommaLabs.Thumbnailer.Services.Thumbnail;

/// <summary>
///   Thumbnail service which relies on external processes to generate file thumbnails.
/// </summary>
public sealed class ConcreteThumbnailService : IThumbnailService
{
    private static readonly double[] s_opaqueBackground = [255];
    private static readonly double[] s_transparentBackground = [0];
    private static readonly string s_emailLayoutPath = Path.GetFullPath("Templates/email-layout.mrc");

    private static readonly ResiliencePipeline s_firefoxRetryStrategy = new ResiliencePipelineBuilder()
        .AddRetry(new RetryStrategyOptions
        {
            ShouldHandle = new PredicateBuilder().HandleResult(r => TempFileIsMissingOrEmpty(r as TempFileMetadata)),
            BackoffType = DelayBackoffType.Exponential,
            UseJitter = true,  // Adds a random factor to the delay.
            MaxRetryAttempts = 3,
            Delay = TimeSpan.FromSeconds(1),
        })
        .Build();

    private readonly SemaphoreSlim _libreOfficeLock = new(initialCount: 1, maxCount: 1);
    private readonly IProcessService _processService;
    private readonly ITempFileRepository _tempFileRepository;
    private readonly IHtmlService _htmlService;

    /// <summary>
    ///   Constructor.
    /// </summary>
    /// <param name="processService">Process service.</param>
    /// <param name="tempFileRepository">Temporary file repository.</param>
    /// <param name="htmlService">HTML service.</param>
    public ConcreteThumbnailService(
        IProcessService processService,
        ITempFileRepository tempFileRepository,
        IHtmlService htmlService)
    {
        _processService = processService;
        _tempFileRepository = tempFileRepository;
        _htmlService = htmlService;
    }

    /// <inheritdoc/>
    public async Task<TempFileMetadata> GenerateThumbnailAsync(ThumbnailGenerationCommand command,
        CancellationToken cancellationToken)
    {
        var file = await PreProcessFileAsync(command, cancellationToken);
        var thumbnail = await _tempFileRepository.GetTempFileAsync(MimeTypeMap.IMAGE.PNG, cancellationToken);

        Image topLayer;
        var layers = new Stack<Image>();

        void PushLayer(Image l) { layers.Push(l); topLayer = l; }
        void DisposeLayers() { while (layers.TryPop(out var l)) l.Dispose(); }

        try
        {
            PushLayer(Image.NewFromFile(file.Path, access: Enums.Access.Sequential, kwargs: GetFileLoadKwargs(file.ContentType)));

            if (command.ShavePx > 0)
            {
                var left = Math.Min(command.ShavePx, topLayer.Width);
                var top = Math.Min(command.ShavePx, topLayer.Height);

                var width = Math.Max(topLayer.Width - (2 * command.ShavePx), 0);
                var height = Math.Max(topLayer.Height - (2 * command.ShavePx), 0);

                PushLayer(topLayer.Crop(left, top, width, height));
            }

            Enums.Interesting? crop = command.SmartCrop ? Enums.Interesting.Attention : null;
            PushLayer(topLayer.ThumbnailImage(command.WidthPx, command.HeightPx, crop: crop));

            if (command.Fill)
            {
                if (topLayer.Bands <= 3)
                {
                    PushLayer(topLayer.BandjoinConst(s_opaqueBackground));
                }
                PushLayer(topLayer.Gravity(Enums.CompassDirection.Centre, command.WidthPx, command.HeightPx, Enums.Extend.Background, s_transparentBackground));
            }

            topLayer.WriteToFile(thumbnail.Path, GetFileSaveKwargs());
        }
        finally
        {
            DisposeLayers();
        }

        return thumbnail;
    }

    private async Task<TempFileMetadata> PreProcessFileAsync(ThumbnailGenerationCommand command, CancellationToken cancellationToken)
    {
        var file = command.File;
        switch (file.ContentType)
        {
            case MimeTypeMap.APPLICATION.DICOM:
                return await ConvertDicomToPngAsync(file, cancellationToken);
            case MimeTypeMap.IMAGE.VND_ADOBE_PHOTOSHOP:
                return await ConvertPsdToPngAsync(file, cancellationToken);
            case MimeTypeMap.MESSAGE.RFC822:
                return await ConvertEmlToPngAsync(file, command.WidthPx, cancellationToken);
        }
        if (file.IsEbook)
        {
            return await ConvertEbookToPdfAsync(file, cancellationToken);
        }
        if (file.IsHtmlDocument)
        {
            return await ConvertHtmlToPngAsync(file, command.WidthPx, cancellationToken);
        }
        if (file.IsOfficeDocument)
        {
            return await ConvertOfficeToPdfAsync(file, cancellationToken);
        }
        if (file.IsRawImage)
        {
            return await ConvertRawImageToTiffAsync(file, cancellationToken);
        }
        if (file.IsTextDocument)
        {
            return await ConvertTextToPngAsync(file, command.WidthPx, cancellationToken);
        }
        if (file.IsVideo)
        {
            return await ConvertVideoToPngAsync(file, cancellationToken);
        }
        return file;
    }

    private async Task<TempFileMetadata> ConvertDicomToPngAsync(TempFileMetadata file, CancellationToken cancellationToken)
    {
        var png = await _tempFileRepository.GetTempFileAsync(MimeTypeMap.IMAGE.PNG, cancellationToken);

        await _processService.RunProcessAsync(
            "dcmj2pnm", $"--quiet --frame 1 --write-png --nointerlace --meta-none {file.Path} {png.Path}",
            setTimeout: true, cancellationToken: cancellationToken);

        return png;
    }

    private async Task<TempFileMetadata> ConvertEbookToPdfAsync(TempFileMetadata file, CancellationToken cancellationToken)
    {
        var pdf = await _tempFileRepository.GetTempFileAsync(MimeTypeMap.APPLICATION.PDF, cancellationToken);

        await _processService.RunProcessAsync(
            "ebook-convert", $"{file.Path} {pdf.Path}",
            setTimeout: true, cancellationToken: cancellationToken);

        return pdf;
    }

    private async Task<TempFileMetadata> ConvertEmlToPngAsync(TempFileMetadata file, int widthPx, CancellationToken cancellationToken)
    {
        var html = await _tempFileRepository.GetTempFileAsync(MimeTypeMap.TEXT.HTML, cancellationToken);

        await _processService.RunProcessAsync(
            "evaluator", $"mhonarc -rcfile {s_emailLayoutPath} -single < {file.Path} > {html.Path}",
            setTimeout: true, cancellationToken: cancellationToken);

        // MHonArc does not embed images, but it extracts them next to the output HTML file.
        // Therefore, since HTML capture logic has been willingly developed
        // in order to read the HTML file only, images must be embedded.
        var htmlContents = await _htmlService.EmbedHtmlImagesAsync(html.Path);
        await File.WriteAllTextAsync(html.Path, htmlContents, cancellationToken);

        return await CaptureHtmlAsPortraitWebPageAsync(html, widthPx, cancellationToken);
    }

    private async Task<TempFileMetadata> ConvertHtmlToPngAsync(TempFileMetadata file, int widthPx, CancellationToken cancellationToken)
    {
        return await CaptureHtmlAsPortraitWebPageAsync(file, widthPx, cancellationToken);
    }

    private async Task<TempFileMetadata> ConvertOfficeToPdfAsync(TempFileMetadata file, CancellationToken cancellationToken)
    {
        await RunLibreOfficeProcess($"--convert-to pdf {file.Path}", cancellationToken);

        var extension = Path.GetExtension(file.Path);
        var filePathWithoutExtension = file.Path[..^extension.Length];
        var pdfPath = filePathWithoutExtension + MimeTypeMap.Extensions.PDF;

        return new TempFileMetadata(MimeTypeMap.APPLICATION.PDF) { Path = pdfPath };
    }

    private async Task<TempFileMetadata> ConvertPsdToPngAsync(TempFileMetadata file, CancellationToken cancellationToken)
    {
        var png = await _tempFileRepository.GetTempFileAsync(MimeTypeMap.IMAGE.PNG, cancellationToken);

        // As of Debian 12 and libvips 8.14, conversion using libvips crashes the .NET process,
        // because a low level error occurs within "unpack_pixels". To work around the problem,
        // we convert the PSD file to PNG using ImageMagick directly.
        await _processService.RunProcessAsync(
            "convert", $"{file.Path}[0] {png.Path}",
            setTimeout: true, cancellationToken: cancellationToken);

        return png;
    }

    private async Task<TempFileMetadata> ConvertRawImageToTiffAsync(TempFileMetadata file, CancellationToken cancellationToken)
    {
        var tiff = await _tempFileRepository.GetTempFileAsync(MimeTypeMap.IMAGE.TIFF, cancellationToken);

        await _processService.RunProcessAsync(
            "evaluator", $"dcraw -c -w -T {file.Path} > {tiff.Path}",
            setTimeout: true, cancellationToken: cancellationToken);

        return tiff;
    }

    private async Task<TempFileMetadata> ConvertTextToPngAsync(TempFileMetadata file, int widthPx, CancellationToken cancellationToken)
    {
        var html = await _tempFileRepository.GetTempFileAsync(MimeTypeMap.TEXT.HTML, cancellationToken);

        var scaleFactor = GetA4PageScaleFactor(widthPx);

        // - full=true produces an HTML file with embedded CSS.
        // - prestyles=font-size:16px makes fonts big enough to show 120 characters per line.
        // - prestyles=white-space:pre-wrap enables line wrapping when necessary.
        await _processService.RunProcessAsync(
            "pygmentize", $"-o {html.Path} -O \"full=true,prestyles=font-size:{24 * scaleFactor}px;white-space:pre-wrap,cssstyles=margin:{30 * scaleFactor}px;\" {file.Path}",
            setTimeout: true, cancellationToken: cancellationToken);

        return await CaptureHtmlAsPortraitA4PageAsync(html, widthPx, cancellationToken);
    }

    private async Task<TempFileMetadata> ConvertVideoToPngAsync(TempFileMetadata file, CancellationToken cancellationToken)
    {
        var png = await _tempFileRepository.GetTempFileAsync(MimeTypeMap.IMAGE.PNG, cancellationToken);

        await _processService.RunProcessAsync(
            "ffmpeg", $"-y -v panic -i {file.Path} -vf thumbnail -frames:v 1 {png.Path}",
            setTimeout: true, cancellationToken: cancellationToken);

        return png;
    }

    private static VOption GetFileLoadKwargs(string contentType)
    {
        return contentType switch
        {
            MimeTypeMap.APPLICATION.PDF => new VOption { ["page"] = 0, ["dpi"] = 96 },
            MimeTypeMap.IMAGE.GIF => new VOption { ["page"] = 0 },
            MimeTypeMap.IMAGE.SVG_XML => new VOption { ["dpi"] = 144 },
            MimeTypeMap.IMAGE.WEBP => new VOption { ["page"] = 0 },
            _ => new VOption()
        };
    }

    /// <summary>
    ///   See pngsave API: https://www.libvips.org/API/current/VipsForeignSave.html#vips-pngsave
    /// </summary>
    private static VOption GetFileSaveKwargs()
    {
        return new VOption()
        {
            ["background"] = s_transparentBackground,
            ["strip"] = true,
        };
    }

    private async Task<TempFileMetadata> CaptureHtmlAsPortraitWebPageAsync(TempFileMetadata file, int widthPx, CancellationToken cancellationToken)
    {
        var png = await _tempFileRepository.GetTempFileAsync(MimeTypeMap.IMAGE.PNG, cancellationToken);

        // Use only HD formats (1080, 2160, ...). widthPx parameter is scaled to HD format,
        // in order to obtain a well proportioned screenshot.
        var scaleFactor = (widthPx / 1080) + 1;

        await RunFirefoxProcess(
            $"--screenshot {png.Path} --window-size {1080 * scaleFactor} {_htmlService.GetHtmlFileUri(file)}",
            png, cancellationToken);

        return png;
    }

    private async Task<TempFileMetadata> CaptureHtmlAsPortraitA4PageAsync(TempFileMetadata file, int widthPx, CancellationToken cancellationToken)
    {
        var png = await _tempFileRepository.GetTempFileAsync(MimeTypeMap.IMAGE.PNG, cancellationToken);

        // Converts HTML into PNG, which can be easily processed by NetVips/ImageMagick. Size will be
        // proportional to 1240x1754 pixels, so that an A4 PNG at 150 DPI (or more) is produced.
        var scaleFactor = GetA4PageScaleFactor(widthPx);

        await RunFirefoxProcess(
            $"--screenshot {png.Path} --window-size {1240 * scaleFactor},{1754 * scaleFactor} {_htmlService.GetHtmlFileUri(file)}",
            png, cancellationToken);

        return png;
    }

    private static int GetA4PageScaleFactor(int widthPx)
    {
        return (widthPx / 1240) + 1;
    }

    private static bool TempFileIsMissingOrEmpty(TempFileMetadata? f)
    {
        return f is null || !File.Exists(f.Path) || new FileInfo(f.Path).Length == 0;
    }

    private async Task RunFirefoxProcess(string processArgs, TempFileMetadata output, CancellationToken cancellationToken)
    {
        // Parallel runs of Firefox process might yield empty outputs
        // without exiting with an error code.
        // That random issue is handled below with a retry strategy.
        await s_firefoxRetryStrategy.ExecuteAsync(async (ct) =>
        {
            await _processService.RunProcessAsync(
                "firefox", $"--headless {processArgs}",
                setTimeout: true, cancellationToken: ct);
            return output;
        }, cancellationToken);
    }

    private async Task RunLibreOfficeProcess(string processArgs, CancellationToken cancellationToken)
    {
        try
        {
            await _libreOfficeLock.WaitAsync(cancellationToken);
            await _processService.RunProcessAsync(
                "libreoffice", $"--headless --safe-mode {processArgs}",
                setTimeout: true, cancellationToken: cancellationToken);
        }
        finally
        {
            _libreOfficeLock.Release();
        }
    }
}
