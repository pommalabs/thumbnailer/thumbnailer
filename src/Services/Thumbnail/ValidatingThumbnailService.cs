﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using Microsoft.Extensions.Logging;
using PommaLabs.Thumbnailer.Core;
using PommaLabs.Thumbnailer.Models.Commands;
using PommaLabs.Thumbnailer.Models.DTO.Internal;
using PommaLabs.Thumbnailer.Services.FallbackImage;

namespace PommaLabs.Thumbnailer.Services.Thumbnail;

/// <summary>
///   Thumbnail service which applies validations.
/// </summary>
public sealed class ValidatingThumbnailService(
    IThumbnailService thumbnailService,
    ILogger<ValidatingThumbnailService> logger,
    IFallbackImageService fallbackImageService)
    : ValidatingServiceBase(logger), IThumbnailService
{
    private readonly IFallbackImageService _fallbackImageService = fallbackImageService;
    private readonly IThumbnailService _thumbnailService = thumbnailService;
    private readonly Validator _validator = new(logger);

    /// <inheritdoc/>
    public async Task<TempFileMetadata> GenerateThumbnailAsync(ThumbnailGenerationCommand command, CancellationToken cancellationToken)
    {
        TempFileMetadata result;
        try
        {
            // Preconditions
            command.Validate(_validator);

            result = await _thumbnailService.GenerateThumbnailAsync(command, cancellationToken);
        }
        catch (NotSupportedException) when (command.Fallback)
        {
            var fallbackSvg = await _fallbackImageService.GenerateFallbackSvgAsync(command.File, cancellationToken);
            var fallbackCommand = _fallbackImageService.CreateFallbackCommand(command, fallbackSvg);

            result = await _thumbnailService.GenerateThumbnailAsync(fallbackCommand, cancellationToken);
        }

        // Postconditions
        ThrowWhen(!File.Exists(result.Path), "Generated thumbnail does not exist on file system");
        return result;
    }
}
