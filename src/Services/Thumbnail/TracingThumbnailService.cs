﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System.Text.Json;
using Microsoft.ApplicationInsights;
using Microsoft.ApplicationInsights.DataContracts;
using Microsoft.ApplicationInsights.Extensibility;
using Microsoft.Extensions.DependencyInjection;
using PommaLabs.Thumbnailer.Core;
using PommaLabs.Thumbnailer.Models.Commands;
using PommaLabs.Thumbnailer.Models.DTO.Internal;

namespace PommaLabs.Thumbnailer.Services.Thumbnail;

/// <summary>
///   Thumbnail service which wraps all operations into activities.
/// </summary>
public sealed class TracingThumbnailService : IThumbnailService
{
    private readonly IThumbnailService _thumbnailService;
    private readonly TelemetryClient? _telemetryClient;

    /// <summary>
    ///   Constructor.
    /// </summary>
    /// <param name="thumbnailService">Thumbnail service.</param>
    /// <param name="serviceProvider">Service provider.</param>
    public TracingThumbnailService(
        IThumbnailService thumbnailService,
        IServiceProvider serviceProvider)
    {
        _thumbnailService = thumbnailService;
        _telemetryClient = serviceProvider.GetService<TelemetryClient>();
    }

    /// <inheritdoc/>
    public async Task<TempFileMetadata> GenerateThumbnailAsync(ThumbnailGenerationCommand command, CancellationToken cancellationToken)
    {
        using var activity = ThumbnailerActivitySource.Instance.StartActivity(Constants.ActivityNames.ThumbnailGeneration);

        if (activity != null)
        {
            activity.AddTag(Constants.TagNames.CommandInternalJobId, command.InternalJobId);
            activity.AddTag(Constants.TagNames.CommandFileContentType, command.File.ContentType);
        }

        IOperationHolder<DependencyTelemetry>? operation = null;
        try
        {
            if (_telemetryClient != null)
            {
                operation = _telemetryClient.StartOperation<DependencyTelemetry>(
                    command.File.ContentType, command.InternalJobId?.ToString());

                operation.Telemetry.Type = Constants.TelemetryTypes.ThumbnailGeneration;
                operation.Telemetry.Data = JsonSerializer.Serialize(command);
            }

            return await _thumbnailService.GenerateThumbnailAsync(command, cancellationToken);
        }
        catch (Exception ex)
        {
            if (operation != null)
            {
                operation.Telemetry.Success = false;
                _telemetryClient!.TrackException(ex);
            }
            throw;
        }
        finally
        {
            operation?.Dispose();
        }
    }
}
