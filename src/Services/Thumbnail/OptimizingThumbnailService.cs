﻿// Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using PommaLabs.Thumbnailer.Models.Commands;
using PommaLabs.Thumbnailer.Models.DTO.Internal;
using PommaLabs.Thumbnailer.Models.Enumerations;
using PommaLabs.Thumbnailer.Services.Optimization;

namespace PommaLabs.Thumbnailer.Services.Thumbnail;

/// <summary>
///   Thumbnail service which applies optimization.
/// </summary>
public sealed class OptimizingThumbnailService : IThumbnailService
{
    private readonly IThumbnailService _thumbnailService;
    private readonly IOptimizationService _optimizationService;

    /// <summary>
    ///   Constructor.
    /// </summary>
    /// <param name="thumbnailService">Thumbnail service.</param>
    /// <param name="optimizationService">optimization service.</param>
    public OptimizingThumbnailService(
        IThumbnailService thumbnailService,
        IOptimizationService optimizationService)
    {
        _thumbnailService = thumbnailService;
        _optimizationService = optimizationService;
    }

    /// <inheritdoc/>
    public async Task<TempFileMetadata> GenerateThumbnailAsync(ThumbnailGenerationCommand command, CancellationToken cancellationToken)
    {
        var result = await _thumbnailService.GenerateThumbnailAsync(command, cancellationToken);

        // Image placeholder generation will be performed in another layer, that is why algorithm is
        // set to its default value.
        var moCommand = new MediaOptimizationCommand(result, ImagePlaceholderAlgorithm.None);
        return await _optimizationService.OptimizeMediaAsync(moCommand, cancellationToken);
    }
}
