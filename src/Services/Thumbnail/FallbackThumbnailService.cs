﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using Microsoft.Extensions.Logging;
using NetVips;
using PommaLabs.Thumbnailer.Models.Commands;
using PommaLabs.Thumbnailer.Models.DTO.Internal;
using PommaLabs.Thumbnailer.Models.Exceptions;
using PommaLabs.Thumbnailer.Services.FallbackImage;

namespace PommaLabs.Thumbnailer.Services.Thumbnail;

/// <summary>
///   Thumbnail service which handles fallback thumbnail generation, when requested.
/// </summary>
public sealed class FallbackThumbnailService : IThumbnailService
{
    private readonly IFallbackImageService _fallbackImageService;
    private readonly ILogger _logger;
    private readonly IThumbnailService _thumbnailService;

    /// <summary>
    ///   Constructor.
    /// </summary>
    /// <param name="thumbnailService">Thumbnail service.</param>
    /// <param name="logger">Logger.</param>
    /// <param name="fallbackImageService">Fallback service.</param>
    public FallbackThumbnailService(
        IThumbnailService thumbnailService,
        ILogger<FallbackThumbnailService> logger,
        IFallbackImageService fallbackImageService)
    {
        _thumbnailService = thumbnailService;
        _logger = logger;
        _fallbackImageService = fallbackImageService;
    }

    /// <inheritdoc/>
    public async Task<TempFileMetadata> GenerateThumbnailAsync(ThumbnailGenerationCommand command, CancellationToken cancellationToken)
    {
        try
        {
            return await _thumbnailService.GenerateThumbnailAsync(command, cancellationToken);
        }
        catch (ProcessException ex) when (command.Fallback)
        {
            return await GenerateFallbackImage(command, ex, cancellationToken);
        }
        catch (VipsException ex) when (command.Fallback)
        {
            return await GenerateFallbackImage(command, ex, cancellationToken);
        }
    }

    private async Task<TempFileMetadata> GenerateFallbackImage(ThumbnailGenerationCommand command, Exception ex, CancellationToken cancellationToken)
    {
        _logger.LogWarning(ex, "An error occurred while generating thumbnail of file '{FilePath}'. As requested, fallback image will be generated", command.File.Path);

        var fallbackSvg = await _fallbackImageService.GenerateFallbackSvgAsync(command.File, cancellationToken);
        var fallbackCommand = _fallbackImageService.CreateFallbackCommand(command, fallbackSvg);

        return await _thumbnailService.GenerateThumbnailAsync(fallbackCommand, cancellationToken);
    }
}
