﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using PommaLabs.Thumbnailer.Models.Commands;
using PommaLabs.Thumbnailer.Models.DTO.Internal;

namespace PommaLabs.Thumbnailer.Services.Thumbnail;

/// <summary>
///   File thumbnail generation service.
/// </summary>
public interface IThumbnailService
{
    /// <summary>
    ///   Generates a thumbnail for given file, according to given parameters.
    /// </summary>
    /// <param name="command">Thumbnail generation command.</param>
    /// <param name="cancellationToken">Cancellation token.</param>
    /// <returns>A thumbnail for given file.</returns>
    Task<TempFileMetadata> GenerateThumbnailAsync(ThumbnailGenerationCommand command, CancellationToken cancellationToken);
}
