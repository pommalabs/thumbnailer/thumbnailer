﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using PommaLabs.Thumbnailer.Models.Commands;
using PommaLabs.Thumbnailer.Models.DTO.Internal;
using PommaLabs.Thumbnailer.Models.Enumerations;
using PommaLabs.Thumbnailer.Services.ImagePlaceholder;

namespace PommaLabs.Thumbnailer.Services.Thumbnail;

/// <summary>
///   Thumbnail service that invokes the thumbnail generation pipeline.
/// </summary>
public sealed class PlaceholderThumbnailService : IThumbnailService
{
    private readonly IThumbnailService _thumbnailService;
    private readonly IImagePlaceholderService _imagePlaceholderService;

    /// <summary>
    ///   Constructor.
    /// </summary>
    /// <param name="thumbnailService">Thumbnail service.</param>
    /// <param name="imagePlaceholderService">Image placeholder service.</param>
    public PlaceholderThumbnailService(
        IThumbnailService thumbnailService,
        IImagePlaceholderService imagePlaceholderService)
    {
        _thumbnailService = thumbnailService;
        _imagePlaceholderService = imagePlaceholderService;
    }

    /// <inheritdoc/>
    public async Task<TempFileMetadata> GenerateThumbnailAsync(ThumbnailGenerationCommand command, CancellationToken cancellationToken)
    {
        // The image placeholder algorithm is set to its default value before sending the command to
        // the next layer, to make the thumbnail caching layer ignore that value. This way, if the
        // same file is sent for processing with a different algorithm, the thumbnail result can be
        // fetched from the cache, if available. The image placeholder pipeline has its own cache,
        // which means that this command result can be fetched from the cache in two steps: first
        // the thumbnail cache, then the image placeholder cache.
        var result = await _thumbnailService.GenerateThumbnailAsync(command with { ImagePlaceholderAlgorithm = ImagePlaceholderAlgorithm.None }, cancellationToken);

        // Result thumbnail is always a PNG file, which is supported by placeholder generation. So,
        // differently from optimization flow, here we do not need to validate result content type.
        var pgCommand = new ImagePlaceholderGenerationCommand(result, command.ImagePlaceholderAlgorithm);
        var imagePlaceholderResult = await _imagePlaceholderService.GenerateImagePlaceholderAsync(pgCommand, cancellationToken);
        result.ImagePlaceholder = imagePlaceholderResult.Placeholder;

        return result;
    }
}
