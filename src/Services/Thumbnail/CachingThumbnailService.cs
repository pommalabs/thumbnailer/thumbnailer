﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using Microsoft.Extensions.Options;
using PommaLabs.KVLite;
using PommaLabs.Thumbnailer.Models.Commands;
using PommaLabs.Thumbnailer.Models.Configurations;
using PommaLabs.Thumbnailer.Models.DTO.Internal;

namespace PommaLabs.Thumbnailer.Services.Thumbnail;

/// <summary>
///   Thumbnail service which applies caching policies.
/// </summary>
public sealed class CachingThumbnailService : IThumbnailService
{
    private readonly ICache _cache;
    private readonly IOptions<DatabaseConfiguration> _databaseConfiguration;
    private readonly IThumbnailService _thumbnailService;

    /// <summary>
    ///   Constructor.
    /// </summary>
    /// <param name="thumbnailService">Thumbnail service.</param>
    /// <param name="cache">Cache.</param>
    /// <param name="databaseConfiguration">Database configuration.</param>
    public CachingThumbnailService(
        IThumbnailService thumbnailService,
        ICache cache,
        IOptions<DatabaseConfiguration> databaseConfiguration)
    {
        _thumbnailService = thumbnailService;
        _cache = cache;
        _databaseConfiguration = databaseConfiguration;
    }

    /// <inheritdoc/>
    public async Task<TempFileMetadata> GenerateThumbnailAsync(ThumbnailGenerationCommand command, CancellationToken cancellationToken)
    {
        var file = await _cache.GetOrAddSlidingAsync(
            partition: nameof(IThumbnailService),
            valueExpression: () => _thumbnailService.GenerateThumbnailAsync(command, cancellationToken),
            interval: _databaseConfiguration.Value.CacheLifetime,
            cancellationToken: cancellationToken);

        // A cached file path might not exists, for example, when the container crashes and
        // Thumbnailer temporary directory, if it has not been persisted, is lost.
        return File.Exists(file.Path)
            ? file
            : await _thumbnailService.GenerateThumbnailAsync(command, cancellationToken);
    }
}
