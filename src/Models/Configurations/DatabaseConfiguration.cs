﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using PommaLabs.Thumbnailer.Models.Enumerations;

namespace PommaLabs.Thumbnailer.Models.Configurations;

/// <summary>
///   Database configuration.
/// </summary>
public sealed class DatabaseConfiguration
{
    /// <summary>
    ///   Backing field of <see cref="SchemaName"/>.
    /// </summary>
    private string? _schemaName;

    /// <summary>
    ///   How long each cache entry will live. Defaults to 20 minutes and should be greater than
    ///   <see cref="SecurityConfiguration.AsyncProcessTimeout"/>, since cache is used to track
    ///   asynchronous jobs completion.
    /// </summary>
    public TimeSpan CacheLifetime { get; set; } = TimeSpan.FromMinutes(20);

    /// <summary>
    ///   Connection string of a SQL database.
    /// </summary>
    public string? ConnectionString { get; set; }

    /// <summary>
    ///   Provider.
    /// </summary>
    public DatabaseProvider Provider { get; set; }

    /// <summary>
    ///   The schema which holds Thumbnailer tables.
    /// </summary>
    public string SchemaName
    {
        get => string.IsNullOrWhiteSpace(_schemaName) ? (Provider switch { DatabaseProvider.PostgreSql => "public", DatabaseProvider.SqlServer => "dbo", _ => string.Empty }) : _schemaName;
        set => _schemaName = value;
    }

    /// <summary>
    ///   The schema which holds Thumbnailer tables, formatted with a trailing dot character.
    /// </summary>
    public string SchemaNameWithDot
    {
        get
        {
            var schemaName = SchemaName;
            return string.IsNullOrWhiteSpace(schemaName) ? string.Empty : $"{schemaName}.";
        }
    }
}
