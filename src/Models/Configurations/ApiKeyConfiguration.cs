﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

namespace PommaLabs.Thumbnailer.Models.Configurations;

/// <summary>
///   API key configuration.
/// </summary>
public sealed class ApiKeyConfiguration
{
    /// <summary>
    ///   Expiration timestamp. If not specified, API key will not expire.
    /// </summary>
    public DateTimeOffset? ExpiresAt { get; set; }

    /// <summary>
    ///   Descriptive name.
    /// </summary>
    public string Name { get; set; } = string.Empty;

    /// <summary>
    ///   API key value.
    /// </summary>
    public string Value { get; set; } = string.Empty;
}
