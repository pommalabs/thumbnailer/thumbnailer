﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

namespace PommaLabs.Thumbnailer.Models.Configurations;

/// <summary>
///   Website configuration.
/// </summary>
public sealed class WebsiteConfiguration
{
    /// <summary>
    ///   Whether to expose or not the public website. Defaults to false. When this property is set
    ///   to false, root path is redirected to Swagger docs.
    /// </summary>
    public bool Enabled { get; set; }

    /// <summary>
    ///   How long a fair use token is valid after it is generated. Defaults to 60 minutes.
    /// </summary>
    public TimeSpan FairUseTokenLifetime { get; set; } = TimeSpan.FromMinutes(60);
}
