﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

namespace PommaLabs.Thumbnailer.Models.Enumerations;

/// <summary>
///   Describes the status of an asynchronous job.
/// </summary>
public enum JobStatus
{
    /// <summary>
    ///   Job has been added to the queue and it has not been processed yet.
    /// </summary>
    Queued = 0,

    /// <summary>
    ///   Job has been removed from the queue and it is being processed.
    /// </summary>
    Processing = 1,

    /// <summary>
    ///   Job has been successfully processed and its result can be downloaded.
    /// </summary>
    Processed = 2,

    /// <summary>
    ///   Job has not been successfully processed and it will not be retried.
    /// </summary>
    Failed = 3,
}
