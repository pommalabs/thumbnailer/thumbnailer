﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System.Text.Json.Serialization;

namespace PommaLabs.Thumbnailer.Models.Enumerations;

/// <summary>
///   How web service will return files.
/// </summary>
[JsonConverter(typeof(JsonStringEnumConverter))]
public enum ResponseType
{
    /// <summary>
    ///   The file is returned as binary content.
    /// </summary>
    Binary = 0,

    /// <summary>
    ///   The file is returned as JSON with a Base64 encoded string.
    /// </summary>
    Base64 = 1
}
