﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

namespace PommaLabs.Thumbnailer.Models.DTO.Internal;

/// <summary>
///   Represents an API key.
/// </summary>
public sealed record ApiKeyCredentials
{
    /// <summary>
    ///   An invalid API key.
    /// </summary>
    public static ApiKeyCredentials Invalid { get; } = new()
    {
        IsValid = false,
        Name = string.Empty,
        Value = string.Empty,
        ExpiresAt = null,
    };

    /// <summary>
    ///   True if API key is valid, false otherwise.
    /// </summary>
    public required bool IsValid { get; init; }

    /// <summary>
    ///   Descriptive name.
    /// </summary>
    public required string Name { get; init; }

    /// <summary>
    ///   API key value.
    /// </summary>
    public required string Value { get; init; }

    /// <summary>
    ///   Key expiration timestamp.
    /// </summary>
    public required DateTimeOffset? ExpiresAt { get; init; }
}
