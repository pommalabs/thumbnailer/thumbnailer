﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

namespace PommaLabs.Thumbnailer.Models.DTO.Public;

/// <summary>
///   Represents a file which will be exchanged via API.
/// </summary>
public sealed record FileDetails
{
    /// <summary>
    ///   File contents, which the client will send/receive as a Base64 encoded string.
    /// </summary>
    public byte[] Contents { get; init; } = [];

    /// <summary>
    ///   File content type (e.g. "image/png").
    /// </summary>
    public string ContentType { get; init; } = string.Empty;

    /// <summary>
    ///   File name (e.g. "image.png").
    /// </summary>
    public string FileName { get; init; } = string.Empty;

    /// <summary>
    ///   A very compact representation of a placeholder for an image, if requested and available.
    /// </summary>
    public string? ImagePlaceholder { get; init; }
}
