﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

namespace PommaLabs.Thumbnailer.Models.DTO.Public;

/// <summary>
///   Represents a generated image placeholder.
/// </summary>
public sealed class ImagePlaceholderResult
{
    /// <summary>
    ///   Constructor.
    /// </summary>
    /// <param name="placeholder">Image placeholder.</param>
    public ImagePlaceholderResult(string placeholder)
    {
        Placeholder = placeholder;
    }

    /// <summary>
    ///   Constructor.
    /// </summary>
    private ImagePlaceholderResult()
    {
        Placeholder = null;
    }

    /// <summary>
    ///   Empty image placeholder result, returned when image placeholder cannot be generated.
    /// </summary>
    public static ImagePlaceholderResult Empty { get; } = new ImagePlaceholderResult();

    /// <summary>
    ///   Image placeholder.
    /// </summary>
    public string? Placeholder { get; }
}
