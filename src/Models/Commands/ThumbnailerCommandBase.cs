﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using PommaLabs.KVLite;
using PommaLabs.Thumbnailer.Core;
using PommaLabs.Thumbnailer.Models.DTO.Internal;
using PommaLabs.Thumbnailer.Models.Enumerations;

namespace PommaLabs.Thumbnailer.Models.Commands;

/// <summary>
///   Represents shared information between Thumbnailer commands.
/// </summary>
public abstract record class ThumbnailerCommandBase : IObjectWithCacheKey
{
    /// <summary>
    ///   Constructor.
    /// </summary>
    /// <param name="file">File.</param>
    /// <param name="imagePlaceholderAlgorithm">Desired placeholder generation algorithm.</param>
    protected ThumbnailerCommandBase(TempFileMetadata file, ImagePlaceholderAlgorithm imagePlaceholderAlgorithm)
    {
        File = file;
        ImagePlaceholderAlgorithm = imagePlaceholderAlgorithm;
    }

    /// <summary>
    ///   File on which the command should be applied.
    /// </summary>
    public TempFileMetadata File { get; init; }

    /// <summary>
    ///   Internal job ID, available when command is part of an asynchronous job.
    /// </summary>
    public Guid? InternalJobId { get; init; }

    /// <summary>
    ///   Chosen algorithm to generate the image placeholder.
    /// </summary>
    public ImagePlaceholderAlgorithm ImagePlaceholderAlgorithm { get; init; }

    /// <summary>
    ///   ID of the processor which is currently handling this job.
    /// </summary>
    public int ProcessorId { get; init; }

    /// <inheritdoc/>
    public virtual CacheKey GetCacheKey()
    {
        return new CacheKey(File, ImagePlaceholderAlgorithm);
    }

    /// <summary>
    ///   Validates command properties and throws an exception if they are not valid.
    /// </summary>
    /// <param name="validator">Validator.</param>
    public abstract void Validate(Validator validator);
}
