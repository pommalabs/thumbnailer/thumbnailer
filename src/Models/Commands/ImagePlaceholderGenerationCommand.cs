﻿// Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using PommaLabs.Thumbnailer.Core;
using PommaLabs.Thumbnailer.Models.DTO.Internal;
using PommaLabs.Thumbnailer.Models.Enumerations;

namespace PommaLabs.Thumbnailer.Models.Commands;

/// <summary>
///   Represents all information required to perform a media optimization command.
/// </summary>
public record ImagePlaceholderGenerationCommand : ThumbnailerCommandBase
{
    /// <summary>
    ///   Constructor.
    /// </summary>
    /// <param name="file">File.</param>
    /// <param name="imagePlaceholderAlgorithm">Desired placeholder generation algorithm.</param>
    public ImagePlaceholderGenerationCommand(TempFileMetadata file, ImagePlaceholderAlgorithm imagePlaceholderAlgorithm)
        : base(file, imagePlaceholderAlgorithm)
    {
    }

    /// <inheritdoc/>
    public override void Validate(Validator validator)
    {
        validator.ValidateContentTypeForImagePlaceholderGeneration(File.ContentType);
    }
}
