﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using PommaLabs.Thumbnailer.Core;
using PommaLabs.Thumbnailer.Models.DTO.Internal;
using PommaLabs.Thumbnailer.Models.DTO.Public;

namespace PommaLabs.Thumbnailer.Repositories.TempFile;

public sealed class ValidatingTempFileRepository(
    ITempFileRepository tempFileRepository,
    ILogger<ValidatingTempFileRepository> logger)
    : ValidatingServiceBase(logger), ITempFileRepository
{
    private const string TempFileNotExisting = "Temporary file does not exist on file system";

    public async Task DeleteTempFileAsync(TempFileMetadata file, CancellationToken cancellationToken)
    {
        await tempFileRepository.DeleteTempFileAsync(file, cancellationToken);

        // Postconditions
        ThrowWhen(File.Exists(file.Path), "A deleted file still exists on file system");
    }

    public Task<long> GetAvailableFreeSpaceInBytes(CancellationToken cancellationToken)
    {
        return tempFileRepository.GetAvailableFreeSpaceInBytes(cancellationToken);
    }

    public async Task<TempFileMetadata> GetTempFileAsync(
        string contentType, string? extension,
        CancellationToken cancellationToken)
    {
        var result = await tempFileRepository.GetTempFileAsync(contentType, extension, cancellationToken);

        // Postconditions
        ThrowWhen(!File.Exists(result.Path), TempFileNotExisting);
        return result;
    }

    public Task<IEnumerable<TempFileMetadata>> GetTempFilesAsync(CancellationToken cancellationToken)
    {
        return tempFileRepository.GetTempFilesAsync(cancellationToken);
    }

    public async Task<TempFileMetadata> HandleFileDownloadAsync(
        TempFileMetadata downloadedFile, CancellationToken cancellationToken)
    {
        var result = await tempFileRepository.HandleFileDownloadAsync(downloadedFile, cancellationToken);

        // Postconditions
        ThrowWhen(!File.Exists(result.Path), TempFileNotExisting);
        WarnWhen(new FileInfo(result.Path).Length == 0L, "Downloaded file is empty");
        return result;
    }

    public async Task<TempFileMetadata> HandleFileUploadAsync(
        IFormFile formFile, CancellationToken cancellationToken)
    {
        var result = await tempFileRepository.HandleFileUploadAsync(formFile, cancellationToken);

        // Postconditions
        ThrowWhen(!File.Exists(result.Path), TempFileNotExisting);
        WarnWhen(new FileInfo(result.Path).Length == 0L, "Uploaded file is empty");
        return result;
    }

    public async Task<TempFileMetadata> HandleFileUploadAsync(
        FileDetails uploadedFile, CancellationToken cancellationToken)
    {
        var result = await tempFileRepository.HandleFileUploadAsync(uploadedFile, cancellationToken);

        // Postconditions
        ThrowWhen(!File.Exists(result.Path), TempFileNotExisting);
        WarnWhen(new FileInfo(result.Path).Length == 0L, "Uploaded file is empty");
        return result;
    }

    public Task<TempFileMetadata> UpdateTempFileAsync(
        TempFileMetadata file, string newContentType, string newExtension,
        CancellationToken cancellationToken)
    {
        return tempFileRepository.UpdateTempFileAsync(file, newContentType, newExtension, cancellationToken);
    }
}
