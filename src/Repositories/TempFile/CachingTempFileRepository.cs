﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System.Globalization;
using System.Security.Cryptography;
using System.Text;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using PommaLabs.KVLite;
using PommaLabs.Thumbnailer.Models.Configurations;
using PommaLabs.Thumbnailer.Models.DTO.Internal;
using PommaLabs.Thumbnailer.Models.DTO.Public;

namespace PommaLabs.Thumbnailer.Repositories.TempFile;

/// <summary>
///   Temporary file repository which applies caching policies.
/// </summary>
public sealed class CachingTempFileRepository : ITempFileRepository
{
    private readonly ICache _cache;
    private readonly IOptions<DatabaseConfiguration> _databaseConfiguration;
    private readonly ITempFileRepository _tempFileRepository;

    /// <summary>
    ///   Constructor.
    /// </summary>
    /// <param name="tempFileRepository">Temporary file repository.</param>
    /// <param name="cache">Cache.</param>
    /// <param name="databaseConfiguration">Database configuration.</param>
    public CachingTempFileRepository(
        ITempFileRepository tempFileRepository,
        ICache cache,
        IOptions<DatabaseConfiguration> databaseConfiguration)
    {
        _tempFileRepository = tempFileRepository;
        _cache = cache;
        _databaseConfiguration = databaseConfiguration;
    }

    /// <inheritdoc/>
    public Task DeleteTempFileAsync(TempFileMetadata file, CancellationToken cancellationToken)
    {
        return _tempFileRepository.DeleteTempFileAsync(file, cancellationToken);
    }

    /// <inheritdoc/>
    public Task<long> GetAvailableFreeSpaceInBytes(CancellationToken cancellationToken)
    {
        return _tempFileRepository.GetAvailableFreeSpaceInBytes(cancellationToken);
    }

    /// <inheritdoc/>
    public Task<TempFileMetadata> GetTempFileAsync(
        string contentType, string? extension,
        CancellationToken cancellationToken)
    {
        return _tempFileRepository.GetTempFileAsync(contentType, extension, cancellationToken);
    }

    /// <inheritdoc/>
    public Task<IEnumerable<TempFileMetadata>> GetTempFilesAsync(CancellationToken cancellationToken)
    {
        return _tempFileRepository.GetTempFilesAsync(cancellationToken);
    }

    /// <inheritdoc/>
    public async Task<TempFileMetadata> HandleFileDownloadAsync(
        TempFileMetadata downloadedFile, CancellationToken cancellationToken)
    {
        var file = await _tempFileRepository.HandleFileDownloadAsync(downloadedFile, cancellationToken);
        return await CheckCacheAsync(file, cancellationToken);
    }

    /// <inheritdoc/>
    public async Task<TempFileMetadata> HandleFileUploadAsync(
        IFormFile formFile, CancellationToken cancellationToken)
    {
        var file = await _tempFileRepository.HandleFileUploadAsync(formFile, cancellationToken);
        return await CheckCacheAsync(file, cancellationToken);
    }

    /// <inheritdoc/>
    public async Task<TempFileMetadata> HandleFileUploadAsync(
        FileDetails uploadedFile, CancellationToken cancellationToken)
    {
        var file = await _tempFileRepository.HandleFileUploadAsync(uploadedFile, cancellationToken);
        return await CheckCacheAsync(file, cancellationToken);
    }

    /// <inheritdoc/>
    public Task<TempFileMetadata> UpdateTempFileAsync(
        TempFileMetadata file, string newContentType, string newExtension,
        CancellationToken cancellationToken)
    {
        return _tempFileRepository.UpdateTempFileAsync(file, newContentType, newExtension, cancellationToken);
    }

    private static string ComputeSHA384Hash(Stream stream)
    {
        stream.Position = 0L;

        using var hashStream = SHA384.Create();
        var hashBytes = hashStream.ComputeHash(stream);

        // Create a new StringBuilder to collect the bytes and create a string.
        var hashString = new StringBuilder();

        // Loop through each byte of the hashed data and format each one as a hexadecimal string.
        for (var i = 0; i < hashBytes.Length; i++)
        {
            hashString.Append(hashBytes[i].ToString("x2", CultureInfo.InvariantCulture));
        }

        // Return the hexadecimal string.
        return hashString.ToString();
    }

    /// <summary>
    ///   Checks the cache looking for a file with same hash of given <paramref name="file"/>. If it
    ///   finds a file with the same hash, then it deletes specified <paramref name="file"/> and it
    ///   returns the file found in cache. Otherwise, the file is cached and it is returned to the caller.
    /// </summary>
    /// <param name="file">
    ///   File which might be deleted if a file with the same hash exists in the cache.
    /// </param>
    /// <param name="cancellationToken">Cancellation token.</param>
    /// <returns>
    ///   The original file, if it does not exist in the cache. Otherwise, it returns the cached file.
    /// </returns>
    /// <remarks>This method is used to avoid flooding the server with the same files.</remarks>
    private async Task<TempFileMetadata> CheckCacheAsync(TempFileMetadata file, CancellationToken cancellationToken)
    {
        if (string.IsNullOrWhiteSpace(file.Path))
        {
            return file;
        }

        string hash;
        await using (var fileStream = File.OpenRead(file.Path))
        {
            hash = ComputeSHA384Hash(fileStream);
        }

        var cachedFile = await _cache.GetOrAddSlidingAsync(
            partition: nameof(TempFileMetadata),
            key: new CacheKey(hash),
            valueGetter: () => Task.FromResult(file),
            interval: _databaseConfiguration.Value.CacheLifetime,
            cancellationToken: cancellationToken);

        // A cached file path might not exists, for example, when the container crashes and
        // Thumbnailer temporary directory, if it has not been persisted, is lost.
        if (!File.Exists(cachedFile.Path))
        {
            return file;
        }

        if (cachedFile.Path != file.Path)
        {
            await DeleteTempFileAsync(file, cancellationToken);
        }

        return cachedFile;
    }
}
