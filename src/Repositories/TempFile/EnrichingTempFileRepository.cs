﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using Microsoft.AspNetCore.Http;
using PommaLabs.Thumbnailer.Core;
using PommaLabs.Thumbnailer.Models.DTO.Internal;
using PommaLabs.Thumbnailer.Models.DTO.Public;

namespace PommaLabs.Thumbnailer.Repositories.TempFile;

/// <summary>
///   Temporary file repository which enriches file information, like content type.
/// </summary>
public sealed class EnrichingTempFileRepository : ITempFileRepository
{
    private readonly ITempFileRepository _tempFileRepository;

    /// <summary>
    ///   Constructor.
    /// </summary>
    /// <param name="tempFileRepository">Temporary file repository.</param>
    public EnrichingTempFileRepository(
        ITempFileRepository tempFileRepository)
    {
        _tempFileRepository = tempFileRepository;
    }

    /// <inheritdoc/>
    public Task DeleteTempFileAsync(TempFileMetadata file, CancellationToken cancellationToken)
    {
        return _tempFileRepository.DeleteTempFileAsync(file, cancellationToken);
    }

    /// <inheritdoc/>
    public Task<long> GetAvailableFreeSpaceInBytes(CancellationToken cancellationToken)
    {
        return _tempFileRepository.GetAvailableFreeSpaceInBytes(cancellationToken);
    }

    /// <inheritdoc/>
    public async Task<TempFileMetadata> GetTempFileAsync(
        string contentType, string? extension,
        CancellationToken cancellationToken)
    {
        return await TryEnrichFileInformationAsync(await _tempFileRepository
            .GetTempFileAsync(contentType, extension, cancellationToken),
            cancellationToken);
    }

    /// <inheritdoc/>
    public Task<IEnumerable<TempFileMetadata>> GetTempFilesAsync(CancellationToken cancellationToken)
    {
        return _tempFileRepository.GetTempFilesAsync(cancellationToken);
    }

    /// <inheritdoc/>
    public async Task<TempFileMetadata> HandleFileDownloadAsync(
        TempFileMetadata downloadedFile, CancellationToken cancellationToken)
    {
        return await TryEnrichFileInformationAsync(await _tempFileRepository
            .HandleFileDownloadAsync(downloadedFile, cancellationToken),
            cancellationToken);
    }

    /// <inheritdoc/>
    public async Task<TempFileMetadata> HandleFileUploadAsync(
        IFormFile formFile, CancellationToken cancellationToken)
    {
        return await TryEnrichFileInformationAsync(await _tempFileRepository
            .HandleFileUploadAsync(formFile, cancellationToken),
            cancellationToken);
    }

    /// <inheritdoc/>
    public async Task<TempFileMetadata> HandleFileUploadAsync(
        FileDetails uploadedFile, CancellationToken cancellationToken)
    {
        return await TryEnrichFileInformationAsync(await _tempFileRepository
            .HandleFileUploadAsync(uploadedFile, cancellationToken),
            cancellationToken);
    }

    /// <inheritdoc/>
    public async Task<TempFileMetadata> UpdateTempFileAsync(
        TempFileMetadata file, string newContentType, string newExtension,
        CancellationToken cancellationToken)
    {
        if (file.Extension == newExtension)
        {
            return new TempFileMetadata(newContentType, newExtension) { Path = file.Path };
        }

        return await _tempFileRepository.UpdateTempFileAsync(
            file, newContentType, newExtension,
            cancellationToken);
    }

    private async Task<TempFileMetadata> TryEnrichFileInformationAsync(
        TempFileMetadata file, CancellationToken cancellationToken)
    {
        // We work on files which have been stored on disk. Therefore, following method can inspect
        // file signature and can get further information about current file.
        var (newContentType, newExtension) = MimeTypeHelper.GetMimeTypeAndExtension(file.Path, file.ContentType);

        if (newContentType == file.ContentType && newExtension == file.Extension)
        {
            // No new information has been gathered. Therefore, we return received file.
            return file;
        }

        // Update temporary file according to new information.
        return await UpdateTempFileAsync(
            file, newContentType, newExtension,
            cancellationToken);
    }
}
