﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System.Collections.Concurrent;
using Microsoft.Extensions.Options;
using NodaTime;
using PommaLabs.Thumbnailer.Models.Configurations;
using PommaLabs.Thumbnailer.Models.DTO.Internal;

namespace PommaLabs.Thumbnailer.Repositories.ApiKey;

/// <summary>
///   An API key repository which stores temporary API keys in memory.
/// </summary>
public sealed class MemoryApiKeyRepository : ApiKeyRepositoryBase
{
    private readonly ConcurrentDictionary<string, ApiKeyConfiguration> _apiKeys = new();
    private readonly IClock _clock;

    /// <summary>
    ///   Constructor.
    /// </summary>
    /// <param name="securityConfiguration">Security configuration.</param>
    /// <param name="clock">Clock.</param>
    public MemoryApiKeyRepository(
        IOptions<SecurityConfiguration> securityConfiguration,
        IClock clock)
        : base(securityConfiguration)
    {
        _clock = clock;
    }

    /// <inheritdoc/>
    public override Task<ApiKeyCredentials> AddTempApiKeyAsync(string key, CancellationToken cancellationToken)
    {
        var utcNow = _clock.GetCurrentInstant().ToDateTimeOffset();
        var tempApiKey = new ApiKeyConfiguration
        {
            Name = GetTempApiKeyName(utcNow),
            Value = key,
            ExpiresAt = utcNow.Add(SecurityConfiguration.Value.TempApiKeyLifetime),
        };

        _apiKeys.TryAdd(tempApiKey.Value, tempApiKey);

        return Task.FromResult(new ApiKeyCredentials
        {
            Name = tempApiKey.Name,
            Value = tempApiKey.Value,
            ExpiresAt = tempApiKey.ExpiresAt,
            IsValid = true,
        });
    }

    /// <inheritdoc/>
    public override Task<int> DeleteExpiredTempApiKeysAsync(CancellationToken cancellationToken)
    {
        var expiredApiKeys = _apiKeys.Values
            .Where(x => x.ExpiresAt < _clock.GetCurrentInstant().ToDateTimeOffset())
            .Select(x => x.Value)
            .ToList();

        var deletedCount = 0;
        foreach (var ak in expiredApiKeys)
        {
            deletedCount += _apiKeys.TryRemove(ak, out _) ? 1 : 0;
        }

        return Task.FromResult(deletedCount);
    }

    /// <inheritdoc/>
    public override Task<ApiKeyCredentials?> ValidateApiKeyAsync(string key, CancellationToken cancellationToken)
    {
        return Task.FromResult(_apiKeys.Values
            .Where(x => ApiKeysMatch(x.Value, key) && x.ExpiresAt >= _clock.GetCurrentInstant().ToDateTimeOffset())
            .Select(x => new ApiKeyCredentials
            {
                Name = x.Name,
                Value = x.Value,
                ExpiresAt = x.ExpiresAt,
                IsValid = true,
            })
            .SingleOrDefault());
    }
}
