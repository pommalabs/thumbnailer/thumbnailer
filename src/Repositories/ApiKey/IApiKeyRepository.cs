﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using PommaLabs.Thumbnailer.Models.DTO.Internal;

namespace PommaLabs.Thumbnailer.Repositories.ApiKey;

/// <summary>
///   API key repository.
/// </summary>
public interface IApiKeyRepository
{
    /// <summary>
    ///   Creates a new temporary API key.
    /// </summary>
    /// <param name="key">API key to be temporarily added.</param>
    /// <returns>A new temporary API key.</returns>
    /// <param name="cancellationToken">Cancellation token.</param>
    Task<ApiKeyCredentials> AddTempApiKeyAsync(string key, CancellationToken cancellationToken);

    /// <summary>
    ///   Deletes all temporary API keys which have expired.
    /// </summary>
    /// <param name="cancellationToken">Cancellation token.</param>
    /// <returns>The number of API keys deleted.</returns>
    Task<int> DeleteExpiredTempApiKeysAsync(CancellationToken cancellationToken);

    /// <summary>
    ///   Tries to validate given API key.
    /// </summary>
    /// <param name="key">API key to be validated.</param>
    /// <param name="cancellationToken">Cancellation token.</param>
    /// <returns>True if given <paramref name="key"/> has been validated, false otherwise.</returns>
    Task<ApiKeyCredentials?> ValidateApiKeyAsync(string key, CancellationToken cancellationToken);
}
