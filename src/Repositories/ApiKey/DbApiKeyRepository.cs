﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using Microsoft.Extensions.Options;
using NodaTime;
using PommaLabs.Thumbnailer.Models.Configurations;
using PommaLabs.Thumbnailer.Models.DTO.Internal;
using SqlKata.Execution;

namespace PommaLabs.Thumbnailer.Repositories.ApiKey;

/// <summary>
///   An API key repository which looks for keys on a SQL database.
/// </summary>
public sealed class DbApiKeyRepository : ApiKeyRepositoryBase
{
    private readonly IClock _clock;
    private readonly QueryFactory _db;
    private readonly IOptions<DatabaseConfiguration> _dbc;

    /// <summary>
    ///   Constructor.
    /// </summary>
    /// <param name="securityConfiguration">Security configuration.</param>
    /// <param name="db">Query factory.</param>
    /// <param name="dbc">Database configuration.</param>
    /// <param name="clock">Clock.</param>
    public DbApiKeyRepository(
        IOptions<SecurityConfiguration> securityConfiguration,
        QueryFactory db,
        IOptions<DatabaseConfiguration> dbc,
        IClock clock)
        : base(securityConfiguration)
    {
        _db = db;
        _dbc = dbc;
        _clock = clock;
    }

    /// <inheritdoc/>
    public override async Task<ApiKeyCredentials> AddTempApiKeyAsync(string key, CancellationToken cancellationToken)
    {
        var utcNow = _clock.GetCurrentInstant().ToDateTimeOffset();
        var tempApiKey = new
        {
            name = GetTempApiKeyName(utcNow),
            value = key,
            expires_at = utcNow.Add(SecurityConfiguration.Value.TempApiKeyLifetime),
            temporary = true
        };

        var apiKeyExists = await _db
            .Query($"{_dbc.Value.SchemaNameWithDot}api_keys")
            .Where("value", "=", key)
            .SelectRaw("1")
            .CountAsync<int>(cancellationToken: cancellationToken) > 0;

        if (!apiKeyExists)
        {
            await _db
                .Query($"{_dbc.Value.SchemaNameWithDot}api_keys")
                .InsertAsync(tempApiKey, cancellationToken: cancellationToken);
        }

        return new ApiKeyCredentials
        {
            Name = tempApiKey.name,
            Value = tempApiKey.value,
            ExpiresAt = tempApiKey.expires_at,
            IsValid = true,
        };
    }

    /// <inheritdoc/>
    public override async Task<int> DeleteExpiredTempApiKeysAsync(CancellationToken cancellationToken)
    {
        return await _db
            .Query($"{_dbc.Value.SchemaNameWithDot}api_keys")
            .WhereTrue("temporary")
            .Where("expires_at", "<", _clock.GetCurrentInstant().ToDateTimeOffset())
            .DeleteAsync(cancellationToken: cancellationToken);
    }

    /// <inheritdoc/>
    public override async Task<ApiKeyCredentials?> ValidateApiKeyAsync(string key, CancellationToken cancellationToken)
    {
        return (await _db
            .Query($"{_dbc.Value.SchemaNameWithDot}api_keys")
            .Where(q => q.WhereNull("expires_at").OrWhere("expires_at", ">=", _clock.GetCurrentInstant().ToDateTimeOffset()))
            .Select($"name as {nameof(ApiKeyCredentials.Name)}")
            .Select($"value as {nameof(ApiKeyCredentials.Value)}")
            .Select($"expires_at as {nameof(ApiKeyCredentials.ExpiresAt)}")
            .SelectRaw($"1 as {nameof(ApiKeyCredentials.IsValid)}")
            .GetAsync<ApiKeyCredentials>(cancellationToken: cancellationToken))
            .FirstOrDefault(x => ApiKeysMatch(x.Value, key));
    }
}
