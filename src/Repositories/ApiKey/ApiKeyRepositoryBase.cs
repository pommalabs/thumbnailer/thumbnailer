﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System.Security.Cryptography;
using System.Text;
using Microsoft.Extensions.Options;
using PommaLabs.Thumbnailer.Models.Configurations;
using PommaLabs.Thumbnailer.Models.DTO.Internal;

namespace PommaLabs.Thumbnailer.Repositories.ApiKey;

/// <summary>
///   Base class for API key repositories.
/// </summary>
public abstract class ApiKeyRepositoryBase : IApiKeyRepository
{
    /// <summary>
    ///   Constructor.
    /// </summary>
    /// <param name="securityConfiguration">Security configuration.</param>
    protected ApiKeyRepositoryBase(IOptions<SecurityConfiguration> securityConfiguration)
    {
        SecurityConfiguration = securityConfiguration;
    }

    /// <summary>
    ///   Security configuration.
    /// </summary>
    protected IOptions<SecurityConfiguration> SecurityConfiguration { get; }

    /// <inheritdoc/>
    public abstract Task<ApiKeyCredentials> AddTempApiKeyAsync(string key, CancellationToken cancellationToken);

    /// <inheritdoc/>
    public abstract Task<int> DeleteExpiredTempApiKeysAsync(CancellationToken cancellationToken);

    /// <inheritdoc/>
    public abstract Task<ApiKeyCredentials?> ValidateApiKeyAsync(string key, CancellationToken cancellationToken);

    /// <summary>
    ///   Returns true if provided API keys match, false otherwise.
    /// </summary>
    /// <param name="expected">Expected API key.</param>
    /// <param name="received">Received API key.</param>
    /// <returns>True if provided API keys match, false otherwise.</returns>
    protected bool ApiKeysMatch(string expected, string received)
    {
        if (!SecurityConfiguration.Value.EnableCaseSensitiveApiKeyValidation)
        {
            expected = expected.ToUpperInvariant();
            received = received.ToUpperInvariant();
        }
        var expectedHash = ComputeSHA384Hash(expected);
        var receivedHash = ComputeSHA384Hash(received);
        return CryptographicOperations.FixedTimeEquals(expectedHash, receivedHash);
    }

    /// <summary>
    ///   Returns a name which should be used for temporary API keys.
    /// </summary>
    /// <param name="utcNow">UTC now.</param>
    /// <returns>A name which should be used for temporary API keys.</returns>
    protected static string GetTempApiKeyName(DateTimeOffset utcNow)
    {
        return $"TMP#{utcNow.ToUnixTimeSeconds()}";
    }

    private static byte[] ComputeSHA384Hash(string str)
    {
        var strBytes = Encoding.UTF8.GetBytes(str);
        return SHA384.HashData(strBytes);
    }
}
