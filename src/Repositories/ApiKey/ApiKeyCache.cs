﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using PommaLabs.Salatino.Services;
using PommaLabs.Thumbnailer.Models.DTO.Internal;

namespace PommaLabs.Thumbnailer.Repositories.ApiKey;

public sealed class ApiKeyCache(int cacheSize) : ValidationCacheBase<string, ApiKeyCredentials>(cacheSize);
