﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using PommaLabs.Thumbnailer.Models.DTO.Internal;

namespace PommaLabs.Thumbnailer.Repositories.ApiKey;

/// <summary>
///   API key repository which applies caching policies.
/// </summary>
public sealed class CachingApiKeyRepository : IApiKeyRepository
{
    private readonly IApiKeyRepository _apiKeyRepository;
    private readonly ApiKeyCache _cache;

    /// <summary>
    ///   Constructor.
    /// </summary>
    /// <param name="apiKeyRepository">API key repository.</param>
    /// <param name="cache">Cache.</param>
    public CachingApiKeyRepository(
        IApiKeyRepository apiKeyRepository,
        ApiKeyCache cache)
    {
        _apiKeyRepository = apiKeyRepository;
        _cache = cache;
    }

    /// <inheritdoc/>
    public async Task<ApiKeyCredentials> AddTempApiKeyAsync(string key, CancellationToken cancellationToken)
    {
        var result = await _apiKeyRepository.AddTempApiKeyAsync(key, cancellationToken);
        _cache.Clear();
        return result;
    }

    /// <inheritdoc/>
    public async Task<int> DeleteExpiredTempApiKeysAsync(CancellationToken cancellationToken)
    {
        var result = await _apiKeyRepository.DeleteExpiredTempApiKeysAsync(cancellationToken);
        _cache.Clear();
        return result;
    }

    /// <inheritdoc/>
    public async Task<ApiKeyCredentials?> ValidateApiKeyAsync(string key, CancellationToken cancellationToken)
    {
        if (_cache.TryGetValue(key, out var apiKey))
        {
            return apiKey;
        }

        apiKey = await _apiKeyRepository.ValidateApiKeyAsync(key, cancellationToken);
        if (apiKey?.IsValid == true)
        {
            _cache.Add(key, apiKey, apiKey.ExpiresAt ?? DateTimeOffset.MaxValue);
        }

        return apiKey;
    }
}
