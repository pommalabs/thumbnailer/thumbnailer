﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using Microsoft.Extensions.Options;
using NodaTime;
using PommaLabs.Salatino.Models.Options;
using PommaLabs.Thumbnailer.Models.Configurations;
using PommaLabs.Thumbnailer.Models.DTO.Internal;

namespace PommaLabs.Thumbnailer.Repositories.ApiKey;

/// <summary>
///   An API key repository which looks for keys on the application configuration file.
/// </summary>
public sealed class ConfigurationApiKeyRepository : ApiKeyRepositoryBase
{
    private readonly IOptions<SecurityOptions> _securityOptions;
    private readonly IApiKeyRepository _apiKeyRepository;
    private readonly IClock _clock;

    /// <summary>
    ///   Constructor.
    /// </summary>
    /// <param name="securityOptions">Security options.</param>
    /// <param name="additionalSecurityOptions">Additional security options.</param>
    /// <param name="apiKeyRepository">API key repository.</param>
    /// <param name="clock">Clock.</param>
    public ConfigurationApiKeyRepository(
        IOptions<SecurityOptions> securityOptions,
        IOptions<SecurityConfiguration> additionalSecurityOptions,
        IApiKeyRepository apiKeyRepository,
        IClock clock)
        : base(additionalSecurityOptions)
    {
        _securityOptions = securityOptions;
        _apiKeyRepository = apiKeyRepository;
        _clock = clock;
    }

    /// <inheritdoc/>
    public override Task<ApiKeyCredentials> AddTempApiKeyAsync(string key, CancellationToken cancellationToken)
    {
        return _apiKeyRepository.AddTempApiKeyAsync(key, cancellationToken);
    }

    /// <inheritdoc/>
    public override Task<int> DeleteExpiredTempApiKeysAsync(CancellationToken cancellationToken)
    {
        return _apiKeyRepository.DeleteExpiredTempApiKeysAsync(cancellationToken);
    }

    /// <inheritdoc/>
    public override async Task<ApiKeyCredentials?> ValidateApiKeyAsync(string key, CancellationToken cancellationToken)
    {
        var matchedApiKey = _securityOptions.Value.ApiKeys
            .Where(x => ApiKeysMatch(x.Value, key) && (x.ExpiresAt == null || x.ExpiresAt >= _clock.GetCurrentInstant().ToDateTimeOffset()))
            .OrderBy(x => x.Name)
            .Select(x => new ApiKeyCredentials
            {
                Name = x.Name,
                Value = x.Value,
                ExpiresAt = x.ExpiresAt,
                IsValid = true,
            })
            .FirstOrDefault();

        return matchedApiKey ?? await _apiKeyRepository.ValidateApiKeyAsync(key, cancellationToken);
    }
}
