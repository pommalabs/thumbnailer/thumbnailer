﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System.Diagnostics;
using System.Reflection;

namespace PommaLabs.Thumbnailer.Core;

/// <summary>
///   Activity source, used to generate telemetry data.
/// </summary>
public static class ThumbnailerActivitySource
{
    private static readonly AssemblyName s_assemblyName = typeof(ThumbnailerActivitySource).Assembly.GetName();

    /// <summary>
    ///   Activity source instance.
    /// </summary>
    public static ActivitySource Instance { get; } = new(s_assemblyName.Name!.Replace(".Core", string.Empty), s_assemblyName.Version!.ToString());
}
