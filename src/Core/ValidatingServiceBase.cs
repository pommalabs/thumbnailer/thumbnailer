﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System.Diagnostics;
using Microsoft.Extensions.Logging;

namespace PommaLabs.Thumbnailer.Core;

public abstract class ValidatingServiceBase(ILogger logger)
{
    [DebuggerStepThrough]
    protected static void ThrowWhen(bool condition, string message)
    {
        if (condition)
        {
            throw new InvalidOperationException(message);
        }
    }

    [DebuggerStepThrough]
    protected void WarnWhen(bool condition, string message)
    {
        // Warning messages should not be emitted only in debug:
        // they might contain useful diagnostic information.
        if (condition && logger.IsEnabled(LogLevel.Warning))
        {
            logger.LogWarning("Validation assert failed - {Message}", message);
        }
    }
}
