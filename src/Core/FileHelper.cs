﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

namespace PommaLabs.Thumbnailer.Core;

/// <summary>
///   Helper methods for file management. Please keep this class as small as possible.
/// </summary>
public static class FileHelper
{
    /// <summary>
    ///   Removes invalid characters which might be contained in given file name.
    /// </summary>
    /// <param name="fileName">File name.</param>
    /// <returns>Cleaned up file name.</returns>
    public static string? CleanupFileName(string? fileName)
    {
        return fileName?.Replace("\"", string.Empty);
    }
}
