﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using Microsoft.Extensions.Logging;
using PommaLabs.MimeTypes;

namespace PommaLabs.Thumbnailer.Core;

/// <summary>
///   Default parameter values and validations.
/// </summary>
public sealed class Validator
{
    /// <summary>
    ///   Thumbnail shave pixels.
    /// </summary>
    public const ushort ThumbnailShavePx = 0;

    /// <summary>
    ///   Thumbnail side pixels.
    /// </summary>
    public const ushort ThumbnailSidePx = 256;

    /// <summary>
    ///   Content types **not** supported by thumbnail generation.
    /// </summary>
    private static readonly HashSet<string> s_contentTypesNotSupportedForThumbnailGeneration = new(StringComparer.OrdinalIgnoreCase)
    {
        MimeTypeMap.APPLICATION.OCTET_STREAM,
        MimeTypeMap.APPLICATION.X_MS_DOS_EXECUTABLE,
        MimeTypeMap.APPLICATION.X_MSDOS_PROGRAM,
        MimeTypeMap.APPLICATION.X_MSDOWNLOAD,
        MimeTypeMap.APPLICATION.X_ZIP_COMPRESSED,
        MimeTypeMap.APPLICATION.ZIP,
    };

    /// <summary>
    ///   Content types supported by media optimization.
    /// </summary>
    private static readonly HashSet<string> s_contentTypesSupportedForMediaOptimization = new(StringComparer.OrdinalIgnoreCase)
    {
        MimeTypeMap.IMAGE.GIF,
        MimeTypeMap.IMAGE.JPEG,
        MimeTypeMap.IMAGE.PNG,
        MimeTypeMap.IMAGE.SVG_XML,
        MimeTypeMap.IMAGE.WEBP,
        MimeTypeMap.VIDEO.MP4,
    };

    /// <summary>
    ///   Content types supported by placeholder generation.
    /// </summary>
    private static readonly HashSet<string> s_contentTypesSupportedForImagePlaceholderGeneration = new(StringComparer.OrdinalIgnoreCase)
    {
        MimeTypeMap.IMAGE.JPEG,
        MimeTypeMap.IMAGE.PNG,
        MimeTypeMap.IMAGE.WEBP,
    };

    /// <summary>
    ///   Logger.
    /// </summary>
    private readonly ILogger _logger;

    /// <summary>
    ///   Constructor.
    /// </summary>
    /// <param name="logger">Logger.</param>
    public Validator(ILogger logger)
    {
        _logger = logger;
    }

    /// <summary>
    ///   Validates content type for media file optimization.
    /// </summary>
    /// <param name="contentType">Content type.</param>
    public void ValidateContentTypeForMediaOptimization(string contentType)
    {
        if (s_contentTypesSupportedForMediaOptimization.Contains(contentType)) return;

        _logger.LogError("An invalid content type was specified as input: {ContentType}", contentType);
        throw new NotSupportedException($"Content type '{contentType}' is not supported for media optimization");
    }

    /// <summary>
    ///   Validates content type for thumbnail generation.
    /// </summary>
    /// <param name="contentType">Content type.</param>
    public void ValidateContentTypeForThumbnailGeneration(string contentType)
    {
        if (!s_contentTypesNotSupportedForThumbnailGeneration.Contains(contentType)) return;

        _logger.LogError("An invalid content type was specified as input: {ContentType}", contentType);
        throw new NotSupportedException($"Content type '{contentType}' is not supported for thumbnail generation");
    }

    /// <summary>
    ///   Validates content type for image placeholder generation.
    /// </summary>
    /// <param name="contentType">Content type.</param>
    public void ValidateContentTypeForImagePlaceholderGeneration(string contentType)
    {
        if (s_contentTypesSupportedForImagePlaceholderGeneration.Contains(contentType)) return;

        _logger.LogError("An invalid content type was specified as input: {ContentType}", contentType);
        throw new NotSupportedException($"Content type '{contentType}' is not supported for image placeholder generation");
    }

    /// <summary>
    ///   Validates file URI parameter.
    /// </summary>
    /// <param name="fileUri">File URI.</param>
    public void ValidateFileUri(Uri fileUri)
    {
        ArgumentNullException.ThrowIfNull(fileUri);
        if (!fileUri.IsAbsoluteUri)
        {
            _logger.LogError("A relative URI was specified as input: {FileUri}", fileUri);
            throw new NotSupportedException("Relative URIs are not allowed");
        }
        if (!string.Equals(fileUri.Scheme, "http", StringComparison.OrdinalIgnoreCase) && !string.Equals(fileUri.Scheme, "https", StringComparison.OrdinalIgnoreCase))
        {
            _logger.LogError("An URI with invalid scheme was specified as input: {FileUri}", fileUri);
            throw new NotSupportedException("URIs with scheme different from http/https are not allowed");
        }
        if (fileUri.IsLoopback)
        {
            _logger.LogError("A loopback URI was specified as input: {FileUri}", fileUri);
            throw new NotSupportedException("Loopback URIs are not allowed");
        }
    }
}
