﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using PommaLabs.Thumbnailer.Core;
using Serilog.Context;

namespace PommaLabs.Thumbnailer.Server.Middlewares;

/// <summary>
///   A middleware component which adds API key name to each log message.
/// </summary>
public sealed class LogApiKeyNameMiddleware
{
    private readonly RequestDelegate _next;

    /// <summary>
    ///   Constructor.
    /// </summary>
    /// <param name="next">Next middleware.</param>
    public LogApiKeyNameMiddleware(RequestDelegate next)
    {
        _next = next;
    }

    /// <inheritdoc/>
    public async Task InvokeAsync(HttpContext context)
    {
        using (LogContext.PushProperty("ApiKeyName", context.User.Identity?.Name ?? Constants.AnonymousIdentityName))
        {
            await _next(context);
        }
    }
}
