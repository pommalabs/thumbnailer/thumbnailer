﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System.Diagnostics.CodeAnalysis;
using System.Runtime.CompilerServices;
using System.Text.Json.Serialization;
using AspNetCore.Authentication.ApiKey;
using Hellang.Middleware.ProblemDetails;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.DataProtection;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using PommaLabs.HtmlArk;
using PommaLabs.KVLite.AspNetCore;
using PommaLabs.Salatino.Models.Options;
using PommaLabs.Thumbnailer.Core;
using PommaLabs.Thumbnailer.Models.Configurations;
using PommaLabs.Thumbnailer.Models.Enumerations;
using PommaLabs.Thumbnailer.Models.Exceptions;
using PommaLabs.Thumbnailer.Repositories.ApiKey;
using PommaLabs.Thumbnailer.Repositories.TempFile;
using PommaLabs.Thumbnailer.Server;
using PommaLabs.Thumbnailer.Server.ApiKeyProviders;
using PommaLabs.Thumbnailer.Server.AuthenticationHandlers;
using PommaLabs.Thumbnailer.Server.BackgroundServices;
using PommaLabs.Thumbnailer.Server.ConfigureOptions;
using PommaLabs.Thumbnailer.Server.HealthChecks;
using PommaLabs.Thumbnailer.Server.Middlewares;
using PommaLabs.Thumbnailer.Services.Download;
using PommaLabs.Thumbnailer.Services.FallbackImage;
using PommaLabs.Thumbnailer.Services.Html;
using PommaLabs.Thumbnailer.Services.ImagePlaceholder;
using PommaLabs.Thumbnailer.Services.Job;
using PommaLabs.Thumbnailer.Services.Optimization;
using PommaLabs.Thumbnailer.Services.Process;
using PommaLabs.Thumbnailer.Services.Thumbnail;
using Serilog;
using Serilog.Events;
using ApiKeyCache = PommaLabs.Thumbnailer.Repositories.ApiKey.ApiKeyCache;

[assembly: InternalsVisibleTo("PommaLabs.Thumbnailer.UnitTests")]
var builder = WebApplication.CreateBuilder(args);
var configuration = builder.Configuration;
var services = builder.Services;

configuration.MapEnvironmentVariablesToConfigurationEntries();

// Add services to the container.

services.AddSalatino(configuration, lc => lc
    .MinimumLevel.Override("Npgsql", LogEventLevel.Warning)
    // Exclude jobs query API, because it can fill up the log without providing any value. Should it
    // produce an error, it would be logged anyway.
    .Filter.ByExcluding("RequestPath like '/api/%/jobs/%' and RequestPath not like '/api/%/jobs/%/download' and StatusCode = 200"));

// Configuration classes.
services.Configure<DatabaseConfiguration>(configuration.GetSection("Database"));
services.Configure<JobProcessorConfiguration>(configuration.GetSection("JobProcessor"));
services.Configure<SecurityConfiguration>(configuration.GetSection("Security"));
services.Configure<WebsiteConfiguration>(configuration.GetSection("Website"));

var databaseConfiguration = configuration.GetSection("Database").Get<DatabaseConfiguration>() ?? new DatabaseConfiguration();
var jobProcessorConfiguration = configuration.GetSection("JobProcessor").Get<JobProcessorConfiguration>() ?? new JobProcessorConfiguration();
var securityConfiguration = configuration.GetSection("Security").Get<SecurityConfiguration>() ?? new SecurityConfiguration();
var websiteConfiguration = configuration.GetSection("Website").Get<WebsiteConfiguration>() ?? new WebsiteConfiguration();

// API keys.
services.AddSingleton(sp =>
{
    var securityOptions = sp.GetRequiredService<IOptions<SecurityOptions>>();
    return new ApiKeyCache(securityOptions.Value.ApiKeyCacheSize);
});
if (databaseConfiguration.Provider != DatabaseProvider.None)
{
    services.AddScoped(_ => QueryFactoryHelper.GetQueryFactory(databaseConfiguration));
    services.AddScoped<IApiKeyRepository, DbApiKeyRepository>();
}
else
{
    services.AddSingleton<IApiKeyRepository, MemoryApiKeyRepository>();
}
services.Decorate<IApiKeyRepository, ConfigurationApiKeyRepository>();
services.Decorate<IApiKeyRepository, CachingApiKeyRepository>();

// KVLite cache.
const bool AutoCreateCacheSchema = false;
const string CacheEntriesTableName = "cache_entries";
switch (databaseConfiguration.Provider)
{
    case DatabaseProvider.MySql:
        services.AddKVLiteMySqlCache(options =>
        {
            options.AutoCreateCacheSchema = AutoCreateCacheSchema;
            options.ConnectionString = databaseConfiguration.ConnectionString;
            options.CacheEntriesTableName = CacheEntriesTableName;
            options.CacheSchemaName = databaseConfiguration.SchemaName;
        });
        break;

    case DatabaseProvider.PostgreSql:
        services.AddKVLitePostgreSqlCache(options =>
        {
            options.AutoCreateCacheSchema = AutoCreateCacheSchema;
            options.ConnectionString = databaseConfiguration.ConnectionString;
            options.CacheEntriesTableName = CacheEntriesTableName;
            options.CacheSchemaName = databaseConfiguration.SchemaName;
        });
        break;

    case DatabaseProvider.SqlServer:
        services.AddKVLiteSqlServerCache(options =>
        {
            options.AutoCreateCacheSchema = AutoCreateCacheSchema;
            options.ConnectionString = databaseConfiguration.ConnectionString;
            options.CacheEntriesTableName = CacheEntriesTableName;
            options.CacheSchemaName = databaseConfiguration.SchemaName;
        });
        break;

    default:
        services.AddKVLiteMemoryCache();
        break;
}

// ASP.NET Core.
services.AddProblemDetails(options =>
{
    options.MapToStatusCode<ArgumentException>(StatusCodes.Status400BadRequest);
    options.MapToStatusCode<ArgumentNullException>(StatusCodes.Status400BadRequest);
    options.MapToStatusCode<ProcessException>(StatusCodes.Status422UnprocessableEntity);
    options.MapToStatusCode<InvalidOperationException>(StatusCodes.Status424FailedDependency);
    options.MapToStatusCode<NotSupportedException>(StatusCodes.Status403Forbidden);
    options.MapToStatusCode<Exception>(StatusCodes.Status500InternalServerError);
});

services.AddControllers()
    .AddJsonOptions(options => options.JsonSerializerOptions.Converters.Add(new JsonStringEnumConverter()));

services.Configure<FormOptions>(options => options.MultipartBodyLengthLimit = securityConfiguration.MaxFileUploadSizeInBytes);

services.PostConfigure<ApiBehaviorOptions>(options =>
{
    var builtInFactory = options.InvalidModelStateResponseFactory;

    options.InvalidModelStateResponseFactory = context =>
    {
        var loggerFactory = context.HttpContext.RequestServices.GetRequiredService<ILoggerFactory>();
        var logger = loggerFactory.CreateLogger(context.ActionDescriptor.DisplayName!);

        var modelValidationErrors = context.ModelState
            .Where(x => x.Value?.Errors.Any() ?? false)
            .SelectMany(x => x.Value!.Errors.Select(e => new { FieldName = x.Key, Error = e }))
            .ToList();

        foreach (var mve in modelValidationErrors)
        {
            logger.LogWarning(mve.Error.Exception, "Field {FieldName} is not valid: {ValidationErrorMessage}", mve.FieldName, mve.Error.ErrorMessage);
        }

        return builtInFactory(context);
    };
});

// Data protection keys, used to protect fair use tokens.
services.AddDataProtection()
    .PersistKeysToFileSystem(new DirectoryInfo(Constants.TempDirectory + "/data-protection-keys"))
    .SetApplicationName("Thumbnailer");

// Razor Pages.
services.AddRazorPages().AddRazorRuntimeCompilation();

// Swagger UI.
services.ConfigureOptions<ConfigureSwaggerGenOptions>();

// Health checks.
services.AddHealthChecks()
    .AddCheck<ApiKeyRepositoryHealthCheck>(nameof(ApiKeyRepositoryHealthCheck))
    .AddCheck<KVLiteHealthCheck>(nameof(KVLiteHealthCheck))
    .AddCheck<TempFileRepositoryHealthCheck>(nameof(TempFileRepositoryHealthCheck));

// Authentication handlers.
var authenticationSchemes = new List<string>();
var authenticationBuilder = services.AddAuthentication();

services.AddScoped<InternalApiKeyProvider>();
if (securityConfiguration.AcceptApiKeysViaHeaderParameter)
{
    authenticationSchemes.Add(Constants.ApiKeyViaHeaderAuthenticationScheme);
    authenticationBuilder.AddApiKeyInHeader<InternalApiKeyProvider>(Constants.ApiKeyViaHeaderAuthenticationScheme, options =>
    {
        options.Realm = nameof(PommaLabs.Thumbnailer);
        options.KeyName = Constants.ApiKeyHeaderName;
        options.IgnoreAuthenticationIfAllowAnonymous = true;
    });
}
if (securityConfiguration.AcceptApiKeysViaQueryStringParameter)
{
    authenticationSchemes.Add(Constants.ApiKeyViaQueryParameterAuthenticationScheme);
    authenticationBuilder.AddApiKeyInQueryParams<InternalApiKeyProvider>(Constants.ApiKeyViaQueryParameterAuthenticationScheme, options =>
    {
        options.Realm = nameof(PommaLabs.Thumbnailer);
        options.KeyName = Constants.ApiKeyQueryParameterName;
        options.IgnoreAuthenticationIfAllowAnonymous = true;
    });
}

if (securityConfiguration.AllowAnonymousAccess)
{
    authenticationSchemes.Add(Constants.AnonymousAuthenticationScheme);
    authenticationBuilder.AddScheme<AuthenticationSchemeOptions, AnonymousAuthenticationHandler>(Constants.AnonymousAuthenticationScheme, default);
}

services.AddAuthorization(options =>
{
    // Allow multiple authorization schemes.
    var defaultAuthorizationPolicyBuilder = new AuthorizationPolicyBuilder(authenticationSchemes.ToArray());

    defaultAuthorizationPolicyBuilder = defaultAuthorizationPolicyBuilder.RequireAuthenticatedUser();
    options.DefaultPolicy = defaultAuthorizationPolicyBuilder.Build();
});

// HTML archiver.
services.AddSingleton<IHtmlArchiver, HtmlArchiver>();

// Background services.
services.AddHostedService<CleanupTempApiKeysBackgroundService>();
services.AddHostedService<CleanupTempFilesBackgroundService>();

for (var i = 1; i <= jobProcessorConfiguration.Count; ++i)
{
    var processorId = i;
    services.AddSingleton<IHostedService>(sp => new ProcessQueuedJobsBackgroundService(
        processorId: processorId,
        logger: sp.GetRequiredService<ILogger<ProcessQueuedJobsBackgroundService>>(),
        jobService: sp.GetRequiredService<IJobService>(),
        optimizationService: sp.GetRequiredService<IOptimizationService>(),
        thumbnailService: sp.GetRequiredService<IThumbnailService>(),
        imagePlaceholderService: sp.GetRequiredService<IImagePlaceholderService>()));
}

// Repositories.
services.AddSingleton<ITempFileRepository, SystemTempFileRepository>();
services.Decorate<ITempFileRepository, ValidatingTempFileRepository>();
services.Decorate<ITempFileRepository, EnrichingTempFileRepository>();
services.Decorate<ITempFileRepository, CachingTempFileRepository>();

// Services.
services.AddSingleton<IDownloadService, HttpDownloadService>();
services.Decorate<IDownloadService, CachingDownloadService>();
services.Decorate<IDownloadService, ValidatingDownloadService>();

services.AddSingleton<IFallbackImageService, ConcreteFallbackImageService>();

services.AddSingleton<IHtmlService, ConcreteHtmlService>();

services.AddSingleton<IImagePlaceholderService, ConcreteImagePlaceholderService>();
services.Decorate<IImagePlaceholderService, CachingImagePlaceholderService>();
services.Decorate<IImagePlaceholderService, TracingPlaceholderService>();
services.Decorate<IImagePlaceholderService, ScalingImagePlaceholderService>();
services.Decorate<IImagePlaceholderService, ValidatingImagePlaceholderService>();

services.AddSingleton<IJobService, ConcurrentJobService>();
services.Decorate<IJobService, ValidatingJobService>();

services.AddSingleton<IOptimizationService, ConcreteOptimizationService>();
services.Decorate<IOptimizationService, CachingOptimizationService>();
services.Decorate<IOptimizationService, PlaceholderOptimizationService>();
services.Decorate<IOptimizationService, TracingOptimizationService>();
services.Decorate<IOptimizationService, ValidatingOptimizationService>();

services.AddSingleton<IProcessService, SystemProcessService>();

services.AddSingleton<IThumbnailService, ConcreteThumbnailService>();
services.Decorate<IThumbnailService, FallbackThumbnailService>();
services.Decorate<IThumbnailService, CachingThumbnailService>();
services.Decorate<IThumbnailService, OptimizingThumbnailService>();
services.Decorate<IThumbnailService, PlaceholderThumbnailService>();
services.Decorate<IThumbnailService, TracingThumbnailService>();
services.Decorate<IThumbnailService, ValidatingThumbnailService>();

builder.WebHost.DisableServerHeader().UsePortEnvironmentVariable();

var app = builder.Build();

// Configure the HTTP request pipeline.

if (websiteConfiguration.Enabled)
{
    // Security headers.
    app.UseReferrerPolicy(options => options.NoReferrer());
    app.UseXContentTypeOptions();
    app.UseXDownloadOptions();
    app.UseXfo(options => options.Deny());
    app.UseXXssProtection(options => options.EnabledWithBlockMode());
    app.UseCsp(options => options
        .DefaultSources(c => c.None())
        .ConnectSources(c => c.Self())
        .FontSources(c => c.Self().CustomSources("fonts.gstatic.com"))
        .FormActions(c => c.Self())
        .ImageSources(c => c.Self().CustomSources("data:", "www.gstatic.com"))
        .ScriptSources(c => c.Self().UnsafeInline().CustomSources("www.gstatic.com"))
        .StyleSources(c => c.Self().UnsafeInline().CustomSources("fonts.googleapis.com")));

    // Expose static files only when website has been enabled, because they are not needed otherwise.
    app.UseStaticFiles();
}
else
{
    app.UseXRobotsTag(options => options.NoIndex().NoFollow());
}

app.UseProblemDetails();

using (app.UseSalatino("Thumbnailer"))
{
    app.UseMiddleware<LogApiKeyNameMiddleware>();

    app.UseNoCacheHttpHeaders();

    app.MapRedirectsToOpenApiDocs(redirectRootRoute: !websiteConfiguration.Enabled);
    app.MapAppInfoEndpoints();

    app.MapControllers();

    if (websiteConfiguration.Enabled)
    {
        // Map Razor Pages only when website has been enabled, in order not to overlap with the
        // "redirect to Swagger" rule.
        app.MapRazorPages();

        // Fair use token generation is currently used only for the website. Therefore, it is better
        // to enable it only when needed.
        app.MapFairUseTokenEndpoints();
    }

    await app.RunAsync();
}

/// <summary>
///   Program entrypoint.
/// </summary>
[SuppressMessage("Major Code Smell", "S1118:Utility classes should not have public constructors", Justification = "Stub class required in order to allow testing")]
public partial class Program;
