﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System.Data;
using Microsoft.Data.SqlClient;
using MySqlConnector;
using Npgsql;
using PommaLabs.Thumbnailer.Models.Configurations;
using PommaLabs.Thumbnailer.Models.Enumerations;
using SqlKata.Compilers;
using SqlKata.Execution;

namespace PommaLabs.Thumbnailer.Server;

/// <summary>
///   Helper methods for SqlKata.
/// </summary>
public static class QueryFactoryHelper
{
    /// <summary>
    ///   Returns a query factory built for given database configuration.
    /// </summary>
    /// <param name="dbc">Database configuration.</param>
    /// <returns>A query factory built for given database configuration.</returns>
    /// <exception cref="NotSupportedException">Given database provider is not supported.</exception>
    public static QueryFactory GetQueryFactory(DatabaseConfiguration dbc)
    {
        IDbConnection connection;
        Compiler compiler;
        switch (dbc.Provider)
        {
            case DatabaseProvider.MySql:
                connection = new MySqlConnection(dbc.ConnectionString);
                compiler = new MySqlCompiler();
                return new QueryFactory(connection, compiler);

            case DatabaseProvider.PostgreSql:
                connection = new NpgsqlConnection(dbc.ConnectionString);
                compiler = new PostgresCompiler();
                return new QueryFactory(connection, compiler);

            case DatabaseProvider.SqlServer:
                connection = new SqlConnection(dbc.ConnectionString);
                compiler = new SqlServerCompiler();
                return new QueryFactory(connection, compiler);

            default:
                var ex = new NotSupportedException("Given database provider is not supported");
                ex.Data.Add(nameof(DatabaseProvider), dbc.Provider);
                throw ex;
        }
    }
}
