﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using Microsoft.Extensions.Diagnostics.HealthChecks;
using PommaLabs.Thumbnailer.Core;
using PommaLabs.Thumbnailer.Repositories.TempFile;

namespace PommaLabs.Thumbnailer.Server.HealthChecks;

/// <summary>
///   Checks the status of the temporary file repository.
/// </summary>
public sealed class TempFileRepositoryHealthCheck : IHealthCheck
{
    internal const int FreeSpaceThreshold = 1 * 1024 * 1024; // 1 MB

    private readonly ITempFileRepository _tempFileRepository;

    /// <summary>
    ///   Constructor.
    /// </summary>
    /// <param name="tempFileRepository">Temporary file repository.</param>
    public TempFileRepositoryHealthCheck(ITempFileRepository tempFileRepository)
    {
        _tempFileRepository = tempFileRepository;
    }

    /// <inheritdoc/>
    public async Task<HealthCheckResult> CheckHealthAsync(
        HealthCheckContext context, CancellationToken cancellationToken = default)
    {
        if (HealthCheckCacheHelper<ITempFileRepository>.TryGetCachedResult(_tempFileRepository, out var result))
        {
            return result;
        }
        try
        {
            var freeSpace = await _tempFileRepository.GetAvailableFreeSpaceInBytes(cancellationToken);
            if (freeSpace < FreeSpaceThreshold)
            {
                result = HealthCheckResult.Unhealthy($"Available free space ({freeSpace} bytes) is below the threshold ({FreeSpaceThreshold} bytes)");
            }
            else
            {
                result = HealthCheckResult.Healthy($"Temporary file repository has more than {FreeSpaceThreshold} bytes of free space");
            }
        }
        catch (Exception ex)
        {
            result = HealthCheckResult.Unhealthy("Temporary file repository threw an exception while getting available free space", exception: ex);
        }
        HealthCheckCacheHelper<ITempFileRepository>.AddResultToCache(_tempFileRepository, result);
        return result;
    }
}
