﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using Microsoft.Extensions.Diagnostics.HealthChecks;
using PommaLabs.Thumbnailer.Core;
using PommaLabs.Thumbnailer.Repositories.ApiKey;

namespace PommaLabs.Thumbnailer.Server.HealthChecks;

/// <summary>
///   Checks the status of the API key repository.
/// </summary>
public sealed class ApiKeyRepositoryHealthCheck : IHealthCheck
{
    private readonly IApiKeyRepository _apiKeyRepository;

    /// <summary>
    ///   Constructor.
    /// </summary>
    /// <param name="apiKeyRepository">API key repository.</param>
    public ApiKeyRepositoryHealthCheck(IApiKeyRepository apiKeyRepository)
    {
        _apiKeyRepository = apiKeyRepository;
    }

    /// <inheritdoc/>
    public async Task<HealthCheckResult> CheckHealthAsync(
        HealthCheckContext context, CancellationToken cancellationToken = default)
    {
        if (HealthCheckCacheHelper<IApiKeyRepository>.TryGetCachedResult(_apiKeyRepository, out var result))
        {
            return result;
        }
        try
        {
            await _apiKeyRepository.ValidateApiKeyAsync(nameof(ApiKeyRepositoryHealthCheck), cancellationToken);
            result = HealthCheckResult.Healthy("API key repository successfully validated test API key");
        }
        catch (Exception ex)
        {
            result = HealthCheckResult.Unhealthy("API key repository threw an exception while validating test API key", exception: ex);
        }
        HealthCheckCacheHelper<IApiKeyRepository>.AddResultToCache(_apiKeyRepository, result);
        return result;
    }
}
