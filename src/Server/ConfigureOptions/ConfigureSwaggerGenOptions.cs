﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System.Diagnostics.CodeAnalysis;
using Microsoft.Extensions.Options;
using Microsoft.OpenApi.Models;
using PommaLabs.Salatino.Models.DataTransferObjects;
using PommaLabs.Thumbnailer.Core;
using PommaLabs.Thumbnailer.Models.Configurations;
using PommaLabs.Thumbnailer.Models.DTO.Public;
using PommaLabs.Thumbnailer.Server.OperationFilters;
using Swashbuckle.AspNetCore.Filters;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace PommaLabs.Thumbnailer.Server.ConfigureOptions;

internal sealed class ConfigureSwaggerGenOptions : IConfigureOptions<SwaggerGenOptions>
{
    private readonly AppVersion _appVersion;
    private readonly IOptions<SecurityConfiguration> _securityConfiguration;

    public ConfigureSwaggerGenOptions(AppVersion appVersion, IOptions<SecurityConfiguration> securityConfiguration)
    {
        _appVersion = appVersion;
        _securityConfiguration = securityConfiguration;
    }

    [SuppressMessage("Minor Code Smell", "S1075:URIs should not be hardcoded", Justification = "External URL of MIT license")]
    public void Configure(SwaggerGenOptions options)
    {
        options.SwaggerDoc("schema", new OpenApiInfo
        {
            Title = "Thumbnailer",
            Version = _appVersion.Version,
            Description = """
File thumbnail generator and simple media file optimizer.

Some useful links:
- [Source code](https://gitlab.com/pommalabs/thumbnailer/thumbnailer)
- [Demo website](https://thumbnailer.pommalabs.xyz/)
- [.NET client](https://gitlab.com/pommalabs/thumbnailer/thumbnailer-dotnet-client)
""",
            Contact = new OpenApiContact
            {
                Name = "PommaLabs Team",
                Email = "hello@pommalabs.xyz"
            },
            License = new OpenApiLicense
            {
                Name = "MIT",
                Url = new Uri("https://opensource.org/licenses/MIT")
            }
        });

        options.IncludeXmlComments(typeof(Program).Assembly);
        options.IncludeXmlComments(typeof(JobDetails).Assembly);

        options.OperationFilter<BinarySchemaOperationFilter>();
        options.OperationFilter<DeprecatedOperationFilter>();

        options.OperationFilter<SecurityRequirementsOperationFilter>(false, "api_key_header");
        options.OperationFilter<SecurityRequirementsOperationFilter>(false, "api_key_query");

        if (_securityConfiguration.Value.AcceptApiKeysViaHeaderParameter)
        {
            options.AddSecurityDefinition("api_key_header", new OpenApiSecurityScheme
            {
                Description = $"API key sent as \"{Constants.ApiKeyHeaderName}\" header.",
                Type = SecuritySchemeType.ApiKey,
                In = ParameterLocation.Header,
                Name = Constants.ApiKeyHeaderName
            });
        }
        if (_securityConfiguration.Value.AcceptApiKeysViaQueryStringParameter)
        {
            options.AddSecurityDefinition("api_key_query", new OpenApiSecurityScheme
            {
                Description = $"API key sent as \"{Constants.ApiKeyQueryParameterName}\" query string.",
                Type = SecuritySchemeType.ApiKey,
                In = ParameterLocation.Query,
                Name = Constants.ApiKeyQueryParameterName
            });
        }

        options.UseVersionedOperationIds();
    }
}
