﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Microsoft.AspNetCore.Razor.TagHelpers;

namespace PommaLabs.Thumbnailer.Server.TagHelpers;

/// <summary>
///   Adds "active" class to navbar link related to current page.
/// </summary>
[HtmlTargetElement(Attributes = "is-active-route")]
public class ActiveRouteTagHelper : TagHelper
{
    private readonly IHttpContextAccessor _httpContextAccessor;

    private IDictionary<string, string>? _routeValues;

    /// <summary>
    ///   Constructor.
    /// </summary>
    /// <param name="httpContextAccessor">HTTP context accessor.</param>
    public ActiveRouteTagHelper(IHttpContextAccessor httpContextAccessor)
    {
        _httpContextAccessor = httpContextAccessor;
    }

    /// <summary>
    ///   The name of the action method.
    /// </summary>
    /// <remarks>Must be <c>null</c> if <see cref="Route"/> is non- <c>null</c>.</remarks>
    [HtmlAttributeName("asp-action")]
    public string? Action { get; set; }

    /// <summary>
    ///   The name of the controller.
    /// </summary>
    /// <remarks>Must be <c>null</c> if <see cref="Route"/> is non- <c>null</c>.</remarks>
    [HtmlAttributeName("asp-controller")]
    public string? Controller { get; set; }

    /// <summary>
    ///   Page name.
    /// </summary>
    [HtmlAttributeName("asp-page")]
    public string? Page { get; set; }

    /// <summary>
    ///   Additional parameters for the route.
    /// </summary>
    [HtmlAttributeName("asp-all-route-data", DictionaryAttributePrefix = "asp-route-")]
    public IDictionary<string, string> RouteValues
    {
        get
        {
            return _routeValues ??= new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase);
        }
        set
        {
            _routeValues = value;
        }
    }

    /// <summary>
    ///   Gets or sets the <see cref="ViewContext"/> for the current request.
    /// </summary>
    [HtmlAttributeNotBound]
    [ViewContext]
    public ViewContext? ViewContext { get; set; }

    /// <inheritdoc/>
    public override void Process(TagHelperContext context, TagHelperOutput output)
    {
        base.Process(context, output);

        if (ShouldBeActive())
        {
            MakeActive(output);
        }

        output.Attributes.RemoveAll("is-active-route");
    }

    private static void MakeActive(TagHelperOutput output)
    {
        var classAttr = output.Attributes.FirstOrDefault(a => a.Name == "class");
        if (classAttr == null)
        {
            classAttr = new TagHelperAttribute("class", "active");
            output.Attributes.Add(classAttr);
        }
        else if (classAttr.Value == null || classAttr.Value?.ToString()?.IndexOf("active", StringComparison.OrdinalIgnoreCase) < 0)
        {
            output.Attributes.SetAttribute("class", classAttr.Value == null
                ? "active"
                : classAttr.Value + " active");
        }
    }

    private bool ShouldBeActive()
    {
        var currentController = string.Empty;
        var currentAction = string.Empty;

        if (ViewContext?.RouteData != null)
        {
            if (ViewContext.RouteData.Values["Controller"] != null)
            {
                currentController = ViewContext.RouteData.Values["Controller"]?.ToString() ?? string.Empty;
            }

            if (ViewContext.RouteData.Values["Action"] != null)
            {
                currentAction = ViewContext.RouteData.Values["Action"]?.ToString() ?? string.Empty;
            }
        }

        if (Controller != null)
        {
            if (!string.IsNullOrWhiteSpace(Controller) && !string.Equals(Controller, currentController, StringComparison.OrdinalIgnoreCase))
            {
                return false;
            }

            if (!string.IsNullOrWhiteSpace(Action) && !string.Equals(Action, currentAction, StringComparison.OrdinalIgnoreCase))
            {
                return false;
            }
        }

        if (Page != null && !string.IsNullOrWhiteSpace(Page) && !string.Equals(Page, _httpContextAccessor.HttpContext!.Request.Path.Value, StringComparison.OrdinalIgnoreCase))
        {
            return false;
        }

        if (ViewContext?.RouteData != null)
        {
            foreach (var routeValue in RouteValues)
            {
                if (!ViewContext.RouteData.Values.ContainsKey(routeValue.Key) ||
                    ViewContext.RouteData.Values[routeValue.Key]?.ToString() != routeValue.Value)
                {
                    return false;
                }
            }
        }

        return true;
    }
}
