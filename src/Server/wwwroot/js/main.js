﻿"use strict";

// Application data:
window.appData = {
    apiKey: "",
    operationType: ""
};

function disableFairUseTokenToApiKeySubmitButton() {
    $("#fair-use-token-to-api-key-submit").attr("disabled", true);
}

function enableFairUseTokenToApiKeySubmitButton() {
    $("#fair-use-token-to-api-key-submit").removeAttr("disabled");
}

async function getFairUseToken() {
    // Gets a fair use token, which will be converted into an API key.
    try {
        const fairUseToken = await $.ajax({
            type: "GET",
            url: "/api/v1/fair-use-token",
        });
        
        console.debug("Fair use token has been generated:", fairUseToken.token);
        $("#fair-use-token").val(fairUseToken.token);
    }
    catch (error) {
        console.error("Fair use token generation error:", error);
        alert("Fair use token could not be generated, probably due to an high number of requests. Please retry later.");
    }
}

async function onFairUseTokenToApiKeyButtonClick() {
    await getFairUseToken();
    
    // Converts the fair use token into a temporary API key.
    try {
        const apiKey = await $.ajax({
            type: "POST",
            url: "/fair-use-token-to-api-key",
            data: $("#fair-use-token-to-api-key-form").serialize(),
        });

        if (apiKey.isValid) {
            console.debug("Temporary API key is valid:", apiKey.value);
            window.appData.apiKey = apiKey.value;

            window.appData.outputZip = new JSZip();

            console.debug("Processing queue using temporary API key");
            window.appData.thumbnailerDropzone.processQueue();
        } else {
            console.debug("Temporary API key is invalid");
            window.location.reload();
        }
    }
    catch (error) {
        console.error("Fair use token to API key conversion error:", error);
        alert("A temporary API key could not be obtained, probably due to an high number of requests. Please retry later.");
    }
    
    return false;
}

// Dropzone configuration:
Dropzone.options.thumbnailerDropzone = {
    // Prevents Dropzone from uploading dropped files immediately.
    autoProcessQueue: false,

    // Maximum number of files which can be uploaded.
    maxFiles: 20,
    parallelUploads: 20,

    // Maximum file size for each upload.
    maxFilesize: 50,

    url: function () {
        return $("#thumbnailer-dropzone").prop("action");
    },

    init: function () {
        window.appData.thumbnailerDropzone = this;

        this.on("addedfile", function () {
            // Show submit button when at least one file has been selected.
            if (this.files.length > 0) {
                enableFairUseTokenToApiKeySubmitButton();
            }
        });

        this.on("sending", function (__file, xhr, __formData) {
            disableFairUseTokenToApiKeySubmitButton();
            xhr.setRequestHeader("X-Api-Key", window.appData.apiKey);
        });

        this.on("success", function (file, response) {
            const ext = (window.appData.operationType === "ThumbnailGeneration") ? ".png" : "";
            window.appData.outputZip.file(file.name + ext, response.contents, { base64: true });
        });

        this.on("queuecomplete", function () {
            window.appData.outputZip.generateAsync({ type: "blob" })
                .then(function (content) {
                    saveAs(content, ~~(Date.now() / 1000) + ".zip");
                });

            this.removeAllFiles(true);
            window.location.reload();
        });
    }
};
