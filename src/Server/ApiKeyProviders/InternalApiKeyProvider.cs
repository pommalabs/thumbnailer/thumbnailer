﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System.Security.Claims;
using AspNetCore.Authentication.ApiKey;
using PommaLabs.Thumbnailer.Models.DTO.Internal;
using PommaLabs.Thumbnailer.Repositories.ApiKey;

namespace PommaLabs.Thumbnailer.Server.ApiKeyProviders;

internal sealed class InternalApiKeyProvider : IApiKeyProvider
{
    private readonly IApiKeyRepository _apiKeyRepository;

    public InternalApiKeyProvider(IApiKeyRepository apiKeyRepository)
    {
        _apiKeyRepository = apiKeyRepository;
    }

    async Task<IApiKey?> IApiKeyProvider.ProvideAsync(string key)
    {
        var dto = await _apiKeyRepository.ValidateApiKeyAsync(key, default);
        return dto == null ? null : new ApiKey(dto);
    }

    private sealed class ApiKey : IApiKey
    {
        public ApiKey(ApiKeyCredentials dto)
        {
            Key = dto.Value;
            OwnerName = dto.Name;
            Claims = new[] { new Claim(ClaimTypes.Name, dto.Name) };
        }

        public IReadOnlyCollection<Claim> Claims { get; }

        public string Key { get; }

        public string OwnerName { get; }
    }
}
