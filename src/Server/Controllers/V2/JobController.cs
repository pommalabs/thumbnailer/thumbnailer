﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PommaLabs.Thumbnailer.Models.DTO.Internal;
using PommaLabs.Thumbnailer.Models.DTO.Public;
using PommaLabs.Thumbnailer.Models.Enumerations;
using PommaLabs.Thumbnailer.Services.Job;

namespace PommaLabs.Thumbnailer.Server.Controllers.V2;

/// <summary>
///   Controller which exposes information about queued jobs.
/// </summary>
[ApiController, Route("api/v2/jobs/{jobId}"), Authorize]
[ProducesResponseType(StatusCodes.Status404NotFound)]
public sealed class JobController : ThumbnailerControllerBase
{
    /// <summary>
    ///   Constructor.
    /// </summary>
    /// <param name="jobService">Job service.</param>
    public JobController(IJobService jobService) : base(jobService)
    {
    }

    /// <summary>
    ///   Looks for the result of the job with specified ID.
    /// </summary>
    /// <param name="jobId">Job ID.</param>
    /// <param name="responseType">Response type.</param>
    /// <param name="openInBrowser">
    ///   When true, browser will receive a "Content-Disposition: inline" header, which should make
    ///   it open the result in-browser. When false, browser will receive a "Content-Disposition:
    ///   attachment" header, which should force it to download the result.
    /// </param>
    /// <param name="cancellationToken">Cancellation token.</param>
    /// <returns>The result of the job, if it has been found.</returns>
    [HttpGet("download")]
    [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(FileDetails))]
    public async Task<IActionResult> DownloadJobResult(
        [FromRoute] string jobId,
        [FromQuery] ResponseType responseType = ResponseType.Binary,
        [FromQuery] bool openInBrowser = false,
        CancellationToken cancellationToken = default)
    {
        var result = await JobService.GetJobResultAsync<object>(jobId, cancellationToken);
        return result switch
        {
            TempFileMetadata tempFile => await RespondFileAsync(responseType, openInBrowser, nameof(result), tempFile),
            ImagePlaceholderResult imagePlaceholder => Ok(imagePlaceholder),
            _ => NotFound(),
        };
    }

    /// <summary>
    ///   Looks for a job with specified ID.
    /// </summary>
    /// <param name="jobId">Job ID.</param>
    /// <param name="cancellationToken">Cancellation token.</param>
    /// <returns>The job, if it has been found.</returns>
    [HttpGet("")]
    [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(JobDetails))]
    public async Task<IActionResult> QueryJob(string jobId, CancellationToken cancellationToken)
    {
        var job = await JobService.QueryJobAsync(jobId, cancellationToken);
        return job == null ? NotFound() : Ok(job);
    }
}
