﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PommaLabs.MimeTypes;
using PommaLabs.Thumbnailer.Core;
using PommaLabs.Thumbnailer.Models.Commands;
using PommaLabs.Thumbnailer.Models.DTO.Internal;
using PommaLabs.Thumbnailer.Models.DTO.Public;
using PommaLabs.Thumbnailer.Models.Enumerations;
using PommaLabs.Thumbnailer.Repositories.TempFile;
using PommaLabs.Thumbnailer.Services.Download;
using PommaLabs.Thumbnailer.Services.Job;

namespace PommaLabs.Thumbnailer.Server.Controllers.V2;

/// <summary>
///   Controller which handles thumbnail generation.
/// </summary>
[ApiController, Route("api/v2/thumbnail"), Authorize]
[ProducesResponseType(StatusCodes.Status200OK, Type = typeof(JobDetails))]
[ProducesResponseType(StatusCodes.Status400BadRequest)]
[ProducesResponseType(StatusCodes.Status401Unauthorized)]
[ProducesResponseType(StatusCodes.Status403Forbidden)]
public sealed class ThumbnailGenerationController : ControllerBase
{
    private readonly IDownloadService _downloadService;
    private readonly IJobService _jobService;

    /// <summary>
    ///   Constructor.
    /// </summary>
    /// <param name="jobService">Job service.</param>
    /// <param name="downloadService">Download service.</param>
    public ThumbnailGenerationController(
        IJobService jobService,
        IDownloadService downloadService)
    {
        _jobService = jobService;
        _downloadService = downloadService;
    }

    /// <summary>
    ///   Generates a thumbnail of given source file.
    /// </summary>
    /// <param name="fileUri">Source file URI.</param>
    /// <param name="widthPx">
    ///   Max thumbnail width. Thumbnail will be generated preserving the source file aspect ratio
    ///   and, if <paramref name="fill"/> is true, a transparent background will be added so that
    ///   thumbnail width and height are exactly as requested.
    /// </param>
    /// <param name="heightPx">
    ///   Max thumbnail height. Thumbnail will be generated preserving the source file aspect ratio
    ///   and, if <paramref name="fill"/> is true, a transparent background will be added so that
    ///   thumbnail width and height are exactly as requested.
    /// </param>
    /// <param name="shavePx">Shaves <paramref name="shavePx"/> from source file edges.</param>
    /// <param name="fill">
    ///   If true, a transparent background will be added so that thumbnail width and height are
    ///   exactly as requested, preserving source file aspect ratio. When <paramref
    ///   name="smartCrop"/> is enabled, this parameter will be ignored. Defaults to true.
    /// </param>
    /// <param name="smartCrop">
    ///   Finds a good crop which satisfies specified <paramref name="widthPx"/> and <paramref
    ///   name="heightPx"/> parameters. When smart crop is enabled, <paramref name="fill"/> is
    ///   ignored. Defaults to false.
    /// </param>
    /// <param name="fallback">
    ///   If true, a fallback image is generated when thumbnail generation fails. Fallback image is
    ///   generated starting from an SVG template, whose customization is documented in project
    ///   README. Defaults to false.
    /// </param>
    /// <param name="imagePlaceholderAlgorithm">
    ///   Desired placeholder generation algorithm. Defaults to None.
    /// </param>
    /// <param name="cancellationToken">Cancellation token.</param>
    /// <returns>A thumbnail of given source file.</returns>
    [HttpGet("")]
    public async Task<IActionResult> GenerateThumbnailFromUri(
        [FromQuery] Uri fileUri,
        [FromQuery] ushort widthPx = Validator.ThumbnailSidePx,
        [FromQuery] ushort heightPx = Validator.ThumbnailSidePx,
        [FromQuery] ushort shavePx = Validator.ThumbnailShavePx,
        [FromQuery] bool fill = true,
        [FromQuery] bool smartCrop = false,
        [FromQuery] bool fallback = false,
        [FromQuery] ImagePlaceholderAlgorithm imagePlaceholderAlgorithm = ImagePlaceholderAlgorithm.None,
        CancellationToken cancellationToken = default)
    {
        var localFile = await _downloadService.DownloadFileAsync(fileUri, cancellationToken);
        return await EnqueueCommandAsync(
            localFile, widthPx, heightPx,
            shavePx, fill, smartCrop, fallback,
            imagePlaceholderAlgorithm, cancellationToken);
    }

    /// <summary>
    ///   Generates a thumbnail of given source file.
    /// </summary>
    /// <param name="tempFileRepository">Temporary file repository.</param>
    /// <param name="file">Source file.</param>
    /// <param name="widthPx">
    ///   Max thumbnail width. Thumbnail will be generated preserving the source file aspect ratio
    ///   and, if <paramref name="fill"/> is true, a transparent background will be added so that
    ///   thumbnail width and height are exactly as requested.
    /// </param>
    /// <param name="heightPx">
    ///   Max thumbnail height. Thumbnail will be generated preserving the source file aspect ratio
    ///   and, if <paramref name="fill"/> is true, a transparent background will be added so that
    ///   thumbnail width and height are exactly as requested.
    /// </param>
    /// <param name="shavePx">Shaves <paramref name="shavePx"/> from source file edges.</param>
    /// <param name="fill">
    ///   If true, a transparent background will be added so that thumbnail width and height are
    ///   exactly as requested, preserving source file aspect ratio. When <paramref
    ///   name="smartCrop"/> is enabled, this parameter will be ignored. Defaults to true.
    /// </param>
    /// <param name="smartCrop">
    ///   Finds a good crop which satisfies specified <paramref name="widthPx"/> and <paramref
    ///   name="heightPx"/> parameters. When smart crop is enabled, <paramref name="fill"/> is
    ///   ignored. Defaults to false.
    /// </param>
    /// <param name="fallback">
    ///   If true, a fallback image is generated when thumbnail generation fails. Fallback image is
    ///   generated starting from an SVG template, whose customization is documented in project
    ///   README. Defaults to false.
    /// </param>
    /// <param name="imagePlaceholderAlgorithm">
    ///   Desired placeholder generation algorithm. Defaults to None.
    /// </param>
    /// <param name="cancellationToken">Cancellation token.</param>
    /// <returns>A thumbnail of given source file.</returns>
    [HttpPost(""), Consumes(Constants.MultipartFormData)]
    [DisableRequestSizeLimit /* Upload limit is globally set in Startup code. */]
    public async Task<IActionResult> GenerateThumbnailFromFile(
        [FromServices] ITempFileRepository tempFileRepository, IFormFile file,
        [FromQuery] ushort widthPx = Validator.ThumbnailSidePx,
        [FromQuery] ushort heightPx = Validator.ThumbnailSidePx,
        [FromQuery] ushort shavePx = Validator.ThumbnailShavePx,
        [FromQuery] bool fill = true,
        [FromQuery] bool smartCrop = false,
        [FromQuery] bool fallback = false,
        [FromQuery] ImagePlaceholderAlgorithm imagePlaceholderAlgorithm = ImagePlaceholderAlgorithm.None,
        CancellationToken cancellationToken = default)
    {
        var localFile = await tempFileRepository.HandleFileUploadAsync(file, cancellationToken);
        return await EnqueueCommandAsync(
            localFile, widthPx, heightPx,
            shavePx, fill, smartCrop, fallback,
            imagePlaceholderAlgorithm, cancellationToken);
    }

    /// <summary>
    ///   Generates a thumbnail of given source file.
    /// </summary>
    /// <param name="tempFileRepository">Temporary file repository.</param>
    /// <param name="file">Source file.</param>
    /// <param name="widthPx">
    ///   Max thumbnail width. Thumbnail will be generated preserving the source file aspect ratio
    ///   and, if <paramref name="fill"/> is true, a transparent background will be added so that
    ///   thumbnail width and height are exactly as requested.
    /// </param>
    /// <param name="heightPx">
    ///   Max thumbnail height. Thumbnail will be generated preserving the source file aspect ratio
    ///   and, if <paramref name="fill"/> is true, a transparent background will be added so that
    ///   thumbnail width and height are exactly as requested.
    /// </param>
    /// <param name="shavePx">Shaves <paramref name="shavePx"/> from source file edges.</param>
    /// <param name="fill">
    ///   If true, a transparent background will be added so that thumbnail width and height are
    ///   exactly as requested, preserving source file aspect ratio. When <paramref
    ///   name="smartCrop"/> is enabled, this parameter will be ignored. Defaults to true.
    /// </param>
    /// <param name="smartCrop">
    ///   Finds a good crop which satisfies specified <paramref name="widthPx"/> and <paramref
    ///   name="heightPx"/> parameters. When smart crop is enabled, <paramref name="fill"/> is
    ///   ignored. Defaults to false.
    /// </param>
    /// <param name="fallback">
    ///   If true, a fallback image is generated when thumbnail generation fails. Fallback image is
    ///   generated starting from an SVG template, whose customization is documented in project
    ///   README. Defaults to false.
    /// </param>
    /// <param name="imagePlaceholderAlgorithm">
    ///   Desired placeholder generation algorithm. Defaults to None.
    /// </param>
    /// <param name="cancellationToken">Cancellation token.</param>
    /// <returns>A thumbnail of given source file.</returns>
    /// <remarks>Source file contents need to be encoded as Base64 string.</remarks>
    [HttpPost("base64"), Consumes(MimeTypeMap.APPLICATION.JSON, MimeTypeMap.APPLICATION.XML)]
    [DisableRequestSizeLimit /* Upload limit is globally set in Startup code. */]
    public async Task<IActionResult> GenerateThumbnailFromJson(
        [FromServices] ITempFileRepository tempFileRepository,
        [FromBody] FileDetails file,
        [FromQuery] ushort widthPx = Validator.ThumbnailSidePx,
        [FromQuery] ushort heightPx = Validator.ThumbnailSidePx,
        [FromQuery] ushort shavePx = Validator.ThumbnailShavePx,
        [FromQuery] bool fill = true,
        [FromQuery] bool smartCrop = false,
        [FromQuery] bool fallback = false,
        [FromQuery] ImagePlaceholderAlgorithm imagePlaceholderAlgorithm = ImagePlaceholderAlgorithm.None,
        CancellationToken cancellationToken = default)
    {
        var localFile = await tempFileRepository.HandleFileUploadAsync(file, cancellationToken);
        return await EnqueueCommandAsync(
            localFile, widthPx, heightPx,
            shavePx, fill, smartCrop, fallback,
            imagePlaceholderAlgorithm, cancellationToken);
    }

    private async Task<IActionResult> EnqueueCommandAsync(
        TempFileMetadata localFile, ushort widthPx, ushort heightPx,
        ushort shavePx, bool fill, bool smartCrop, bool fallback,
        ImagePlaceholderAlgorithm imagePlaceholderAlgorithm, CancellationToken cancellationToken)
    {
        var tgCommand = new ThumbnailGenerationCommand(localFile, imagePlaceholderAlgorithm)
        {
            WidthPx = widthPx,
            HeightPx = heightPx,
            ShavePx = shavePx,
            Fill = fill,
            SmartCrop = smartCrop,
            Fallback = fallback,
        };
        return Ok(await _jobService.EnqueueCommandAsync<TempFileMetadata>(tgCommand, cancellationToken));
    }
}
