﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System.Diagnostics.CodeAnalysis;
using Hellang.Middleware.ProblemDetails;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Net.Http.Headers;
using PommaLabs.Thumbnailer.Models.Commands;
using PommaLabs.Thumbnailer.Models.DTO.Internal;
using PommaLabs.Thumbnailer.Models.DTO.Public;
using PommaLabs.Thumbnailer.Models.Enumerations;
using PommaLabs.Thumbnailer.Services.Job;

namespace PommaLabs.Thumbnailer.Server.Controllers;

/// <summary>
///   Base controller.
/// </summary>
public abstract class ThumbnailerControllerBase : ControllerBase
{
    /// <summary>
    ///   Constructor.
    /// </summary>
    /// <param name="jobService">Job service.</param>
    protected ThumbnailerControllerBase(IJobService jobService)
    {
        JobService = jobService;
    }

    /// <summary>
    ///   Job service.
    /// </summary>
    protected IJobService JobService { get; }

    /// <summary>
    ///   Enqueue specified command and waits the job has been processed. This method also handles
    ///   error cases, such as failed jobs.
    /// </summary>
    /// <param name="command">Command.</param>
    /// <param name="cancellationToken">Cancellation token.</param>
    /// <typeparam name="T">Job result type.</typeparam>
    /// <returns>Job result.</returns>
    /// <exception cref="ProblemDetailsException">Job has failed.</exception>
    protected async Task<T> EnqueueAndWaitCommandAsync<T>(ThumbnailerCommandBase command, CancellationToken cancellationToken)
    {
        var job = await JobService.EnqueueCommandAsync<T>(command, cancellationToken);

        var jobProcessed = false;
        while (!jobProcessed)
        {
            job = (await JobService.QueryJobAsync(job.JobId, cancellationToken))!;

            switch (job.Status)
            {
                case JobStatus.Queued:
                case JobStatus.Processing:
                    break;

                case JobStatus.Processed:
                    jobProcessed = true;
                    break;

                case JobStatus.Failed:
                    throw new ProblemDetailsException(StatusCodes.Status500InternalServerError, job.FailureReason!);

                default:
                    throw new ProblemDetailsException(StatusCodes.Status500InternalServerError, "Async job has an invalid status");
            }

            await Task.Delay(250, cancellationToken);
        }

        return (await JobService.GetJobResultAsync<T>(job.JobId, cancellationToken))!;
    }

    /// <summary>
    ///   Produces the response according to the desired type.
    /// </summary>
    /// <param name="responseType">Response type.</param>
    /// <param name="openInBrowser">
    ///   When true, browser will receive a "Content-Disposition: inline" header, which should make
    ///   it open the result in-browser. When false, browser will receive a "Content-Disposition:
    ///   attachment" header, which should force it to download the result.
    /// </param>
    /// <param name="fileNameWithoutExtension">File name without extension.</param>
    /// <param name="tempFile">Temporary file.</param>
    /// <returns>The response, computed according to the desired type.</returns>
    [SuppressMessage("Security", "SCS0018", Justification = "Path does not depend on user input")]
    protected async Task<IActionResult> RespondFileAsync(
        ResponseType responseType, bool openInBrowser,
        string fileNameWithoutExtension, TempFileMetadata tempFile)
    {
        var fileName = fileNameWithoutExtension + tempFile.Extension;

        switch (responseType)
        {
            case ResponseType.Binary:
                AddContentDispositionHeader(openInBrowser, fileName);
                TryAddImagePlaceholderHeader(tempFile.ImagePlaceholder);

                return PhysicalFile(
                    tempFile.Path,
                    tempFile.ContentType,
                    enableRangeProcessing: true);

            case ResponseType.Base64:
                return Ok(new FileDetails
                {
                    Contents = await System.IO.File.ReadAllBytesAsync(tempFile.Path),
                    ContentType = tempFile.ContentType,
                    FileName = fileName,
                    ImagePlaceholder = tempFile.ImagePlaceholder,
                });

            default:
                return BadRequest("Invalid response type");
        }
    }

    /// <summary>
    ///   Set up the "Content-Disposition" header with proper encoding of the file name.
    /// </summary>
    /// <param name="openInBrowser">
    ///   When true, browser will receive a "Content-Disposition: inline" header, which should make
    ///   it open the result in-browser. When false, browser will receive a "Content-Disposition:
    ///   attachment" header, which should force it to download the result.
    /// </param>
    /// <param name="fileName">Output file name.</param>
    private void AddContentDispositionHeader(bool openInBrowser, string fileName)
    {
        var dispositionType = openInBrowser ? "inline" : "attachment";
        var contentDisposition = new ContentDispositionHeaderValue(dispositionType);
        contentDisposition.SetHttpFileName(fileName);
        Response.Headers[HeaderNames.ContentDisposition] = contentDisposition.ToString();
    }

    /// <summary>
    ///   Add an image placeholder to the response header.
    /// </summary>
    /// <param name="imagePlaceholder">Image placeholder string to be added.</param>
    private void TryAddImagePlaceholderHeader(string? imagePlaceholder)
    {
        if (imagePlaceholder != null)
        {
            HttpContext.Response.Headers["X-Image-Placeholder"] = imagePlaceholder;
        }
    }
}
