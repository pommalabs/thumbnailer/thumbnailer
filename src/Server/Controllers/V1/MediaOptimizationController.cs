﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PommaLabs.MimeTypes;
using PommaLabs.Thumbnailer.Core;
using PommaLabs.Thumbnailer.Models.Commands;
using PommaLabs.Thumbnailer.Models.DTO.Internal;
using PommaLabs.Thumbnailer.Models.DTO.Public;
using PommaLabs.Thumbnailer.Models.Enumerations;
using PommaLabs.Thumbnailer.Repositories.TempFile;
using PommaLabs.Thumbnailer.Services.Download;
using PommaLabs.Thumbnailer.Services.Job;

namespace PommaLabs.Thumbnailer.Server.Controllers.V1;

/// <summary>
///   Controller which handles media file optimization.
/// </summary>
[ApiController, Route("api/v1/optimize"), Authorize]
[ProducesResponseType(StatusCodes.Status200OK, Type = typeof(FileDetails))]
[ProducesResponseType(StatusCodes.Status400BadRequest)]
[ProducesResponseType(StatusCodes.Status401Unauthorized)]
[ProducesResponseType(StatusCodes.Status403Forbidden)]
[ProducesResponseType(StatusCodes.Status422UnprocessableEntity)]
[ProducesResponseType(StatusCodes.Status424FailedDependency)]
public sealed class MediaOptimizationController : ThumbnailerControllerBase
{
    /// <summary>
    ///   Constructor.
    /// </summary>
    /// <param name="jobService">Job service.</param>
    public MediaOptimizationController(IJobService jobService) : base(jobService)
    {
    }

    /// <summary>
    ///   Optimizes given source media file.
    /// </summary>
    /// <param name="downloadService">Download service.</param>
    /// <param name="fileUri">Source file URI.</param>
    /// <param name="imagePlaceholderAlgorithm">
    ///   Desired placeholder generation algorithm. Defaults to None.
    /// </param>
    /// <param name="responseType">Response type.</param>
    /// <param name="openInBrowser">
    ///   When true, browser will receive a "Content-Disposition: inline" header, which should make
    ///   it open the result in-browser. When false, browser will receive a "Content-Disposition:
    ///   attachment" header, which should force it to download the result.
    /// </param>
    /// <param name="cancellationToken">Cancellation token.</param>
    /// <returns>An optimized version of given source media file.</returns>
    [HttpGet("")]
    public async Task<IActionResult> OptimizeMediaFromUri(
        [FromServices] IDownloadService downloadService,
        [FromQuery] Uri fileUri,
        [FromQuery] ImagePlaceholderAlgorithm imagePlaceholderAlgorithm = ImagePlaceholderAlgorithm.None,
        [FromQuery] ResponseType responseType = ResponseType.Binary,
        [FromQuery] bool openInBrowser = false,
        CancellationToken cancellationToken = default)
    {
        var localFile = await downloadService.DownloadFileAsync(fileUri, cancellationToken);
        var moCommand = new MediaOptimizationCommand(localFile, imagePlaceholderAlgorithm);
        var optimized = await EnqueueAndWaitCommandAsync<TempFileMetadata>(moCommand, cancellationToken);
        return await RespondFileAsync(responseType, openInBrowser, nameof(optimized), optimized);
    }

    /// <summary>
    ///   Optimizes given source media file.
    /// </summary>
    /// <param name="tempFileRepository">Temporary file repository.</param>
    /// <param name="file">Source file.</param>
    /// <param name="imagePlaceholderAlgorithm">
    ///   Desired placeholder generation algorithm. Defaults to None.
    /// </param>
    /// <param name="responseType">Response type.</param>
    /// <param name="openInBrowser">
    ///   When true, browser will receive a "Content-Disposition: inline" header, which should make
    ///   it open the result in-browser. When false, browser will receive a "Content-Disposition:
    ///   attachment" header, which should force it to download the result.
    /// </param>
    /// <param name="cancellationToken">Cancellation token.</param>
    /// <returns>An optimized version of given source media file.</returns>
    [HttpPost(""), Consumes(Constants.MultipartFormData)]
    [DisableRequestSizeLimit /* Upload limit is globally set in Startup code. */]
    public async Task<IActionResult> OptimizeMediaFromFile(
        [FromServices] ITempFileRepository tempFileRepository,
        IFormFile file,
        [FromQuery] ImagePlaceholderAlgorithm imagePlaceholderAlgorithm = ImagePlaceholderAlgorithm.None,
        [FromQuery] ResponseType responseType = ResponseType.Binary,
        [FromQuery] bool openInBrowser = false,
        CancellationToken cancellationToken = default)
    {
        var localFile = await tempFileRepository.HandleFileUploadAsync(file, cancellationToken);
        var moCommand = new MediaOptimizationCommand(localFile, imagePlaceholderAlgorithm);
        var optimized = await EnqueueAndWaitCommandAsync<TempFileMetadata>(moCommand, cancellationToken);
        return await RespondFileAsync(responseType, openInBrowser, nameof(optimized), optimized);
    }

    /// <summary>
    ///   Optimizes given source media file.
    /// </summary>
    /// <param name="tempFileRepository">Temporary file repository.</param>
    /// <param name="file">Source file.</param>
    /// <param name="imagePlaceholderAlgorithm">
    ///   Desired placeholder generation algorithm. Defaults to None.
    /// </param>
    /// <param name="responseType">Response type.</param>
    /// <param name="openInBrowser">
    ///   When true, browser will receive a "Content-Disposition: inline" header, which should make
    ///   it open the result in-browser. When false, browser will receive a "Content-Disposition:
    ///   attachment" header, which should force it to download the result.
    /// </param>
    /// <param name="cancellationToken">Cancellation token.</param>
    /// <returns>An optimized version of given source media file.</returns>
    /// <remarks>Source file contents need to be encoded as Base64 string.</remarks>
    [HttpPost("base64"), Consumes(MimeTypeMap.APPLICATION.JSON, MimeTypeMap.APPLICATION.XML)]
    [DisableRequestSizeLimit /* Upload limit is globally set in Startup code. */]
    public async Task<IActionResult> OptimizeMediaFromJson(
        [FromServices] ITempFileRepository tempFileRepository,
        [FromBody] FileDetails file,
        [FromQuery] ImagePlaceholderAlgorithm imagePlaceholderAlgorithm = ImagePlaceholderAlgorithm.None,
        [FromQuery] ResponseType responseType = ResponseType.Binary,
        [FromQuery] bool openInBrowser = false,
        CancellationToken cancellationToken = default)
    {
        var localFile = await tempFileRepository.HandleFileUploadAsync(file, cancellationToken);
        var moCommand = new MediaOptimizationCommand(localFile, imagePlaceholderAlgorithm);
        var optimized = await EnqueueAndWaitCommandAsync<TempFileMetadata>(moCommand, cancellationToken);
        return await RespondFileAsync(responseType, openInBrowser, nameof(optimized), optimized);
    }
}
