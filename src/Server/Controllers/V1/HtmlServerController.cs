﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PommaLabs.MimeTypes;
using PommaLabs.Thumbnailer.Core;
using PommaLabs.Thumbnailer.Server.ActionFilters;
using PommaLabs.Thumbnailer.Services.Html;

namespace PommaLabs.Thumbnailer.Server.Controllers.V1;

/// <summary>
///   Internal controller which serves HTML files to Firefox process.
///   This API must remain internal and it should be accessed from localhost only.
/// </summary>
[ApiController, Route("api/v1/htmlServer"), AllowAnonymous]
[ApiExplorerSettings(IgnoreApi = true), RestrictToLocalhost]
public sealed class HtmlServerController : ControllerBase
{
    private readonly IHtmlService _htmlService;

    /// <summary>
    ///   Constructor.
    /// </summary>
    /// <param name="htmlService">HTML service.</param>
    public HtmlServerController(IHtmlService htmlService)
    {
        _htmlService = htmlService;
    }

    /// <summary>
    ///   Internal HTML file server.
    /// </summary>
    /// <param name="encryptedHtmlFileName">Encrypted HTML file name.</param>
    [HttpGet("{encryptedHtmlFileName}")]
    public IActionResult ServeHtmlFile(string encryptedHtmlFileName)
    {
        if (!_htmlService.TryDecryptHtmlFileName(encryptedHtmlFileName, out var htmlFileName))
        {
            return NotFound("HTML file name cannot be decrypted");
        }

        if (!string.Equals(Path.GetExtension(htmlFileName), ".html", StringComparison.OrdinalIgnoreCase))
        {
            return NotFound("Internal HTML server can only serve HTML files");
        }

        var htmlFilePath = Path.GetFullPath(Path.Combine(Constants.TempDirectory, htmlFileName));
        if (Path.GetRelativePath(Constants.TempDirectory, htmlFilePath) != htmlFileName || !System.IO.File.Exists(htmlFilePath))
        {
            return NotFound("HTML file name contains invalid characters or file does not exist");
        }

        return PhysicalFile(htmlFilePath, $"{MimeTypeMap.TEXT.HTML}; charset=utf-8");
    }
}
