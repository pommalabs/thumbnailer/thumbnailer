﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PommaLabs.MimeTypes;
using PommaLabs.Thumbnailer.Core;
using PommaLabs.Thumbnailer.Models.Commands;
using PommaLabs.Thumbnailer.Models.DTO.Public;
using PommaLabs.Thumbnailer.Models.Enumerations;
using PommaLabs.Thumbnailer.Repositories.TempFile;
using PommaLabs.Thumbnailer.Services.Download;
using PommaLabs.Thumbnailer.Services.Job;

namespace PommaLabs.Thumbnailer.Server.Controllers.V1;

/// <summary>
///   Controller which handles image placeholder generation.
/// </summary>
[ApiController, Route("api/v1/imagePlaceholder"), Authorize]
[ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ImagePlaceholderResult))]
[ProducesResponseType(StatusCodes.Status400BadRequest)]
[ProducesResponseType(StatusCodes.Status401Unauthorized)]
[ProducesResponseType(StatusCodes.Status403Forbidden)]
public sealed class ImagePlaceholderGenerationController : ThumbnailerControllerBase
{
    /// <summary>
    ///   Constructor.
    /// </summary>
    /// <param name="jobService">Job service.</param>
    public ImagePlaceholderGenerationController(IJobService jobService) : base(jobService)
    {
    }

    /// <summary>
    ///   Creates an image placeholder of given source media file.
    /// </summary>
    /// <param name="downloadService">Download service.</param>
    /// <param name="fileUri">Source file URI.</param>
    /// <param name="imagePlaceholderAlgorithm">Desired placeholder generation algorithm.</param>
    /// <param name="cancellationToken">Cancellation token.</param>
    /// <returns>An hash representing the image placeholder of given source media file.</returns>
    [HttpGet("")]
    public async Task<IActionResult> GenerateImagePlaceholderFromUri(
        [FromServices] IDownloadService downloadService,
        [FromQuery] Uri fileUri,
        [FromQuery] ImagePlaceholderAlgorithm imagePlaceholderAlgorithm,
        CancellationToken cancellationToken = default)
    {
        var localFile = await downloadService.DownloadFileAsync(fileUri, cancellationToken);
        var pgCommand = new ImagePlaceholderGenerationCommand(localFile, imagePlaceholderAlgorithm);
        var placeholder = await EnqueueAndWaitCommandAsync<ImagePlaceholderResult>(pgCommand, cancellationToken);
        return Ok(placeholder);
    }

    /// <summary>
    ///   Creates an image placeholder of given source media file.
    /// </summary>
    /// <param name="tempFileRepository">Temporary file repository.</param>
    /// <param name="file">Source file.</param>
    /// <param name="imagePlaceholderAlgorithm">Desired placeholder generation algorithm.</param>
    /// <param name="cancellationToken">Cancellation token.</param>
    /// <returns>An hash representing the image placeholder of given source media file.</returns>
    [HttpPost(""), Consumes(Constants.MultipartFormData)]
    [DisableRequestSizeLimit]
    public async Task<IActionResult> GenerateImagePlaceholderFromFile(
        [FromServices] ITempFileRepository tempFileRepository,
        IFormFile file,
        [FromQuery] ImagePlaceholderAlgorithm imagePlaceholderAlgorithm,
        CancellationToken cancellationToken = default)
    {
        var localFile = await tempFileRepository.HandleFileUploadAsync(file, cancellationToken);
        var pgCommand = new ImagePlaceholderGenerationCommand(localFile, imagePlaceholderAlgorithm);
        var placeholder = await EnqueueAndWaitCommandAsync<ImagePlaceholderResult>(pgCommand, cancellationToken);
        return Ok(placeholder);
    }

    /// <summary>
    ///   Creates an image placeholder of given source media file.
    /// </summary>
    /// <param name="tempFileRepository">Temporary file repository.</param>
    /// <param name="file">Source file.</param>
    /// <param name="imagePlaceholderAlgorithm">Desired placeholder generation algorithm.</param>
    /// <param name="cancellationToken">Cancellation token.</param>
    /// <returns>An hash representing the image placeholder of given source media file.</returns>
    [HttpPost("base64"), Consumes(MimeTypeMap.APPLICATION.JSON, MimeTypeMap.APPLICATION.XML)]
    [DisableRequestSizeLimit /* Upload limit is globally set in Startup code. */]
    public async Task<IActionResult> GenerateImagePlaceholderFromJson(
        [FromServices] ITempFileRepository tempFileRepository,
        [FromBody] FileDetails file,
        [FromQuery] ImagePlaceholderAlgorithm imagePlaceholderAlgorithm,
        CancellationToken cancellationToken = default)
    {
        var localFile = await tempFileRepository.HandleFileUploadAsync(file, cancellationToken);
        var pgCommand = new ImagePlaceholderGenerationCommand(localFile, imagePlaceholderAlgorithm);
        var placeholder = await EnqueueAndWaitCommandAsync<ImagePlaceholderResult>(pgCommand, cancellationToken);
        return Ok(placeholder);
    }
}
