﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PommaLabs.MimeTypes;
using PommaLabs.Thumbnailer.Core;
using PommaLabs.Thumbnailer.Models.Attributes;
using PommaLabs.Thumbnailer.Models.Commands;
using PommaLabs.Thumbnailer.Models.DTO.Internal;
using PommaLabs.Thumbnailer.Models.DTO.Public;
using PommaLabs.Thumbnailer.Models.Enumerations;
using PommaLabs.Thumbnailer.Repositories.TempFile;
using PommaLabs.Thumbnailer.Services.Download;
using PommaLabs.Thumbnailer.Services.Job;

namespace PommaLabs.Thumbnailer.Server.Controllers.V1;

/// <summary>
///   Controller which handles thumbnail generation.
/// </summary>
[ApiController, Route("api/v1/thumbnail"), Authorize]
[ProducesResponseType(StatusCodes.Status200OK, Type = typeof(FileDetails))]
[ProducesResponseType(StatusCodes.Status400BadRequest)]
[ProducesResponseType(StatusCodes.Status401Unauthorized)]
[ProducesResponseType(StatusCodes.Status403Forbidden)]
[ProducesResponseType(StatusCodes.Status422UnprocessableEntity)]
[ProducesResponseType(StatusCodes.Status424FailedDependency)]
public sealed class ThumbnailGenerationController : ThumbnailerControllerBase
{
    /// <summary>
    ///   Constructor.
    /// </summary>
    /// <param name="jobService">Job service.</param>
    public ThumbnailGenerationController(IJobService jobService) : base(jobService)
    {
    }

    /// <summary>
    ///   Generates a thumbnail of given source file.
    /// </summary>
    /// <param name="downloadService">Download service.</param>
    /// <param name="fileUri">Source file URI.</param>
    /// <param name="widthPx">
    ///   Max thumbnail width. Thumbnail will be generated preserving the source file aspect ratio
    ///   and, if <paramref name="fill"/> is true, a transparent background will be added so that
    ///   thumbnail width and height are exactly as requested.
    /// </param>
    /// <param name="heightPx">
    ///   Max thumbnail height. Thumbnail will be generated preserving the source file aspect ratio
    ///   and, if <paramref name="fill"/> is true, a transparent background will be added so that
    ///   thumbnail width and height are exactly as requested.
    /// </param>
    /// <param name="sidePx">
    ///   Max thumbnail width and height, it can be used instead of <paramref name="widthPx"/> and
    ///   <paramref name="heightPx"/> to obtain square thumbnails. Thumbnail will be generated
    ///   preserving the source file aspect ratio and, if <paramref name="fill"/> is true, a
    ///   transparent background will be added so that thumbnail width and height are exactly as requested.
    /// </param>
    /// <param name="shavePx">Shaves <paramref name="shavePx"/> from source file edges.</param>
    /// <param name="fill">
    ///   If true, a transparent background will be added so that thumbnail width and height are
    ///   exactly as requested, preserving source file aspect ratio. When <paramref
    ///   name="smartCrop"/> is enabled, this parameter will be ignored. Defaults to true.
    /// </param>
    /// <param name="smartCrop">
    ///   Finds a good crop which satisfies specified <paramref name="widthPx"/> and <paramref
    ///   name="heightPx"/> parameters. When smart crop is enabled, <paramref name="fill"/> is
    ///   ignored. Defaults to false.
    /// </param>
    /// <param name="fallback">
    ///   If true, a fallback image is generated when thumbnail generation fails. Fallback image is
    ///   generated starting from an SVG template, whose customization is documented in project
    ///   README. Defaults to false.
    /// </param>
    /// <param name="imagePlaceholderAlgorithm">
    ///   Desired placeholder generation algorithm. Defaults to None.
    /// </param>
    /// <param name="responseType">Response type.</param>
    /// <param name="openInBrowser">
    ///   When true, browser will receive a "Content-Disposition: inline" header, which should make
    ///   it open the result in-browser. When false, browser will receive a "Content-Disposition:
    ///   attachment" header, which should force it to download the result.
    /// </param>
    /// <param name="cancellationToken">Cancellation token.</param>
    /// <returns>A thumbnail of given source file.</returns>
    [HttpGet("")]
    public async Task<IActionResult> GenerateThumbnailFromUri(
        [FromServices] IDownloadService downloadService,
        [FromQuery] Uri fileUri,
        [FromQuery] ushort widthPx = Validator.ThumbnailSidePx,
        [FromQuery] ushort heightPx = Validator.ThumbnailSidePx,
        [FromQuery, Deprecated] ushort sidePx = Validator.ThumbnailSidePx,
        [FromQuery] ushort shavePx = Validator.ThumbnailShavePx,
        [FromQuery] bool fill = true,
        [FromQuery] bool smartCrop = false,
        [FromQuery] bool fallback = false,
        [FromQuery] ImagePlaceholderAlgorithm imagePlaceholderAlgorithm = ImagePlaceholderAlgorithm.None,
        [FromQuery] ResponseType responseType = ResponseType.Binary,
        [FromQuery] bool openInBrowser = false,
        CancellationToken cancellationToken = default)
    {
        var localFile = await downloadService.DownloadFileAsync(fileUri, cancellationToken);
        return await GenerateThumbnailAsync(
            localFile, widthPx, heightPx, sidePx,
            shavePx, fill, smartCrop, fallback, imagePlaceholderAlgorithm,
            responseType, openInBrowser, cancellationToken);
    }

    /// <summary>
    ///   Generates a thumbnail of given source file.
    /// </summary>
    /// <param name="tempFileRepository">Temporary file repository.</param>
    /// <param name="file">Source file.</param>
    /// <param name="widthPx">
    ///   Max thumbnail width. Thumbnail will be generated preserving the source file aspect ratio
    ///   and, if <paramref name="fill"/> is true, a transparent background will be added so that
    ///   thumbnail width and height are exactly as requested.
    /// </param>
    /// <param name="heightPx">
    ///   Max thumbnail height. Thumbnail will be generated preserving the source file aspect ratio
    ///   and, if <paramref name="fill"/> is true, a transparent background will be added so that
    ///   thumbnail width and height are exactly as requested.
    /// </param>
    /// <param name="sidePx">
    ///   Max thumbnail width and height, it can be used instead of <paramref name="widthPx"/> and
    ///   <paramref name="heightPx"/> to obtain square thumbnails. Thumbnail will be generated
    ///   preserving the source file aspect ratio and, if <paramref name="fill"/> is true, a
    ///   transparent background will be added so that thumbnail width and height are exactly as requested.
    /// </param>
    /// <param name="shavePx">Shaves <paramref name="shavePx"/> from source file edges.</param>
    /// <param name="fill">
    ///   If true, a transparent background will be added so that thumbnail width and height are
    ///   exactly as requested, preserving source file aspect ratio. When <paramref
    ///   name="smartCrop"/> is enabled, this parameter will be ignored. Defaults to true.
    /// </param>
    /// <param name="smartCrop">
    ///   Finds a good crop which satisfies specified <paramref name="widthPx"/> and <paramref
    ///   name="heightPx"/> parameters. When smart crop is enabled, <paramref name="fill"/> is
    ///   ignored. Defaults to false.
    /// </param>
    /// <param name="fallback">
    ///   If true, a fallback image is generated when thumbnail generation fails. Fallback image is
    ///   generated starting from an SVG template, whose customization is documented in project
    ///   README. Defaults to false.
    /// </param>
    /// <param name="imagePlaceholderAlgorithm">
    ///   Desired placeholder generation algorithm. Defaults to None.
    /// </param>
    /// <param name="responseType">Response type.</param>
    /// <param name="openInBrowser">
    ///   When true, browser will receive a "Content-Disposition: inline" header, which should make
    ///   it open the result in-browser. When false, browser will receive a "Content-Disposition:
    ///   attachment" header, which should force it to download the result.
    /// </param>
    /// <param name="cancellationToken">Cancellation token.</param>
    /// <returns>A thumbnail of given source file.</returns>
    [HttpPost(""), Consumes(Constants.MultipartFormData)]
    [DisableRequestSizeLimit /* Upload limit is globally set in Startup code. */]
    public async Task<IActionResult> GenerateThumbnailFromFile(
        [FromServices] ITempFileRepository tempFileRepository,
        IFormFile file,
        [FromQuery] ushort widthPx = Validator.ThumbnailSidePx,
        [FromQuery] ushort heightPx = Validator.ThumbnailSidePx,
        [FromQuery, Deprecated] ushort sidePx = Validator.ThumbnailSidePx,
        [FromQuery] ushort shavePx = Validator.ThumbnailShavePx,
        [FromQuery] bool fill = true,
        [FromQuery] bool smartCrop = false,
        [FromQuery] bool fallback = false,
        [FromQuery] ImagePlaceholderAlgorithm imagePlaceholderAlgorithm = ImagePlaceholderAlgorithm.None,
        [FromQuery] ResponseType responseType = ResponseType.Binary,
        [FromQuery] bool openInBrowser = false,
        CancellationToken cancellationToken = default)
    {
        var localFile = await tempFileRepository.HandleFileUploadAsync(file, cancellationToken);
        return await GenerateThumbnailAsync(
            localFile, widthPx, heightPx, sidePx,
            shavePx, fill, smartCrop, fallback, imagePlaceholderAlgorithm,
            responseType, openInBrowser, cancellationToken);
    }

    /// <summary>
    ///   Generates a thumbnail of given source file.
    /// </summary>
    /// <param name="tempFileRepository">Temporary file repository.</param>
    /// <param name="file">Source file.</param>
    /// <param name="widthPx">
    ///   Max thumbnail width. Thumbnail will be generated preserving the source file aspect ratio
    ///   and, if <paramref name="fill"/> is true, a transparent background will be added so that
    ///   thumbnail width and height are exactly as requested.
    /// </param>
    /// <param name="heightPx">
    ///   Max thumbnail height. Thumbnail will be generated preserving the source file aspect ratio
    ///   and, if <paramref name="fill"/> is true, a transparent background will be added so that
    ///   thumbnail width and height are exactly as requested.
    /// </param>
    /// <param name="sidePx">
    ///   Max thumbnail width and height, it can be used instead of <paramref name="widthPx"/> and
    ///   <paramref name="heightPx"/> to obtain square thumbnails. Thumbnail will be generated
    ///   preserving the source file aspect ratio and, if <paramref name="fill"/> is true, a
    ///   transparent background will be added so that thumbnail width and height are exactly as requested.
    /// </param>
    /// <param name="shavePx">Shaves <paramref name="shavePx"/> from source file edges.</param>
    /// <param name="fill">
    ///   If true, a transparent background will be added so that thumbnail width and height are
    ///   exactly as requested, preserving source file aspect ratio. When <paramref
    ///   name="smartCrop"/> is enabled, this parameter will be ignored. Defaults to true.
    /// </param>
    /// <param name="smartCrop">
    ///   Finds a good crop which satisfies specified <paramref name="widthPx"/> and <paramref
    ///   name="heightPx"/> parameters. When smart crop is enabled, <paramref name="fill"/> is
    ///   ignored. Defaults to false.
    /// </param>
    /// <param name="fallback">
    ///   If true, a fallback image is generated when thumbnail generation fails. Fallback image is
    ///   generated starting from an SVG template, whose customization is documented in project
    ///   README. Defaults to false.
    /// </param>
    /// <param name="imagePlaceholderAlgorithm">
    ///   Desired placeholder generation algorithm. Defaults to None.
    /// </param>
    /// <param name="responseType">Response type.</param>
    /// <param name="openInBrowser">
    ///   When true, browser will receive a "Content-Disposition: inline" header, which should make
    ///   it open the result in-browser. When false, browser will receive a "Content-Disposition:
    ///   attachment" header, which should force it to download the result.
    /// </param>
    /// <param name="cancellationToken">Cancellation token.</param>
    /// <returns>A thumbnail of given source file.</returns>
    /// <remarks>Source file contents need to be encoded as Base64 string.</remarks>
    [HttpPost("base64"), Consumes(MimeTypeMap.APPLICATION.JSON, MimeTypeMap.APPLICATION.XML)]
    [DisableRequestSizeLimit /* Upload limit is globally set in Startup code. */]
    public async Task<IActionResult> GenerateThumbnailFromJson(
        [FromServices] ITempFileRepository tempFileRepository,
        [FromBody] FileDetails file,
        [FromQuery] ushort widthPx = Validator.ThumbnailSidePx,
        [FromQuery] ushort heightPx = Validator.ThumbnailSidePx,
        [FromQuery, Deprecated] ushort sidePx = Validator.ThumbnailSidePx,
        [FromQuery] ushort shavePx = Validator.ThumbnailShavePx,
        [FromQuery] bool fill = true,
        [FromQuery] bool smartCrop = false,
        [FromQuery] bool fallback = false,
        [FromQuery] ImagePlaceholderAlgorithm imagePlaceholderAlgorithm = ImagePlaceholderAlgorithm.None,
        [FromQuery] ResponseType responseType = ResponseType.Binary,
        [FromQuery] bool openInBrowser = false,
        CancellationToken cancellationToken = default)
    {
        var localFile = await tempFileRepository.HandleFileUploadAsync(file, cancellationToken);
        return await GenerateThumbnailAsync(
            localFile, widthPx, heightPx, sidePx,
            shavePx, fill, smartCrop, fallback, imagePlaceholderAlgorithm,
            responseType, openInBrowser, cancellationToken);
    }

    private async Task<IActionResult> GenerateThumbnailAsync(
        TempFileMetadata localFile, ushort widthPx, ushort heightPx, ushort sidePx,
        ushort shavePx, bool fill, bool smartCrop, bool fallback, ImagePlaceholderAlgorithm imagePlaceholderAlgorithm,
        ResponseType responseType, bool openInBrowser, CancellationToken cancellationToken)
    {
        FixWidthAndHeight(ref widthPx, ref heightPx, sidePx);
        var tgCommand = new ThumbnailGenerationCommand(localFile, imagePlaceholderAlgorithm)
        {
            WidthPx = widthPx,
            HeightPx = heightPx,
            ShavePx = shavePx,
            Fill = fill,
            SmartCrop = smartCrop,
            Fallback = fallback,
        };
        var thumbnail = await EnqueueAndWaitCommandAsync<TempFileMetadata>(tgCommand, cancellationToken);
        return await RespondFileAsync(responseType, openInBrowser, nameof(thumbnail), thumbnail);
    }

    private static void FixWidthAndHeight(ref ushort widthPx, ref ushort heightPx, ushort sidePx)
    {
        if ((widthPx != Validator.ThumbnailSidePx && widthPx != 0) || (heightPx != Validator.ThumbnailSidePx && heightPx != 0))
        {
            // Width and height measures have been specified and they have higher priority over side.
        }
        else if (sidePx != Validator.ThumbnailSidePx && sidePx != 0)
        {
            // Side measure has been specified and it has lower priority over width and height.
            widthPx = heightPx = sidePx;
        }
        else
        {
            // Either all measures are set to default or they are all zero. Therefore, it is safe to
            // reset them to the default value, in order to avoid zero values.
            widthPx = heightPx = Validator.ThumbnailSidePx;
        }
    }
}
