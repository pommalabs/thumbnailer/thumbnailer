﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using Microsoft.OpenApi.Models;
using PommaLabs.Thumbnailer.Models.Attributes;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace PommaLabs.Thumbnailer.Server.OperationFilters;

/// <summary>
///   Marks certain Swagger operation parameters as deprecated.
/// </summary>
public sealed class DeprecatedOperationFilter : IOperationFilter
{
    /// <inheritdoc/>
    public void Apply(OpenApiOperation operation, OperationFilterContext context)
    {
        var deprecatedParameters = context.ApiDescription.ParameterDescriptions
            .Where(x => x.CustomAttributes().Any(a => a is DeprecatedAttribute));

        foreach (var dp in deprecatedParameters)
        {
            var deprecatedParameter = operation.Parameters.FirstOrDefault(x => x.Name == dp.Name);
            if (deprecatedParameter != null)
            {
                deprecatedParameter.Deprecated = true;
                deprecatedParameter.Description = "Deprecated. " + deprecatedParameter.Description;
            }
        }
    }
}
