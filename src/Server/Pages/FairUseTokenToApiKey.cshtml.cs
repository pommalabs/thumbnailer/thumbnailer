﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System.Globalization;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using PommaLabs.Salatino.Models.DataTransferObjects;
using PommaLabs.Salatino.Services.FairUseToken;
using PommaLabs.Thumbnailer.Models.DTO.Internal;
using PommaLabs.Thumbnailer.Repositories.ApiKey;

namespace PommaLabs.Thumbnailer.Server.Pages;

/// <summary>
///   Converts a valid fair use token into a temporary API key.
/// </summary>
[AllowAnonymous]
public sealed class FairUseTokenToApiKeyModel : PageModel
{
    private readonly IApiKeyRepository _apiKeyRepository;
    private readonly IFairUseTokenService _fairUseTokenService;

    /// <summary>
    ///   Constructor.
    /// </summary>
    /// <param name="apiKeyRepository">API key repository.</param>
    /// <param name="fairUseTokenService">Fair use token service.</param>
    public FairUseTokenToApiKeyModel(
        IApiKeyRepository apiKeyRepository,
        IFairUseTokenService fairUseTokenService)
    {
        _apiKeyRepository = apiKeyRepository;
        _fairUseTokenService = fairUseTokenService;
    }

    /// <summary>
    ///   Converts an encrypted fair use token into a temporary API key.
    /// </summary>
    /// <param name="token">Encrypted fair use token.</param>
    /// <param name="cancellationToken">Cancellation token.</param>
    public async Task<JsonResult> OnPost(string token, CancellationToken cancellationToken)
    {
        if (string.IsNullOrWhiteSpace(token))
        {
            return new JsonResult(ApiKeyCredentials.Invalid);
        }
        try
        {
            var fairUseToken = new FairUseToken { Token = token };
            if (!_fairUseTokenService.IsTokenValid(fairUseToken))
            {
                return new JsonResult(ApiKeyCredentials.Invalid);
            }

            // Temporary API keys cannot have a length greater than 32 characters, because
            // Thumbnailer SQL tables have been designed that way. That is why "N" format specifier
            // is used in GUID to string conversion. If temporary API keys length were to be
            // increased, then an ALTER TABLE script should be prepared first.
            var apiKey = Guid.NewGuid().ToString("N", CultureInfo.InvariantCulture).ToUpperInvariant();

            return new JsonResult(await _apiKeyRepository.AddTempApiKeyAsync(apiKey, cancellationToken));
        }
        catch
        {
            // An exception thrown while extracting the API key should not be logged, because an
            // attacker could fill application logs sending bad tokens.
            return new JsonResult(ApiKeyCredentials.Invalid);
        }
    }
}
