﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace PommaLabs.Thumbnailer.Server.Pages;

/// <summary>
///   Page which allows the user to upload files and generate thumbnails of them.
/// </summary>
[AllowAnonymous]
public sealed class ThumbnailModel : PageModel
{
    /// <summary>
    ///   Returns the page.
    /// </summary>
    public void OnGet()
    {
        // This is a static page, no processing needs to be done on the server.
    }
}
