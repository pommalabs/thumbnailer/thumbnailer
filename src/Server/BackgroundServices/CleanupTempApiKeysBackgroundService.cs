﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using Microsoft.Extensions.Options;
using PommaLabs.Thumbnailer.Models.Configurations;
using PommaLabs.Thumbnailer.Repositories.ApiKey;

namespace PommaLabs.Thumbnailer.Server.BackgroundServices;

/// <summary>
///   Background service which deletes expired temporary API keys.
/// </summary>
public sealed class CleanupTempApiKeysBackgroundService : BackgroundService
{
    private readonly IOptions<DatabaseConfiguration> _databaseConfiguration;
    private readonly ILogger<CleanupTempApiKeysBackgroundService> _logger;
    private readonly IServiceScopeFactory _serviceScopeFactory;

    /// <summary>
    ///   Constructor.
    /// </summary>
    /// <param name="logger">Logger.</param>
    /// <param name="databaseConfiguration">Database configuration.</param>
    /// <param name="serviceScopeFactory">Service scope factory.</param>
    public CleanupTempApiKeysBackgroundService(
        ILogger<CleanupTempApiKeysBackgroundService> logger,
        IOptions<DatabaseConfiguration> databaseConfiguration,
        IServiceScopeFactory serviceScopeFactory)
    {
        _logger = logger;
        _databaseConfiguration = databaseConfiguration;
        _serviceScopeFactory = serviceScopeFactory;
    }

    /// <inheritdoc/>
    protected override async Task ExecuteAsync(CancellationToken stoppingToken)
    {
        _logger.LogInformation("Temporary API keys cleanup service is starting");

        using var scope = _serviceScopeFactory.CreateScope();
        var apiKeyRepository = scope.ServiceProvider.GetRequiredService<IApiKeyRepository>();

        while (!stoppingToken.IsCancellationRequested)
        {
            try
            {
                var deletedApiKeyCount = await apiKeyRepository.DeleteExpiredTempApiKeysAsync(stoppingToken);
                _logger.LogDebug("Temporary API keys cleanup service deleted {DeletedApiKeyCount} keys", deletedApiKeyCount);
            }
            catch (Exception ex)
            {
                _logger.LogWarning(ex, "An error occurred while deleting expired temporary API keys");
            }

            var delaySeconds = (int)_databaseConfiguration.Value.CacheLifetime.TotalSeconds / 10;
            _logger.LogDebug("Temporary API keys cleanup service will run again in {DelaySeconds} seconds", delaySeconds);

            await Task.Delay(delaySeconds * 1000, stoppingToken);
        }

        _logger.LogInformation("Temporary API keys cleanup service is stopping");
    }
}
