﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using Microsoft.Extensions.Options;
using NodaTime;
using PommaLabs.Thumbnailer.Models.Configurations;
using PommaLabs.Thumbnailer.Repositories.TempFile;

namespace PommaLabs.Thumbnailer.Server.BackgroundServices;

/// <summary>
///   Background service which deletes unused temporary files.
/// </summary>
public sealed class CleanupTempFilesBackgroundService : BackgroundService
{
    private readonly IClock _clock;
    private readonly IOptions<DatabaseConfiguration> _databaseConfiguration;
    private readonly ILogger<CleanupTempFilesBackgroundService> _logger;
    private readonly ITempFileRepository _tempFileRepository;

    /// <summary>
    ///   Constructor.
    /// </summary>
    /// <param name="logger">Logger.</param>
    /// <param name="databaseConfiguration">Database configuration.</param>
    /// <param name="tempFileRepository">Temporary file repository.</param>
    /// <param name="clock">Clock.</param>
    public CleanupTempFilesBackgroundService(
        ILogger<CleanupTempFilesBackgroundService> logger,
        IOptions<DatabaseConfiguration> databaseConfiguration,
        ITempFileRepository tempFileRepository,
        IClock clock)
    {
        _logger = logger;
        _databaseConfiguration = databaseConfiguration;
        _tempFileRepository = tempFileRepository;
        _clock = clock;
    }

    /// <inheritdoc/>
    protected override async Task ExecuteAsync(CancellationToken stoppingToken)
    {
        _logger.LogInformation("Temporary files cleanup service is starting");

        while (!stoppingToken.IsCancellationRequested)
        {
            try
            {
                await DeleteOldTempFilesAsync(stoppingToken);
            }
            catch (Exception ex)
            {
                _logger.LogWarning(ex, "An error occurred while deleting old temporary files");
            }

            var delaySeconds = (int)_databaseConfiguration.Value.CacheLifetime.TotalSeconds / 10;
            _logger.LogDebug("Temporary files cleanup service will run again in {DelaySeconds} seconds", delaySeconds);

            await Task.Delay(delaySeconds * 1000, stoppingToken);
        }

        _logger.LogInformation("Temporary files cleanup service is stopping");
    }

    private async Task DeleteOldTempFilesAsync(CancellationToken stoppingToken)
    {
        var threshold = _clock.GetCurrentInstant()
            .Minus(Duration.FromSeconds(_databaseConfiguration.Value.CacheLifetime.TotalSeconds * 2))
            .ToDateTimeOffset();

        var evaluatedFileCount = 0;
        var deletedFileCount = 0;

        foreach (var file in await _tempFileRepository.GetTempFilesAsync(stoppingToken))
        {
            evaluatedFileCount++;

            if (stoppingToken.IsCancellationRequested)
            {
                break;
            }

            var fileName = Path.GetFileName(file.Path);

            if (File.GetLastAccessTimeUtc(file.Path) >= threshold)
            {
                _logger.LogDebug("File '{FileName}' has a last access time newer than {Threshold}, it will not be deleted", fileName, threshold);
                continue;
            }

            _logger.LogDebug("File '{FileName}' has a last access time older than {Threshold}, it will be deleted", fileName, threshold);
            await _tempFileRepository.DeleteTempFileAsync(file, stoppingToken);

            deletedFileCount++;
        }

        _logger.LogDebug("Temporary files cleanup service evaluated {EvaluatedFileCount} files and deleted {DeletedFileCount} of them", evaluatedFileCount, deletedFileCount);
    }
}
