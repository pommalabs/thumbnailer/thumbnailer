﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using PommaLabs.Thumbnailer.Models.Commands;
using PommaLabs.Thumbnailer.Services.ImagePlaceholder;
using PommaLabs.Thumbnailer.Services.Job;
using PommaLabs.Thumbnailer.Services.Optimization;
using PommaLabs.Thumbnailer.Services.Thumbnail;

namespace PommaLabs.Thumbnailer.Server.BackgroundServices;

/// <summary>
///   Background service which processes asynchronous jobs.
/// </summary>
public sealed class ProcessQueuedJobsBackgroundService : BackgroundService
{
    private readonly IJobService _jobService;
    private readonly ILogger<ProcessQueuedJobsBackgroundService> _logger;
    private readonly IOptimizationService _optimizationService;
    private readonly int _processorId;
    private readonly IThumbnailService _thumbnailService;
    private readonly IImagePlaceholderService _imagePlaceholderService;

    /// <summary>
    ///   Constructor.
    /// </summary>
    /// <param name="processorId">Processor ID.</param>
    /// <param name="logger">Logger.</param>
    /// <param name="jobService">Job service.</param>
    /// <param name="optimizationService">Optimization service.</param>
    /// <param name="thumbnailService">Thumbnail service.</param>
    /// <param name="imagePlaceholderService">Image placeholder service.</param>
    public ProcessQueuedJobsBackgroundService(
        int processorId,
        ILogger<ProcessQueuedJobsBackgroundService> logger,
        IJobService jobService,
        IOptimizationService optimizationService,
        IThumbnailService thumbnailService,
        IImagePlaceholderService imagePlaceholderService)
    {
        _processorId = processorId;
        _logger = logger;
        _jobService = jobService;
        _optimizationService = optimizationService;
        _thumbnailService = thumbnailService;
        _imagePlaceholderService = imagePlaceholderService;
    }

    /// <inheritdoc/>
    protected override async Task ExecuteAsync(CancellationToken stoppingToken)
    {
        _logger.LogInformation("Queued jobs processor service with ID {ProcessorId} is starting", _processorId);

        while (!stoppingToken.IsCancellationRequested)
        {
            ThumbnailerCommandBase? command = null;
            try
            {
                command = await _jobService.DequeueCommandAsync(stoppingToken);
                command = command with { ProcessorId = _processorId };

                object result = command switch
                {
                    ImagePlaceholderGenerationCommand pgCommand => await _imagePlaceholderService.GenerateImagePlaceholderAsync(pgCommand, stoppingToken),
                    MediaOptimizationCommand moCommand => await _optimizationService.OptimizeMediaAsync(moCommand, stoppingToken),
                    ThumbnailGenerationCommand tgCommand => await _thumbnailService.GenerateThumbnailAsync(tgCommand, stoppingToken),
                    _ => throw new InvalidOperationException($"Given command type '{command.GetType().Name}' does not have an handler"),
                };

                _logger.LogDebug("Command with internal job ID '{InternalJobId}' was processed successfully", command.InternalJobId);
                await _jobService.MarkJobAsProcessedAsync(command.InternalJobId.GetValueOrDefault(), result, stoppingToken);
            }
            catch (Exception ex)
            {
                if (command == null)
                {
                    _logger.LogWarning(ex, "An error occurred while retrieving a new command to be processed");
                    continue;
                }

                _logger.LogWarning(ex, "An error occurred while processing a command with internal job ID '{InternalJobId}'", command.InternalJobId);
                await _jobService.MarkJobAsFailedAsync(command.InternalJobId.GetValueOrDefault(), ex.Message, stoppingToken);
            }
        }

        _logger.LogInformation("Queued jobs processor service with ID {ProcessorId} is stopping", _processorId);
    }
}
