# Thumbnailer

[![License: MIT][project-license-badge]][project-license]
[![Donate][paypal-donations-badge]][paypal-donations]
[![standard-readme compliant][github-standard-readme-badge]][github-standard-readme]
[![GitLab pipeline status][gitlab-pipeline-status-badge]][gitlab-pipelines]
[![Quality gate][sonar-quality-gate-badge]][sonar-website]
[![Code coverage][sonar-coverage-badge]][sonar-website]
[![Renovate enabled][renovate-badge]][renovate-website]

Web service which exposes endpoints to generate file thumbnails and to optimize media files.

Thumbnailer heavily relies on many open source command line tools. Just to name a few:

- [`NetVips`][github-net-vips], used to generate image thumbnails.
- [`pngquant`][pngquant-website], used to optimize PNG images and file thumbnails.
- [`LibreOffice`][libreoffice-website], used to convert Office files to images.
- [`Firefox`][firefox-website], used to convert HTML files to images.
- [`ffmpeg`][ffmpeg-website], used to generate video thumbnails and to optimize MP4 files.

Web service runs on ASP.NET Core and following clients are available:

- [.NET client][thumbnailer-dotnet-client]

## Table of Contents

- [Install](#install)
  - [Tags](#tags)
  - [Configuration](#configuration)
  - [Logging](#logging)
  - [Database](#database)
  - [Persistence](#persistence)
    - [Read-only](#read-only)
  - [Templates](#templates)
    - [Email layout](#email-layout)
    - [Fallback image](#fallback-image)
- [Usage](#usage)
  - [Synchronous API (v1)](#synchronous-api-v1)
  - [Asynchronous API (v2)](#asynchronous-api-v2)
- [Maintainers](#maintainers)
- [Contributing](#contributing)
  - [Contributors](#contributors)
  - [Editing](#editing)
  - [Restoring dependencies](#restoring-dependencies)
  - [Starting development server](#starting-development-server)
  - [Running tests](#running-tests)
  - [Building Docker image](#building-docker-image)
- [License](#license)

## Install

Thumbnailer web service is provided as a Docker image.

You can quickly start a local test instance with following command:

```bash
docker run -it --rm -e Security__AllowAnonymousAccess=true -p 8080:8080 container-registry.pommalabs.xyz/pommalabs/thumbnailer:latest
```

Local test instance will be listening on port 8080 and it will accept unauthenticated requests.
Please check the [Configuration](#configuration) section to find further information
about how the web service can be properly configured.

### Tags

Following tags are available:

| Tag      | Base image                                                   |
|----------|--------------------------------------------------------------|
| `latest` | `container-registry.pommalabs.xyz/pommalabs/dotnet:8-aspnet` |

Each tag can be downloaded with following command, just replace `latest` with desired tag:

```bash
docker pull container-registry.pommalabs.xyz/pommalabs/thumbnailer:latest
```

Tags are rebuilt every week by a job scheduled on GitLab CI platform.

### Configuration

Docker image can be configured using the following environment variables.
Moreover, the same variables can also be set by mounting a `.env` file in `/opt/app/`.

| Environment variable                             | Notes                                             | Default value |
|--------------------------------------------------|---------------------------------------------------|---------------|
| `Database__CacheLifetime`                        | How long each cache entry will live.              | 20 minutes    |
| `Database__ConnectionString`                     | Connection string of a SQL database.              |               |
| `Database__Provider`                             | `None`, `MySql`, `PostgreSql` or `SqlServer`.     | `None`        |
| `Database__SchemaName`                           | SQL schema name.                                  |               |
| `JobProcessor__Count`                            | How many job processor threads should be spawned. | 2 processors  |
| `Security__AcceptApiKeysViaHeaderParameter`      | Accept API keys via header parameter.             | `true`        |
| `Security__AcceptApiKeysViaQueryStringParameter` | Accept API keys via query string parameter.       | `false`       |
| `Security__AllowAnonymousAccess`                 | Allow anonymous users to consume services.        | `false`       |
| `Security__ApiKeys__X__ExpiresAt`                | If not specified, API key will not expire.        |               |
| `Security__ApiKeys__X__Name`                     | Descriptive name of API key at index X.           |               |
| `Security__ApiKeys__X__Value`                    | Value of API key at index X.                      |               |
| `Security__ApiKeysJson`                          | A JSON array containing desired API keys.         |               |
| `Security__AsyncProcessTimeout`                  | Timeout for low level processes (in async jobs).  | 10 minutes    |
| `Security__EnableCaseSensitiveApiKeyValidation`  | Enable case sensitive API key validation.         | `false`       |
| `Security__FileDownloadTimeout`                  | Timeout for file download.                        | 30 seconds    |
| `Security__MaxFileDownloadSizeInBytes`           | How many bytes are allowed for remote downloads.  | 64 MB         |
| `Security__MaxFileUploadSizeInBytes`             | How many bytes are allowed for uploads.           | 32 MB         |
| `Security__ProcessTimeout`                       | Timeout for low level processes (in sync calls).  | 30 seconds    |
| `Security__TempApiKeyLifetime`                   | How long a website temporary API key is valid.    | 5 minutes     |
| `Website__Enabled`                               | Expose the public website.                        | `false`       |
| `Website__FairUseTokenLifetime`                  | How long a fair use token is valid.               | 60 minutes    |

`Security__ApiKeysJson` variable is a JSON array of API key objects; each object
can have three properties:

- `name`, required, descriptive name.
- `value`, required, which will be used to perform authentication.
- `expiresAt`, optional, expiration timestamp. If not specified, API key will not expire.

An example value for `Security__ApiKeysJson` is the following one:

```json
[{"name":"Expiring API key","value":"123","expiresAt":"2018-12-10T13:49:51.141Z"},{"name":"Not expiring API key","value":"456"}]
```

Please note that setting both `Security__ApiKeysJson` and single array item configurations
(e.g. `Security__ApiKeys__0__Name`) is not supported and its behavior cannot be relied upon.

### Logging

Web service writes log messages to the console.

If an Azure Application Insights connection string is specified using the
`ApplicationInsights__ConnectionString` configuration,
logs and telemetry data are also sent to that service.

### Database

Setup scripts for supported systems are available:

- [MySQL and MariaDB][thumbnailer-mysql-schema]
- [PostgreSQL][thumbnailer-postgresql-schema]
- [SQL Server][thumbnailer-sqlserver-schema]

You can customize those scripts according to your needs, but please remember that
if you change schema name, you should update `Database__SchemaName`
configuration property accordingly.

### Persistence

When an external database is configured, it is also used by the web service
to store cache entries using the [KVLite library][gitlab-kvlite].

Cache entries store the SHA384 of input files and the parameters used;
that data is linked to the location of the output file
in Thumbnailer temporary directory (`/tmp/thumbnailer`).
Temporary files stay in that directory for at least as long as
it has been specified with `Database__CacheLifetime` configuration.

Moreover, distributed cache is also used to track job information for API v2.

So, you might want to persist the temporary directory root, `/tmp/thumbnailer`,
in order to be able to handle the following use cases:

- Web service receives a lot of requests and temporary files directory
  can grow quite a lot. In that case, a separate volume with proper size can be configured.
- Web service works in load balancing and it is configured to use an external DB.
  In that case, sharing the temporary files directory is required in order
  to let all instances be able to read cached outputs.

However, persisting that directory is not mandatory at all.
Web service works fine without having it persisted, especially when only one node is used.

#### Read-only

A special note on persistence applies when Docker container is run in read-only mode.
In that scenario, following directories need to be mounted on writeable storage
in order for web service to properly work:

- `/home/app`, where [`LibreOffice`][libreoffice-website] writes configuration files at runtime.
- `/tmp`, used by web service itself (`/tmp/thumbnailer`) and by .NET runtime.

### Templates

Web service stores its templates in a dedicated directory, `/opt/app/Templates`.
Currently, that directory contains following templates:

- `email-layout.mrc`, which is used to convert EML files to HTML,
  a step required for thumbnail generation of EML files.
- `fallback.svg`, which is used to generate fallback images
  when thumbnail generation fails.

If templates need to be customized, then templates directory can be mounted
to a local directory using a [bind mount][docker-bind-mounts], for example:

```bash
docker run -it --rm \
  -e Security__AllowAnonymousAccess=true -p 8080:8080 \
  -v /my/templates:/opt/app/Templates:ro \
  container-registry.pommalabs.xyz/pommalabs/thumbnailer:latest
```

Source templates can be found on [GitLab][thumbnailer-templates].

When templates are changed, web service should be restarted,
because templates are loaded at startup.

#### Email layout

In order to obtain a thumbnail for EML files, those ones are first converted
to HTML using [MHonArc][mhonarc-website] and the template used during the conversion
is stored in `email-layout.mrc`. The default layout shipped with Thumbnailer
follows the default layout shipped with MHonArc, except for the following differences:

- Printed headers have been restricted to `to`, `from`, `subject`, `date`.
- Only `text/*` and `image/*` parts are considered.
- Non-CID URLs are included in the final output.

If you wish to further customize the layout, please refer to
[official MHonArc documentation][mhonarc-documentation].

#### Fallback image

Template for fallback image, stored in `fallback.svg`, is handled as a Liquid
template using [dotLiquid library][github-dotliquid]. Liquid syntax can be used
to apply filters and conditional logic.

Fallback image template receives a `file` object containing the following attributes:

- `content_type`, containing input file MIME type.
- `extension`, file extension deduced by Thumbnailer, which might differ
  from the one received as input. File extension includes the leading dot character.
- `size`, file size in bytes.
- `humanized_size`, file size converted into a readable string (e.g. "2.4 MB").

## Usage

Please check OpenAPI docs on your instance (just visit the `/swagger` page)
or head to the [demo instance Swagger page][thumbnailer-swagger-index].
Each endpoint has been documented using the OpenAPI specification.

**Media optimization** supports following content types:

- `image/gif`
- `image/jpeg`
- `image/png`
- `image/svg+xml`
- `image/webp`
- `video/mp4`

**Thumbnail generation** supports quite a lot of content types.
Anyway, you can use the [test files][thumbnailer-samples] as a quick reference
to find out what the web service really supports.
Inside each "format" directory you will find a copy of the latest thumbnails, that is,
for each file you will find the most recent copy of its thumbnail.

For files whose thumbnail cannot be generated either because there is none,
e.g. for archive files, or because it is not supported by the web service,
a `fallback` flag can be set when requesting thumbnail generation.
When enabled, a fallback image is generated when thumbnail generation fails or when it cannot be started at all.
See [Templates](#templates) section to find out how to customize fallback images.

### Synchronous API (v1)

Synchronous API is exposed through [v1 endpoints][thumbnailer-swagger-index] and,
as its name suggests, it allows to send synchronous HTTP requests for
thumbnail generation or media generation, where the output file is provided
as a response for each request.

This approach is quite simple but it is not recommended, because each HTTP
request should have a very short timeout and that fact does not work well with
the processing of large or complex files.
Anyway, this kind of API was the first one to be implemented by Thumbnailer and
it will be maintained in the future.
There are use cases where a simple API is enough and it is preferred over a more complex one.

Synchronous requests timeout is controlled by `Security__ProcessTimeout` setting,
as described in the [Configuration section](#configuration).

With a Thumbnailer instance running, as described in the [Install section](#install),
you can generate the thumbnail of a sample image with the following request:

```http
GET /api/v1/thumbnail?fileUri=https://via.placeholder.com/2048&widthPx=256&heightPx=256&shavePx=0&fill=true&smartCrop=false&responseType=Binary HTTP/1.1
Host: localhost:8080
```

That endpoint will generate a response containing the requested thumbnail.

Please inspect Swagger documentation of [v1 endpoints][thumbnailer-swagger-index]
in order to find further information about exposed operations.
For example, input files can also be POSTed using a `multipart/form-data` request
or with a JSON payload.

### Asynchronous API (v2)

Asynchronous API is exposed through [v2 endpoints][thumbnailer-swagger-index] and
it allows to start processing jobs which can be monitored with a dedicated API.
When a job is finished, its output will be available for download.

Asynchronous API is more complex to work with, because it requires at least three
HTTP calls in order to obtain the operation output. However, the asynchronous
approach is required when potentially large or complex files need to be processed.

Asynchronous jobs timeout is controlled by `Security__AsyncProcessTimeout` setting,
as described in the [Configuration section](#configuration).

With a Thumbnailer instance running, as described in the [Install section](#install),
you can start the generation of the thumbnail of a sample image with the following request:

```http
GET /api/v2/thumbnail?fileUri=https://via.placeholder.com/2048&widthPx=256&heightPx=256&shavePx=0&fill=true&smartCrop=false HTTP/1.1
Host: localhost:8080
```

That endpoint will respond with a JSON payload containing the job ID:

```json
{
    "failureReason": null,
    "jobId": "NEW_JOB_ID",
    "status": "Queued"
}
```

That job ID can be used to query the job status:

```http
GET /api/v2/jobs/NEW_JOB_ID HTTP/1.1
Host: localhost:8080
```

When job status becomes `Processed`, as in sample below:

```json
{
    "failureReason": null,
    "jobId": "NEW_JOB_ID",
    "status": "Processed"
}
```

Then job result is ready to be downloaded from following endpoint:

```http
GET /api/v2/jobs/NEW_JOB_ID/download?responseType=Binary HTTP/1.1
Host: localhost:8080
```

That endpoint will generate a response containing the requested thumbnail.

Please inspect Swagger documentation of [v2 endpoints][thumbnailer-swagger-index]
in order to find further information about exposed operations.
For example, input files can also be POSTed using a `multipart/form-data` request
or with a JSON payload.

## Maintainers

[@pomma89][gitlab-pomma89].

## Contributing

MRs accepted.

Small note: If editing the README, please conform to the [standard-readme][github-standard-readme] specification.

### Contributors

@AlessioGiacobbe implemented image placeholder generation using
[BlurHash][blurhash-website] and [ThumbHash][thumbhash-website] algorithms.
@AlessioGiacobbe also implemented unit and integration tests for that feature.

### Editing

[Visual Studio Code][vscode-website], with [Remote Containers extension][vscode-remote-containers],
is the recommended way to work on this project.

A development container has been configured with all required tools.

[Visual Studio Community][vs-website] is also supported
and an updated solution file, `thumbnailer.sln`, has been provided.

### Restoring dependencies

When opening the development container, dependencies should be automatically restored.

Anyway, dependencies can be restored with following command:

```bash
dotnet restore
```

### Starting development server

A local development server listening on port `8080` can be started with following command:

```bash
dotnet run --project ./src/Server/Server.csproj
```

### Running tests

Tests can be run with following command:

```bash
dotnet test
```

Tests can also be run with following command, which collects coverage information:

```bash
./build.sh --target run-tests
```

Tests for a specific file format can be using the following command,
where `FORMAT` should be the name of one of directories holding
[test files][thumbnailer-samples], written in upper case:

```bash
export FORMAT="PNG"
dotnet test --filter "Name~${FORMAT}_"
```

Tests can also be run again a web service instance. Following command
should be adapted according to instance base URI and optional API key:

```bash
export THUMBNAILER_BASE_URI="http://localhost:8080"
export THUMBNAILER_API_KEY="123QWE"
dotnet test
```

### Building Docker image

Docker image can be built with following command:

```bash
docker build . -f ./src/Server/Dockerfile -t $DOCKER_TAG
```

Please replace `$DOCKER_TAG` with a valid tag (e.g. `thumbnailer`).

## License

MIT © 2019-2024 [PommaLabs Team and Contributors][pommalabs-website]

[blurhash-website]: https://blurha.sh/
[docker-bind-mounts]: https://docs.docker.com/storage/bind-mounts/
[ffmpeg-website]: https://ffmpeg.org/
[firefox-website]: https://www.mozilla.org/
[github-dotliquid]: https://github.com/dotliquid/dotliquid
[github-net-vips]: https://github.com/kleisauke/net-vips
[github-standard-readme]: https://github.com/RichardLitt/standard-readme
[github-standard-readme-badge]: https://img.shields.io/badge/readme%20style-standard-brightgreen.svg?style=flat-square
[gitlab-kvlite]: https://gitlab.com/pommalabs/kvlite
[gitlab-pipeline-status-badge]: https://gitlab.com/pommalabs/thumbnailer/thumbnailer/badges/main/pipeline.svg?style=flat-square
[gitlab-pipelines]: https://gitlab.com/pommalabs/thumbnailer/thumbnailer/pipelines
[gitlab-pomma89]: https://gitlab.com/pomma89
[libreoffice-website]: https://www.libreoffice.org/
[mhonarc-documentation]: https://www.mhonarc.org/MHonArc/doc/index.html
[mhonarc-website]: https://www.mhonarc.org/
[paypal-donations]: https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=ELJWKEYS9QGKA
[paypal-donations-badge]: https://img.shields.io/badge/Donate-PayPal-important.svg?style=flat-square
[pngquant-website]: https://pngquant.org/
[pommalabs-website]: https://pommalabs.xyz/
[project-license]: https://gitlab.com/pommalabs/thumbnailer/-/blob/main/LICENSE
[project-license-badge]: https://img.shields.io/badge/License-MIT-yellow.svg?style=flat-square
[renovate-badge]: https://img.shields.io/badge/renovate-enabled-brightgreen.svg?style=flat-square
[renovate-website]: https://renovate.whitesourcesoftware.com/
[sonar-coverage-badge]: https://img.shields.io/sonar/coverage/pommalabs_thumbnailer?server=https%3A%2F%2Fsonarcloud.io&sonarVersion=8&style=flat-square
[sonar-quality-gate-badge]: https://img.shields.io/sonar/quality_gate/pommalabs_thumbnailer?server=https%3A%2F%2Fsonarcloud.io&sonarVersion=8&style=flat-square
[sonar-website]: https://sonarcloud.io/dashboard?id=pommalabs_thumbnailer
[thumbhash-website]: https://evanw.github.io/thumbhash/
[thumbnailer-dotnet-client]: https://www.nuget.org/packages/PommaLabs.Thumbnailer.Client/
[thumbnailer-samples]: https://gitlab.com/pommalabs/thumbnailer/thumbnailer/-/tree/main/tests/IntegrationTests/Samples
[thumbnailer-templates]: https://gitlab.com/pommalabs/thumbnailer/thumbnailer/-/tree/main/src/Server/Templates
[thumbnailer-mysql-schema]: https://gitlab.com/pommalabs/thumbnailer/thumbnailer/-/blob/main/tests/IntegrationTests/SqlScripts/mysql.sql
[thumbnailer-postgresql-schema]: https://gitlab.com/pommalabs/thumbnailer/thumbnailer/-/blob/main/tests/IntegrationTests/SqlScripts/postgresql.sql
[thumbnailer-sqlserver-schema]: https://gitlab.com/pommalabs/thumbnailer/thumbnailer/-/blob/main/tests/IntegrationTests/SqlScripts/sqlserver.sql
[thumbnailer-swagger-index]: https://thumbnailer.pommalabs.xyz/swagger/index.html
[vs-website]: https://visualstudio.microsoft.com/
[vscode-remote-containers]: https://code.visualstudio.com/docs/remote/containers
[vscode-website]: https://code.visualstudio.com/
